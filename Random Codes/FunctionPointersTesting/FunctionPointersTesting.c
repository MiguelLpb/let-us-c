/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: Miguel
 *
 * Created on 19 de mayo de 2020, 06:45 PM
 */

#include <stdio.h>
#include <stdlib.h>

//Prototypes
int do_operation();
int add();
int mult();

//A function pointer is a variable that stores the address of a variable we want to call later on
//You read fucntion pointers differently in comparison to other syntax: The identifier is in the middle, 
//the arguments of the function on the right and the return of the function on the left


//Take the main as an example: Main is a function that takes two arguments: an integer and a pointer to a character pointer and it returns an integer
int main(int argc, char** argv) {

    
    
    //We declare a variable with the return from do_operation, which delivers the return from the add function, which is 10.
    int result = do_operation(add, 5, 5);
    printf("\nThe answer is %d", result);
    
    //We declare a variable with the return from do_operation, which delivers the return from the add function, which is 10.
    int result2 = do_operation(mult, 5, 5);
    printf("\nThe answer is %d", result2);
    
    return (EXIT_SUCCESS);
}


// This function takes another function called op and two integers as arguments. It returns op(x,y)
//op takes two integers and returns an int
 int do_operation(int (*op)(int,int), int x, int y) {
        return op(x,y);
    }
 int add(int a, int b) {return a + b;}
 int mult(int a, int b) {return a * b;}