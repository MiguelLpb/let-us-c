/*This program swaps two numbers without using an aditional temporary variable*/

int main (void) {
    int a = 5;
    int b = 7;
    int temp;
    
    
    //Swaps a and b using third variable
 /*   temp = a;
    a = b;
    b = temp; */
    
    a = a + b;  // a = 5 + 7 = 12 
    b = a - b;  // b = 12 - 7 = 5
    a = a - b;  // a = 12 - 5 = 7
    
    printf("a = %d and b = %d", a, b);
    
}