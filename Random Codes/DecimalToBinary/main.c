/*
 * Take any number in base 10 and convert it to binary
*/



#include <stdio.h>
double OriginalNum;

//Functions
int toBinary(int decimalNo); //Converts a integer number in base 10 to binary
double DecFToBin2(double decNum); //Converts a fraction to binary


int main (void) {

    double num;
    printf("What number do you want to convert? Enter an integer or decimal number: \n");
    scanf(" %lf", &num);
    int Int_num = (int)num;
    double Float_num = num - (double)Int_num;


    OriginalNum = Float_num;
    
    double answer = (double)(toBinary(Int_num) + DecFToBin2(Float_num));
    
    printf("%lf converted to binary is %lf \n", num, answer );
}

int toBinary(int decimalNo){

    //Base Case
    if (decimalNo < 2) // if the decimal number is 0 or 1 then return 0 or 1
        return decimalNo;
    
    //Recursive Case
    return toBinary(decimalNo / 2) * 10 + decimalNo % 2;
}

double DecFToBin2(double decNum){

    double currentNum = decNum * 2.0;
    double remainder = ((int)currentNum) % 2;
    
    //base case
    if(currentNum == 1 || currentNum == OriginalNum)
        return remainder /10;
    
    //recursive case
    return DecFToBin2(currentNum - remainder) / 10 + remainder/10;
        
}