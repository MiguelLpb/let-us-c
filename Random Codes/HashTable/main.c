/* 
    Description: This program creates a hash table that inserts some 7 values using the quadratic probing method 
 */

//Libraries
#include <stdio.h>
#include <stdlib.h>

//Define the size of hash table
#define SIZE 7 //NOTE: for the quadratic probing method, the size must be a primary number to make sure that we can insert keys on collisions.
               // and the hash table must be at least half empty


//Define our function
int quadratic_probing_insert(int *hashtable, int *empty, int key); //We use pointers because the elements from hashtable and empty come from an array


int main() {

    int hashtable[SIZE]; //We are creating the hash table
    int empty[SIZE] = {0}; //Initializing every element in the array equal 0
    
    int i;
    for(i=0; i<SIZE; i++){
        quadratic_probing_insert(hashtable,  empty, i);
    }
    
    //Print the hashtable to the console
    for(i=0; i<SIZE; i++){
        printf("%d ", hashtable[i]);
    }   

/*
 1. We need to get the key 'k'
 2. Set counter i=0
 3. Compute the hash function h[k] = k % SIZE, I will call the hash function 'index'
 4. If the hashtable [ h[k] ] is empty
    4.1 Insert key 'k' at hashtable [ h[k] ]
    4.2 Stop
    Else
    4.3 The key space at hashtable [ h[k] ] is occupied, so we need to find the next available key space
    4.4 Increment i
    4.5 Compute the new hash function h[k] = (k + i * i) % SIZE
 5. The hashtable is full
 6. Stop
 */


int quadratic_probing_insert(int *hashtable, int *empty, int key){

    int i, index; //index is the hash function 'h'
    
    for (i=0; i<SIZE; i++){
        index = (key + i * i) % SIZE; //Computing our hash function 
        
        if(empty[index] == 0){
            hashtable[index] = key;
            empty[index] = -1; //Mark that the position in the hashtable has been taken, using the empty array
            return index;
        }
    }

    return -1;
}