/*
 This program sorts an array of elements using Selection Sort Algorithm
 
 array input: 3 2 1 
 How it works:
 3 2 1
 1 2 3
 */

#include <stdio.h>

//Selection Sort

void SelectionSort(int size, int *a);
int main(){
    int size, i, array[21];
    
    printf("Enter the total number of elements: ");
    scanf("%d", &size);
    
    printf("Enter %d elements: ", size);
    for(i=0; i<size; i++){
        scanf(" %d", &array[i]);
    }
    
    
    //Selection Sort
    SelectionSort(size, array);
    
    printf("The array after sorting is: ");
    for(i=0; i<size; i++){
        printf(" %d", array[i]);
    }
    
    return 0;
}


void SelectionSort(int size, int *a){

    int i, j;
    
    for(j=0; j< size - 1; j++){
        int iMin = j;
        
        for(i=j+1; i<size; i++){
            if(a[i]<a[iMin]){
                iMin = i;
            }
        }
        
        if(iMin != j){
        //swap
            int temp = a[j];
            a[j] = a[iMin];
            a[iMin] = temp;
        }
    }
}