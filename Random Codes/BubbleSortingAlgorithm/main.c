/*
 This program sorts an array of elements using the bubble sort algorithm

 Output:
 Enter total number of elements: 4
 Enter the 4 elements: 1   5   4   3
 After sorting:        1   3   4   5
  
 */

#include <stdio.h>

int BubbleSort(int size, int *array);

int main(void) {

    int size, i, array[20];
    
    printf(" Enter total number of elements: ");
    scanf(" %d", &size);
    
    printf("Enter the %d elements: ", size);
    for(i=0; i<size; i++){
        scanf(" %d", &array[i]);
    }
    
    
    //Run the bubble sort algorithm to sort the list of elements
    BubbleSort(size, array);
    
    printf("After sorting: ");
    
    for(i=0; i<size; i++){
        printf(" %d ", array[i]);
    }

    
    printf("\n");
    return 0;
}


int BubbleSort(int size, int *array){

    int i, j, temp;
    
    //Bubble Sorting Algorithm
    for(i=size-2; i>=0; i--){
        for (j=0; j<=i; j++){
        //Swap
            if(array[j] > array[j+1]){
                temp = array[j];
                array[j] = array[j+1];
                array[j+1] = temp;
            }
        }
    }
    
    return 1;
}