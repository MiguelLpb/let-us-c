/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: Miguel
 *
 * Created on 19 de mayo de 2020, 05:50 PM
 */

#include <stdio.h>
#include <stdlib.h>

/*A pointer is a special variable that is capable of storing some address
It points to a memory location where the first byte is stored*/

int main(int argc, char** argv) {

    
    //This is a normal variable declaration
    int x = 5;
    
    //This is a pointer declaration
    int *ptr; 
    
    //The value of the pointer equals the address where x is stored at
    ptr = &x;
    
    //This outputs the address of the pointer
    printf("\nThis is the address of ptr: %p", &ptr);
    
    //This outputs the value of ptr, which is the address where x is stored.
    printf("\nThis is the value of ptr: %p", ptr);
    
    //This says: go to the address of the object and take what is stored in there to print it
    printf("\nThis is the value of x: %d", &ptr);
    
    //Changing the value of the object pointed by the pointer
    *ptr = 3;
    printf("\nThe new value of x: %d ", x);
    
    return (EXIT_SUCCESS);
}


            /*
            ______X______
            |           |                   _________POINTER_________
            |           |                   |                        |
            |     5     |<------------------|           1000         |
            |           |                   |                        | 
            |           |                   |________________________|
            -------------                               2000
  
 */     