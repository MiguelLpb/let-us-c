/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   main.c
 * Author: Miguel
 *
 * Created on 18 de noviembre de 2019, 05:09 PM
 */

#include <stdio.h>
#include <stdlib.h>

/*
 * 
 */

   

int main(int argc, char** argv) {
    
  
    float timeSpent;
    float factor = 61320;
    float totalTime;
    float totalHours;
    float totalDays;
    
    
    printf("Please enter the amount of minutes you spend in that planet: \n");
    scanf(" %f", &timeSpent);
    
    totalHours = (timeSpent/60)*factor;
    totalDays = totalHours/24;
    totalTime = totalDays/365;
    
    
    if (timeSpent > 0) {
        printf("\nYou spent %.2f Earth-years during your visit in that planet.",totalTime);
    }
    else {printf("\nYour input is not valid. Try again.");
    }
    
    
    return (EXIT_SUCCESS);
}