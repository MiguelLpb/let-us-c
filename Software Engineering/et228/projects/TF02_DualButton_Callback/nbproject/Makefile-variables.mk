#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=Cygwin-Windows
CND_ARTIFACT_DIR_Debug=dist/Debug/Cygwin-Windows
CND_ARTIFACT_NAME_Debug=tf02_dualbutton_callback
CND_ARTIFACT_PATH_Debug=dist/Debug/Cygwin-Windows/tf02_dualbutton_callback
CND_PACKAGE_DIR_Debug=dist/Debug/Cygwin-Windows/package
CND_PACKAGE_NAME_Debug=tf02dualbuttoncallback.tar
CND_PACKAGE_PATH_Debug=dist/Debug/Cygwin-Windows/package/tf02dualbuttoncallback.tar
# Release configuration
CND_PLATFORM_Release=Cygwin-Windows
CND_ARTIFACT_DIR_Release=dist/Release/Cygwin-Windows
CND_ARTIFACT_NAME_Release=tf02_dualbutton_callback
CND_ARTIFACT_PATH_Release=dist/Release/Cygwin-Windows/tf02_dualbutton_callback
CND_PACKAGE_DIR_Release=dist/Release/Cygwin-Windows/package
CND_PACKAGE_NAME_Release=tf02dualbuttoncallback.tar
CND_PACKAGE_PATH_Release=dist/Release/Cygwin-Windows/package/tf02dualbuttoncallback.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
