#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=Cygwin-Windows
CND_DLIB_EXT=dll
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/956494b8/brick_hat.o \
	${OBJECTDIR}/_ext/956494b8/brick_hat_zero.o \
	${OBJECTDIR}/_ext/956494b8/brick_master.o \
	${OBJECTDIR}/_ext/956494b8/bricklet_oled_128x64_v2.o \
	${OBJECTDIR}/_ext/956494b8/bricklet_rgb_led_button.o \
	${OBJECTDIR}/_ext/956494b8/ip_connection.o \
	${OBJECTDIR}/_ext/956494b8/la.o \
	${OBJECTDIR}/_ext/956494b8/la_config.o \
	${OBJECTDIR}/_ext/956494b8/la_display.o \
	${OBJECTDIR}/_ext/956494b8/la_hw.o \
	${OBJECTDIR}/_ext/956494b8/la_lamp.o \
	${OBJECTDIR}/_ext/956494b8/la_play.o \
	${OBJECTDIR}/_ext/956494b8/la_rule.o \
	${OBJECTDIR}/_ext/956494b8/la_solver.o \
	${OBJECTDIR}/_ext/956494b8/la_tf.o \
	${OBJECTDIR}/_ext/956494b8/logger.o \
	${OBJECTDIR}/_ext/956494b8/timer.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/tf_licht_aus_final.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/tf_licht_aus_final.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.c} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/tf_licht_aus_final ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/_ext/956494b8/brick_hat.o: ../../tf/source/brick_hat.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/brick_hat.o ../../tf/source/brick_hat.c

${OBJECTDIR}/_ext/956494b8/brick_hat_zero.o: ../../tf/source/brick_hat_zero.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/brick_hat_zero.o ../../tf/source/brick_hat_zero.c

${OBJECTDIR}/_ext/956494b8/brick_master.o: ../../tf/source/brick_master.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/brick_master.o ../../tf/source/brick_master.c

${OBJECTDIR}/_ext/956494b8/bricklet_oled_128x64_v2.o: ../../tf/source/bricklet_oled_128x64_v2.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/bricklet_oled_128x64_v2.o ../../tf/source/bricklet_oled_128x64_v2.c

${OBJECTDIR}/_ext/956494b8/bricklet_rgb_led_button.o: ../../tf/source/bricklet_rgb_led_button.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/bricklet_rgb_led_button.o ../../tf/source/bricklet_rgb_led_button.c

${OBJECTDIR}/_ext/956494b8/ip_connection.o: ../../tf/source/ip_connection.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/ip_connection.o ../../tf/source/ip_connection.c

${OBJECTDIR}/_ext/956494b8/la.o: ../../tf/source/la.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/la.o ../../tf/source/la.c

${OBJECTDIR}/_ext/956494b8/la_config.o: ../../tf/source/la_config.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/la_config.o ../../tf/source/la_config.c

${OBJECTDIR}/_ext/956494b8/la_display.o: ../../tf/source/la_display.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/la_display.o ../../tf/source/la_display.c

${OBJECTDIR}/_ext/956494b8/la_hw.o: ../../tf/source/la_hw.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/la_hw.o ../../tf/source/la_hw.c

${OBJECTDIR}/_ext/956494b8/la_lamp.o: ../../tf/source/la_lamp.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/la_lamp.o ../../tf/source/la_lamp.c

${OBJECTDIR}/_ext/956494b8/la_play.o: ../../tf/source/la_play.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/la_play.o ../../tf/source/la_play.c

${OBJECTDIR}/_ext/956494b8/la_rule.o: ../../tf/source/la_rule.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/la_rule.o ../../tf/source/la_rule.c

${OBJECTDIR}/_ext/956494b8/la_solver.o: ../../tf/source/la_solver.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/la_solver.o ../../tf/source/la_solver.c

${OBJECTDIR}/_ext/956494b8/la_tf.o: ../../tf/source/la_tf.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/la_tf.o ../../tf/source/la_tf.c

${OBJECTDIR}/_ext/956494b8/logger.o: ../../tf/source/logger.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/logger.o ../../tf/source/logger.c

${OBJECTDIR}/_ext/956494b8/timer.o: ../../tf/source/timer.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/timer.o ../../tf/source/timer.c

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
