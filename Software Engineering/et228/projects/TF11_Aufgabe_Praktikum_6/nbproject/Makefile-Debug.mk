#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=Cygwin-Windows
CND_DLIB_EXT=dll
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/956494b8/bricklet_dual_button.o \
	${OBJECTDIR}/_ext/956494b8/bricklet_rgb_led.o \
	${OBJECTDIR}/_ext/956494b8/bricklet_rotary_poti.o \
	${OBJECTDIR}/_ext/956494b8/contact_dual_button.o \
	${OBJECTDIR}/_ext/956494b8/dimmer_rotary.o \
	${OBJECTDIR}/_ext/956494b8/ip_connection.o \
	${OBJECTDIR}/_ext/956494b8/lamp_rgb.o \
	${OBJECTDIR}/_ext/956494b8/logger.o \
	${OBJECTDIR}/_ext/956494b8/switch_dual_button_unvollstaendig.o \
	${OBJECTDIR}/_ext/956494b8/tf.o \
	${OBJECTDIR}/_ext/956494b8/tf11.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/tf11_aufgabe_praktikum_6.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/tf11_aufgabe_praktikum_6.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.c} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/tf11_aufgabe_praktikum_6 ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/_ext/956494b8/bricklet_dual_button.o: ../../tf/source/bricklet_dual_button.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/bricklet_dual_button.o ../../tf/source/bricklet_dual_button.c

${OBJECTDIR}/_ext/956494b8/bricklet_rgb_led.o: ../../tf/source/bricklet_rgb_led.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/bricklet_rgb_led.o ../../tf/source/bricklet_rgb_led.c

${OBJECTDIR}/_ext/956494b8/bricklet_rotary_poti.o: ../../tf/source/bricklet_rotary_poti.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/bricklet_rotary_poti.o ../../tf/source/bricklet_rotary_poti.c

${OBJECTDIR}/_ext/956494b8/contact_dual_button.o: ../../tf/source/contact_dual_button.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/contact_dual_button.o ../../tf/source/contact_dual_button.c

${OBJECTDIR}/_ext/956494b8/dimmer_rotary.o: ../../tf/source/dimmer_rotary.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/dimmer_rotary.o ../../tf/source/dimmer_rotary.c

${OBJECTDIR}/_ext/956494b8/ip_connection.o: ../../tf/source/ip_connection.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/ip_connection.o ../../tf/source/ip_connection.c

${OBJECTDIR}/_ext/956494b8/lamp_rgb.o: ../../tf/source/lamp_rgb.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/lamp_rgb.o ../../tf/source/lamp_rgb.c

${OBJECTDIR}/_ext/956494b8/logger.o: ../../tf/source/logger.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/logger.o ../../tf/source/logger.c

${OBJECTDIR}/_ext/956494b8/switch_dual_button_unvollstaendig.o: ../../tf/source/switch_dual_button_unvollstaendig.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/switch_dual_button_unvollstaendig.o ../../tf/source/switch_dual_button_unvollstaendig.c

${OBJECTDIR}/_ext/956494b8/tf.o: ../../tf/source/tf.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/tf.o ../../tf/source/tf.c

${OBJECTDIR}/_ext/956494b8/tf11.o: ../../tf/source/tf11.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/tf11.o ../../tf/source/tf11.c

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
