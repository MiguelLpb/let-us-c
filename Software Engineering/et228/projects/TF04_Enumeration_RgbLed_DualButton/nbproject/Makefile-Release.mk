#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=Cygwin-Windows
CND_DLIB_EXT=dll
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/44969f1f/tf04_enumeration_rgbled_dualbutton.o \
	${OBJECTDIR}/_ext/956494b8/bricklet_dual_button.o \
	${OBJECTDIR}/_ext/956494b8/bricklet_rgb_led.o \
	${OBJECTDIR}/_ext/956494b8/ip_connection.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/tf04_enumeration_rgbled_dualbutton.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/tf04_enumeration_rgbled_dualbutton.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.c} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/tf04_enumeration_rgbled_dualbutton ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/_ext/44969f1f/tf04_enumeration_rgbled_dualbutton.o: ../../code/TF03_Enumeration/tf04_enumeration_rgbled_dualbutton.c
	${MKDIR} -p ${OBJECTDIR}/_ext/44969f1f
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/44969f1f/tf04_enumeration_rgbled_dualbutton.o ../../code/TF03_Enumeration/tf04_enumeration_rgbled_dualbutton.c

${OBJECTDIR}/_ext/956494b8/bricklet_dual_button.o: ../../tf/source/bricklet_dual_button.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/bricklet_dual_button.o ../../tf/source/bricklet_dual_button.c

${OBJECTDIR}/_ext/956494b8/bricklet_rgb_led.o: ../../tf/source/bricklet_rgb_led.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/bricklet_rgb_led.o ../../tf/source/bricklet_rgb_led.c

${OBJECTDIR}/_ext/956494b8/ip_connection.o: ../../tf/source/ip_connection.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/ip_connection.o ../../tf/source/ip_connection.c

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
