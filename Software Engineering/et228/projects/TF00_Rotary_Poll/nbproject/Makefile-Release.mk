#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=Cygwin-Windows
CND_DLIB_EXT=dll
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/4597ff82/tf00_rotary_poll.o \
	${OBJECTDIR}/_ext/956494b8/bricklet_rotary_poti.o \
	${OBJECTDIR}/_ext/956494b8/ip_connection.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/tf00_rotary_poll.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/tf00_rotary_poll.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.c} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/tf00_rotary_poll ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/_ext/4597ff82/tf00_rotary_poll.o: ../../code/TF00_Rotary_Poll/tf00_rotary_poll.c
	${MKDIR} -p ${OBJECTDIR}/_ext/4597ff82
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/4597ff82/tf00_rotary_poll.o ../../code/TF00_Rotary_Poll/tf00_rotary_poll.c

${OBJECTDIR}/_ext/956494b8/bricklet_rotary_poti.o: ../../tf/source/bricklet_rotary_poti.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/bricklet_rotary_poti.o ../../tf/source/bricklet_rotary_poti.c

${OBJECTDIR}/_ext/956494b8/ip_connection.o: ../../tf/source/ip_connection.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -O2 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/ip_connection.o ../../tf/source/ip_connection.c

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
