#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=Cygwin-Windows
CND_DLIB_EXT=dll
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/956494b8/lamp.o \
	${OBJECTDIR}/_ext/956494b8/switch.o \
	${OBJECTDIR}/_ext/956494b8/switch_dual_button.o \
	${OBJECTDIR}/_ext/956494b8/tf.o \
	${OBJECTDIR}/_ext/956494b8/tf05_structured.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/tf05_structured.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/tf05_structured.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.c} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/tf05_structured ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/_ext/956494b8/lamp.o: ../../tf/source/lamp.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -g -I../../tf/source -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/lamp.o ../../tf/source/lamp.c

${OBJECTDIR}/_ext/956494b8/switch.o: ../../tf/source/switch.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -g -I../../tf/source -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/switch.o ../../tf/source/switch.c

${OBJECTDIR}/_ext/956494b8/switch_dual_button.o: ../../tf/source/switch_dual_button.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -g -I../../tf/source -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/switch_dual_button.o ../../tf/source/switch_dual_button.c

${OBJECTDIR}/_ext/956494b8/tf.o: ../../tf/source/tf.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -g -I../../tf/source -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/tf.o ../../tf/source/tf.c

${OBJECTDIR}/_ext/956494b8/tf05_structured.o: ../../tf/source/tf05_structured.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -g -I../../tf/source -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/tf05_structured.o ../../tf/source/tf05_structured.c

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
