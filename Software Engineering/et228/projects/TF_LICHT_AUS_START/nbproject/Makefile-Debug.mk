#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=Cygwin-Windows
CND_DLIB_EXT=dll
CND_CONF=Debug
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/4e0e7d4e/la.o \
	${OBJECTDIR}/_ext/4e0e7d4e/la_config.o \
	${OBJECTDIR}/_ext/4e0e7d4e/la_display.o \
	${OBJECTDIR}/_ext/4e0e7d4e/la_hw.o \
	${OBJECTDIR}/_ext/4e0e7d4e/la_lamp.o \
	${OBJECTDIR}/_ext/4e0e7d4e/la_play.o \
	${OBJECTDIR}/_ext/4e0e7d4e/la_rule.o \
	${OBJECTDIR}/_ext/4e0e7d4e/la_tf.o \
	${OBJECTDIR}/_ext/4e0e7d4e/logger.o \
	${OBJECTDIR}/_ext/4e0e7d4e/timer.o \
	${OBJECTDIR}/_ext/956494b8/bricklet_oled_128x64_v2.o \
	${OBJECTDIR}/_ext/956494b8/bricklet_rgb_led_button.o \
	${OBJECTDIR}/_ext/956494b8/ip_connection.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/tf_licht_aus_start.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/tf_licht_aus_start.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.c} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/tf_licht_aus_start ${OBJECTFILES} ${LDLIBSOPTIONS}

${OBJECTDIR}/_ext/4e0e7d4e/la.o: ../../tf/TF_LICHT_AUS_ORIGINAL/la.c
	${MKDIR} -p ${OBJECTDIR}/_ext/4e0e7d4e
	${RM} "$@.d"
	$(COMPILE.c) -g -I../../tf/source -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/4e0e7d4e/la.o ../../tf/TF_LICHT_AUS_ORIGINAL/la.c

${OBJECTDIR}/_ext/4e0e7d4e/la_config.o: ../../tf/TF_LICHT_AUS_ORIGINAL/la_config.c
	${MKDIR} -p ${OBJECTDIR}/_ext/4e0e7d4e
	${RM} "$@.d"
	$(COMPILE.c) -g -I../../tf/source -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/4e0e7d4e/la_config.o ../../tf/TF_LICHT_AUS_ORIGINAL/la_config.c

${OBJECTDIR}/_ext/4e0e7d4e/la_display.o: ../../tf/TF_LICHT_AUS_ORIGINAL/la_display.c
	${MKDIR} -p ${OBJECTDIR}/_ext/4e0e7d4e
	${RM} "$@.d"
	$(COMPILE.c) -g -I../../tf/source -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/4e0e7d4e/la_display.o ../../tf/TF_LICHT_AUS_ORIGINAL/la_display.c

${OBJECTDIR}/_ext/4e0e7d4e/la_hw.o: ../../tf/TF_LICHT_AUS_ORIGINAL/la_hw.c
	${MKDIR} -p ${OBJECTDIR}/_ext/4e0e7d4e
	${RM} "$@.d"
	$(COMPILE.c) -g -I../../tf/source -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/4e0e7d4e/la_hw.o ../../tf/TF_LICHT_AUS_ORIGINAL/la_hw.c

${OBJECTDIR}/_ext/4e0e7d4e/la_lamp.o: ../../tf/TF_LICHT_AUS_ORIGINAL/la_lamp.c
	${MKDIR} -p ${OBJECTDIR}/_ext/4e0e7d4e
	${RM} "$@.d"
	$(COMPILE.c) -g -I../../tf/source -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/4e0e7d4e/la_lamp.o ../../tf/TF_LICHT_AUS_ORIGINAL/la_lamp.c

${OBJECTDIR}/_ext/4e0e7d4e/la_play.o: ../../tf/TF_LICHT_AUS_ORIGINAL/la_play.c
	${MKDIR} -p ${OBJECTDIR}/_ext/4e0e7d4e
	${RM} "$@.d"
	$(COMPILE.c) -g -I../../tf/source -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/4e0e7d4e/la_play.o ../../tf/TF_LICHT_AUS_ORIGINAL/la_play.c

${OBJECTDIR}/_ext/4e0e7d4e/la_rule.o: ../../tf/TF_LICHT_AUS_ORIGINAL/la_rule.c
	${MKDIR} -p ${OBJECTDIR}/_ext/4e0e7d4e
	${RM} "$@.d"
	$(COMPILE.c) -g -I../../tf/source -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/4e0e7d4e/la_rule.o ../../tf/TF_LICHT_AUS_ORIGINAL/la_rule.c

${OBJECTDIR}/_ext/4e0e7d4e/la_tf.o: ../../tf/TF_LICHT_AUS_ORIGINAL/la_tf.c
	${MKDIR} -p ${OBJECTDIR}/_ext/4e0e7d4e
	${RM} "$@.d"
	$(COMPILE.c) -g -I../../tf/source -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/4e0e7d4e/la_tf.o ../../tf/TF_LICHT_AUS_ORIGINAL/la_tf.c

${OBJECTDIR}/_ext/4e0e7d4e/logger.o: ../../tf/TF_LICHT_AUS_ORIGINAL/logger.c
	${MKDIR} -p ${OBJECTDIR}/_ext/4e0e7d4e
	${RM} "$@.d"
	$(COMPILE.c) -g -I../../tf/source -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/4e0e7d4e/logger.o ../../tf/TF_LICHT_AUS_ORIGINAL/logger.c

${OBJECTDIR}/_ext/4e0e7d4e/timer.o: ../../tf/TF_LICHT_AUS_ORIGINAL/timer.c
	${MKDIR} -p ${OBJECTDIR}/_ext/4e0e7d4e
	${RM} "$@.d"
	$(COMPILE.c) -g -I../../tf/source -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/4e0e7d4e/timer.o ../../tf/TF_LICHT_AUS_ORIGINAL/timer.c

${OBJECTDIR}/_ext/956494b8/bricklet_oled_128x64_v2.o: ../../tf/source/bricklet_oled_128x64_v2.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/bricklet_oled_128x64_v2.o ../../tf/source/bricklet_oled_128x64_v2.c

${OBJECTDIR}/_ext/956494b8/bricklet_rgb_led_button.o: ../../tf/source/bricklet_rgb_led_button.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/bricklet_rgb_led_button.o ../../tf/source/bricklet_rgb_led_button.c

${OBJECTDIR}/_ext/956494b8/ip_connection.o: ../../tf/source/ip_connection.c
	${MKDIR} -p ${OBJECTDIR}/_ext/956494b8
	${RM} "$@.d"
	$(COMPILE.c) -g -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/_ext/956494b8/ip_connection.o ../../tf/source/ip_connection.c

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
