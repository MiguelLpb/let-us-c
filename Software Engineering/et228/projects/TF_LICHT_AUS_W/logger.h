/* 
 * File:   logger.h
 * 
 * Responsibilities
 * -- provide logger capabilities
 * -- only logging to console
 */

#ifndef LOGGER_H
#define LOGGER_H

/*
 * definition of available log levels
 */
typedef enum
{
    FLOW, INFO, ERROR, NONE, PROMPT
} log_level_t;

/*
 * set log level threshold
 * -- lvl: level to set
 * 
 * initially set to ERROR
 */

extern void log_set_level(log_level_t lvl);

/*
 * log a message  followed by new line
 * -- lvl: log level of the message
 * -- s: the message
 * 
 * logging if lvl >= currently set level
 */
extern void log_msg(log_level_t lvl, const char * s);

/*
 * no line break
 */
extern void log_prompt(const char * s);


/*
 * log entering a function at level INFO 
 * -- s: the function name
 */
extern void log_entry(const char * s);


#endif /* LOGGER_H */

