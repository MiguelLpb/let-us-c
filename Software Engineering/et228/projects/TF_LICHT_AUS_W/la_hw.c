
/* 
 * File:   la_game_hw.c
 * 
 * Responsibilities 
 * -- implement promised API 
 * 
 * Note: depends on TF HW
 */

#include "la_hw.h"
#include "la_config.h"
#include "la_tf.h"

#include <stdlib.h>

extern void la_hw_init()
{
}

static int tf_is_ready();

extern int la_hw_is_ready()
{
    return (
            la_config_output_to_console() && la_config_input_from_console()
            ) 
            || tf_is_ready();
}

static int tf_is_ready()
{
    for(int i = 0; i < NO_OF_LAMPS; i++)
    {
        if (!la_tf_button_is_valid (i) )
        {
            return 0;
        }
    }
    return la_tf_display_is_valid(); 
}    
