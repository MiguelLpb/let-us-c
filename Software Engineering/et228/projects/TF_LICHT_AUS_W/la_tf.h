
/* 
 * File:   la_tf.h
 * 
 * responsibilities
 * -- provide information about TF HW
 * 
 * Note: tf header files are not included here
 */

#ifndef LA_TF_H
#define LA_TF_H

#include <stdint.h>


/* 
 * function called whenever a button is released 
 * <nr> 0..6 number of button 
 */
typedef void (*tf_button_released_callback_t)(int nr);

/*
 * register the callback <cb> to be called whenever a button is released
 */
extern void la_tf_register (tf_button_released_callback_t cb); 

/*
 * perform any necessary initialisation
 */
extern void la_tf_init();


extern int la_tf_display_is_valid();

extern int la_tf_button_is_valid(int nr);

extern void la_tf_button_set_light(int nr, uint8_t red, uint8_t green, uint8_t blue);

#endif /* LA_TF_H */

