/* 
 * File:   timer.h
 * 
 * Responsibilities 
 * -- provide timing capabilities
 */

#ifndef TIMER_H
#define TIMER_H

#include <sys/time.h>

#ifdef __CYGWIN__
#include <windows.h>
#elif _POSIX_C_SOURCE >= 199309L
#include <time.h>   // for nanosleep
#else
#include <unistd.h> // for usleep
#endif


/*
 * sleep for the given duration
 * milliseconds: duratiom in ms to sleep
 */
extern void timer_sleep_ms (unsigned int milliseconds); 

/*
 * returns difference in ms between given arguments t1 and t0
 * t0: earlier
 * t1: later
 * if t0 is "after" t1, the result will be negative
 */
double timer_get_difference_ms(struct timeval t0, struct timeval t1);


#endif /* TIMER_H */

