
/* 
 * File:   la_display.c
 * 
 * Responsibilities 
 * -- implement promised API 
 * 
 * Note: depends on HW
 */

#include <stdio.h>
#include <string.h>

#include "la_display.h"
#include "la_config.h"

#include "logger.h"

extern void la_display_init()
{
    log_entry(__FUNCTION__);
}

extern void la_display_msg(char * msg)
{
    log_entry(__FUNCTION__);
    if (la_config_output_to_console())
    {
        log_msg(INFO, msg);
    }
}

extern void la_display_lamps(lamp_t * lamps)
{
    log_entry(__FUNCTION__);

    if (la_config_output_to_console())
    {
        char msg[100];
        sprintf(msg, "lamps 0..%d: ", NO_OF_LAMPS-1);
        for (int nr = 0; nr < NO_OF_LAMPS; nr++)
        {
            char state[10];
            sprintf(state, "%3s ", lamps[nr].is_on ? "ON" : "--");
            strcat(msg, state);
        }
        log_msg(INFO, msg);
    }

}

extern void la_display_won()
{
    log_entry(__FUNCTION__);
    if (la_config_output_to_console())
    {
        log_msg(INFO, " === WON === ");
    }
}
