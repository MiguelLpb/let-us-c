
/* 
 * File:   la_config.h
 * 
 * responsibilities 
 * -- read command line arguments
 * -- read configuration file la_config.txt if present in current directory 
 * -- provide information about configuration set by command line arguments
 * 
 * options
 * -c do start with console mode
 * -l set log level threshhold to lvl 0 (INFO), 1 (ERROR) 2 (NONE)
 * -s slow loop 
 * -h TF host
 * 
 * decisisions
 * -- this is the central point of configuration
 */

#ifndef LA_CONFIG_H
#define LA_CONFIG_H

#define NO_OF_LAMPS (7) 

extern const unsigned int HW_FAILURE_DELAY_MS;

extern void la_config_init(int argc, char** argv);


/*
 * returns TF host
 */
extern char * la_config_get_tf_host();

/*
 * returns if output to console is initially wanted
 */
extern int la_config_output_to_console();

extern int la_config_input_from_console();

extern int la_config_log_level_as_int();

/*
 * returns the polling delay of the game in ms
 */
extern unsigned int la_config_get_game_polling_delay_ms();


#endif /* LA_CONFIG_H */

