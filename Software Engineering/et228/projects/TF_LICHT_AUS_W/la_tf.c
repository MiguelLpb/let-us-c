
/* 
 * File:   la_tf.c
 * 
 * Responsibilities 
 * 
 * -- implement promised API 
 */
#include "la_tf.h"

#include <stdio.h>
#include <stdint.h>

#include "la_config.h"

#include "logger.h"
#include "timer.h"

#include "ip_connection.h"
#include "bricklet_rgb_led_button.h"
#include "bricklet_oled_128x64_v2.h"

#ifdef __CYGWIN__
#include <windows.h>
#endif

#define UID_LEN (8)

static IPConnection ipcon;

// Display

typedef struct
{
    OLED128x64V2 oled;
    int valid;
    char uid[UID_LEN];
} display_t;

static display_t display;

typedef struct
{
    RGBLEDButton button;
    int valid;
    char uid[UID_LEN];
} rgb_button_t;

// RGB Buttons
static rgb_button_t buttons[7];

/*
 * external callback to be set by users 
 */
static tf_button_released_callback_t button_released_callback;

extern void la_tf_register(tf_button_released_callback_t cb)
{
    button_released_callback = cb;
}


/*
 * internal callbacks to be registered at tf 
 */
static void cb_connect(uint8_t connect_reason, void *user_data);
static void cb_disconnect(uint8_t connect_reason, void *user_data);
static void cb_enumerate(const char *uid, const char *connected_uid,
        char position, uint8_t hardware_version[3],
        uint8_t firmware_version[3], uint16_t device_identifier,
        uint8_t enumeration_type, void *user_data);

static void check_for_connection(const char * uid, char position);
static void set_uid(const char * connected_uid, char * uid);
static void check_for_disconnection(const char * uid);


#define PORT (4223)

extern void la_tf_init()
{
    log_entry(__FUNCTION__);
    /* Create IP connection */

    button_released_callback = 0;

    display.valid = 0;
    for (int i = 0; i < NO_OF_LAMPS; i++)
    {
        buttons[i].valid = 0;
    }
    ipcon_create(&ipcon);

    /* Connect to brickd */
    char * host;
    if (ipcon_connect(&ipcon, host = la_config_get_tf_host(), PORT) < 0)
    {
        log_msg(ERROR, "Error: Could not connect to brickd. Exit.\n");
        exit(EXIT_FAILURE);
    }
    else
    {
        char msg[100];
        sprintf(msg, "TF connected to %s", host);
        log_msg(INFO, msg);
    }
    /* Register enumeration callback to "cb_enumerate" */
    ipcon_register_callback(&ipcon,
            IPCON_CALLBACK_ENUMERATE,
            (void *) cb_enumerate,
            NULL);

    ipcon_register_callback(&ipcon, IPCON_CALLBACK_CONNECTED, (void *) cb_connect, NULL);
    ipcon_register_callback(&ipcon, IPCON_CALLBACK_DISCONNECTED, (void *) cb_disconnect, NULL);
#ifdef __CYGWIN__
    log_msg(INFO, "Compiled with CYGWIN. Wait a moment...");
    timer_sleep_ms(500);
    log_msg(INFO, "... now enumerating");
    ipcon_enumerate(&ipcon);
#endif
}

static void cb_connect(uint8_t connect_reason, void *user_data)
{
    log_entry(__FUNCTION__);

    ipcon_enumerate(&ipcon);
}

static void cb_disconnect(uint8_t connect_reason, void *user_data)
{
    log_entry(__FUNCTION__);

    for (int i = 0; i < NO_OF_LAMPS; i++)
    {
        buttons[i].valid = 0;
        rgb_led_button_destroy(&buttons[i].button);
    }
    display.valid = 0;
    oled_128x64_v2_destroy(&display.oled );
}

static void cb_enumerate(const char *uid, const char *connected_uid,
        char position, uint8_t hardware_version[3],
        uint8_t firmware_version[3], uint16_t device_identifier,
        uint8_t enumeration_type, void *user_data)
{
    log_entry(__FUNCTION__);

    switch (enumeration_type) {
        case IPCON_ENUMERATION_TYPE_DISCONNECTED:
            check_for_disconnection(uid);
            break;
        case IPCON_ENUMERATION_TYPE_CONNECTED:
        case IPCON_ENUMERATION_TYPE_AVAILABLE:
            check_for_connection(uid, position);
            break;
        default:
            /* illegal enumeration_type */
            break;
    }
}

/*
 * ToDo : fewer statements
 */

static void check_for_connection(const char * uid, char position)
{
    log_entry(__FUNCTION__);
    /*
    ToDo : make this short! 
   switch (position)
   {
       case 'a':
           if (!display_valid)
           {
               oled_128x64_v2_create(&display, uid, &ipcon);
               display_valid = 1;
               set_uid(uid, display_uid);
               //Display loeschen?
           }
           break;
       case 'b':
           if (!buttons[2].button_valid)
           {
               rgb_led_button_create(&button2, uid, &ipcon);
               button2_valid = 1;
               set_uid(uid, button2_uid);
               rgb_led_button_register_callback(&button2,
                       RGB_LED_BUTTON_CALLBACK_BUTTON_STATE_CHANGED,
                       (void (*)(void))cb_button_state_changed,
                       &uid);
           }
           break;
       case 'c':
           if (!button3_valid)
           {
               rgb_led_button_create(&button3, uid, &ipcon);
               button3_valid = 1;
               set_uid(uid, button3_uid);
               rgb_led_button_register_callback(&button3,
                       RGB_LED_BUTTON_CALLBACK_BUTTON_STATE_CHANGED,
                       (void (*)(void))cb_button_state_changed,
                       &uid);
           }
           break;
       case 'd':
           if (!button4_valid)
           {
               rgb_led_button_create(&button4, uid, &ipcon);
               button4_valid = 1;
               set_uid(uid, button4_uid);
               rgb_led_button_register_callback(&button4,
                       RGB_LED_BUTTON_CALLBACK_BUTTON_STATE_CHANGED,
                       (void (*)(void))cb_button_state_changed,
                       &uid);
           }
           break;
       case 'e':
           if (!button1_valid)
           {
               rgb_led_button_create(&button1, uid, &ipcon);
               button1_valid = 1;
               set_uid(uid, button1_uid);
               rgb_led_button_register_callback(&button1,
                       RGB_LED_BUTTON_CALLBACK_BUTTON_STATE_CHANGED,
                       (void (*)(void))cb_button_state_changed,
                       &uid);
           }
           break;
       case 'f':
           if (!button0_valid)
           {
               rgb_led_button_create(&button0, uid, &ipcon);
               button0_valid = 1;
               set_uid(uid, button0_uid);
               rgb_led_button_register_callback(&button0,
                       RGB_LED_BUTTON_CALLBACK_BUTTON_STATE_CHANGED,
                       (void (*)(void))cb_button_state_changed,
                       &uid);
           }
           break;
       case 'g':
           if (!button6_valid)
           {
               rgb_led_button_create(&button6, uid, &ipcon);
               button6_valid = 1;
               set_uid(uid, button6_uid);
               rgb_led_button_register_callback(&button6,
                       RGB_LED_BUTTON_CALLBACK_BUTTON_STATE_CHANGED,
                       (void (*)(void))cb_button_state_changed,
                       &uid);
           }
           break;
       case 'h':
           if (!button5_valid)
           {
               rgb_led_button_create(&button5, uid, &ipcon);
               button5_valid = 1;
               set_uid(uid, button5_uid);
               rgb_led_button_register_callback(&button5,
                       RGB_LED_BUTTON_CALLBACK_BUTTON_STATE_CHANGED,
                       (void (*)(void))cb_button_state_changed,
                       &uid);
           }
           break;

       default:
           log_msg(ERROR, &position);
           break;



   }
     */
}

static void set_uid(const char * connected_uid, char * uid)
{
    log_entry(__FUNCTION__);

#ifdef __CYGWIN__
    strncpy(uid, connected_uid, UID_LEN);
#else    
    strlcpy(uid, connected_uid, UID_LEN);
#endif
}

static void check_for_disconnection(const char * uid)
{
    for (int i = 0; i < NO_OF_LAMPS; i++)
    {
        if (!strcmp(uid, buttons[i].uid))
        {
            buttons[i].valid = 0;
            rgb_led_button_destroy(&buttons[i].button);
            return;
        }
    }
    if (!strcmp(uid, display.uid))
    {
        display.valid = 0;
        oled_128x64_v2_destroy(& (display.oled) );
    }
}

extern int la_tf_display_is_valid()
{
    return display.valid;
}

extern int la_tf_button_is_valid(int nr)
{
    if ((nr >= 0) && (nr < NO_OF_LAMPS))
    {
        return buttons[nr].valid;
    }
    return 0;
}

extern void la_tf_button_set_light(int nr, uint8_t red, uint8_t green, uint8_t blue)
{
    if ((nr >= 0) && (nr < NO_OF_LAMPS))
    {
        rgb_led_button_set_color(& (buttons[nr].button),  red,  green,  blue);
    }
}
