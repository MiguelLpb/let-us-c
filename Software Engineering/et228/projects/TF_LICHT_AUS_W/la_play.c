
/* 
 * File:   la_play.c
 * 
 * Responsibilities 
 * -- implement promised API 
 * 
 * Note: does NOT on HW
 */
#include "la_play.h"

#include "logger.h"
#include "timer.h"
#include "stdio.h"
#include "stdlib.h"

#include "la_config.h"
#include "la_hw.h"
#include "la_lamp.h"
#include "la_rule.h"
#include "la_display.h"

/*
 * basic game states
 * 
 * remark: future modes (e.g. fight count down) may require additional 
 * states (e.g. LOST)
 */
typedef enum
{
    HW_IS_NOT_READY, INIT, PLAY, WON,
} la_state_t;


static la_state_t handle_HW_IS_NOT_READY();
static la_state_t handle_INIT();
static la_state_t handle_PLAY();
static la_state_t handle_WON();
static la_state_t handle_illegal_state(la_state_t state);


/*
 * play the game
 * 
 * notes: 
 * -- this is a loop to make timed events possible
 * -- the loop handles the current state using switch .. case
 * -- the handling functions return the next state, which can be the same
 * -- async behaviour is handled by callbacks in the hw specific parts
 * todo
 * -- if hw becomes unavailable and available again, this has to happen smoothly
 * -- right now: start over (INIT) when hw becomes available
 */
extern void la_play()
{
    log_entry(__FUNCTION__);

    la_state_t la_state = INIT;
    while (1)
    {
        switch (la_state)
        {
            case HW_IS_NOT_READY:
                la_state = handle_HW_IS_NOT_READY();
                break;
            case INIT:
                la_state = handle_INIT();
                break;
            case PLAY:
                la_state = handle_PLAY();
                break;
            case WON:
                la_state = handle_WON();
                break;
            default:
                la_state = handle_illegal_state(la_state);
                break;
        }
        if (!la_hw_is_ready())
        {
            la_state = HW_IS_NOT_READY;
        }
        log_entry ("timer_sleep_ms");
        timer_sleep_ms(la_config_get_game_polling_delay_ms());
        log_entry (" EXIT timer_sleep_ms");
        
    }
}


static la_state_t handle_HW_IS_NOT_READY()
{
    log_entry(__FUNCTION__);
    
    return la_hw_is_ready() ? INIT : HW_IS_NOT_READY;
}


static la_state_t handle_INIT()
{
    log_entry(__FUNCTION__);
    la_display_msg ("starting new game");
    return PLAY;
}

static int get_no_of_lamp_to_switch();


static la_state_t handle_PLAY()
{
    la_state_t result = PLAY;

    int nr = get_no_of_lamp_to_switch();
    if (nr >= 0)
    {
        la_lamp_toogle_lamp(nr);
        la_lamp_display_all();
    }
    if (la_lamp_get_no_off_lamps_on() == 0)
    {
        result = WON;
    }
    return result;
}


static int get_no_of_lamp_to_switch()
{
    int lamp_to_switch = -1 ; 
    if (la_config_input_from_console())
    {
        char c;
        do
        {
            char msg[100];
            sprintf(msg, "no of lamp (0..%d): ", NO_OF_LAMPS-1);
            log_prompt (msg);
            c = getchar();
            getchar(); /* read enter char */
        }
        while (c < '0' || c > ('0' + NO_OF_LAMPS-1));
        lamp_to_switch = c - '0';
    }
    else
    {
        for (int i = 0; i < NO_OF_LAMPS; i++)
        {
            if (la_lamp_to_toggle(i))
            {
                lamp_to_switch = i;
                break;
            }
        }
    }
    return lamp_to_switch; 
}


static la_state_t handle_WON()
{
    log_entry(__FUNCTION__);
    la_display_won();
    timer_sleep_ms(1000);
    return INIT;
}


la_state_t handle_illegal_state(la_state_t state)
{
    log_entry(__FUNCTION__);

    char msg[100];
    sprintf(msg, "illegal state %d", state);
    log_msg(ERROR, msg);
    return INIT;
}
