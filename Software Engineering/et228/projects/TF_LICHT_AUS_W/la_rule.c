
/* 
 * File:   la_rule.c
 * 
 * Responsibilities 
 * -- implement promised API 
 */

#include "la_rule.h"
#include "logger.h"
#include <stdio.h>


extern void la_rule_toogle_lamp(lamp_t  * lamps, int nr)
{
    log_entry(__FUNCTION__);
    
    (lamps + nr)->is_on = !(lamps + nr)->is_on;
}
