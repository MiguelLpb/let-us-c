
/* 
 * File:   licht_aus.c
 * 
 * Responsibilities 
 * -- initialize everything
 * -- start play when ready
 */

#include <stdio.h>
#include <stdlib.h>

#include "logger.h"
#include "timer.h"

#include "la_config.h"
#include "la_tf.h"
#include "la_display.h"
#include "la_lamp.h"
#include "la_hw.h"
#include "la_play.h"


static void init(int argc, char** argv);

int main(int argc, char** argv)
{
    init(argc, argv);
    while (1)
    {
        if (!la_hw_is_ready())
        {
            log_msg(INFO, "Insufficient hardware.");
            while (!la_hw_is_ready())
            {
                timer_sleep_ms(HW_FAILURE_DELAY_MS);
            }
            log_msg(INFO,"Hardware is ready. Let's play!!!");
        }
        la_play();
    }
    return (EXIT_SUCCESS);
}

static void init (int argc, char** argv)
{
    la_config_init(argc, argv);
    log_set_level((log_level_t) la_config_log_level_as_int() );
    log_msg(INFO, "starting 'Licht Aus!'");
    la_tf_init();
    la_display_init();
    la_lamp_init();
}
