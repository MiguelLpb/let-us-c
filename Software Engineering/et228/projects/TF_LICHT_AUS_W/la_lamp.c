
/* 
 * File:   la_lamp.c
 * 
 * Responsibilities 
 * -- implement promised API 
 * 
 * Note: depends on HW
 */
#include "la_lamp.h"
#include "la_rule.h"
#include "la_display.h"
#include "la_tf.h"
#include "la_config.h"
#include "logger.h"

#include <string.h>
#include <stdio.h>
#include <stdint.h>

/*
 * the array of 7 lamps 
 */
static lamp_t lamps[NO_OF_LAMPS];

static void button_callback(int nr)
{
    lamps[nr].has_been_pressed = 1;
}

extern int la_lamp_is_ready(int nr)
{
    return la_tf_button_is_valid(nr);
}

typedef struct
{
    uint8_t R;
    uint8_t G;
    uint8_t B;
} rgb_t;

static rgb_t color_on = {127, 127, 127};
static rgb_t color_off = {0, 0, 0};

void la_lamp_toogle_lamp(int nr)
{
    log_entry(__FUNCTION__);

    la_rule_toogle_lamp(lamps, nr);
    char msg[100];
    sprintf(msg, "lamp %d to %s", nr, lamps[nr].is_on ? "on" : "off");
    la_display_msg(msg);
    for (int i = 1; i < NO_OF_LAMPS; i++)
    {
        if (la_tf_button_is_valid(i))
        {
            if (lamps[i].is_on)
            {
                la_tf_button_set_light(i, color_on.R, color_on.G, color_on.B);
            }
            else
            {
                la_tf_button_set_light(i, color_on.R, color_off.G, color_off.B);
            }
        }
    }
}

void la_lamp_init()
{
    log_entry(__FUNCTION__);

    la_tf_register(button_callback);
}

extern void la_lamp_display_all()
{

    log_entry(__FUNCTION__);

    la_display_lamps(lamps);
}

extern int la_lamp_get_no_off_lamps_on()
{
    log_entry(__FUNCTION__);

    int n = 0;
    for (int i = 0; i < NO_OF_LAMPS; i++)
    {
        if (lamps[i].is_on)
        {
            n++;
        }
    }
    return n;
}

extern int la_lamp_to_toggle(int nr)
{
    log_entry(__FUNCTION__);

    int res = 0 ; 
    if ((nr >= 0) && (nr < NO_OF_LAMPS))
    {
        res = lamps[nr].has_been_pressed;
        lamps[nr].has_been_pressed = 0;
    }
    return res;
}