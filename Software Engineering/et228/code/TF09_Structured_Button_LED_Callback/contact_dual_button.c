
/* 
 * File:   contact_dual_button.c
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 4. Mai 2019, 16:24
 */
#include <stdio.h>

#include "contact.h"
#include "logger.h"
#include "tf.h"

static contact_callback_t contact_callback;
static void on_dual_button_state_changed_right(uint8_t button_l, uint8_t button_r);


extern void contact_init(void)
{
    log_entry("%s\n", __FUNCTION__);
    contact_callback = 0;
}


extern void contact_subscribe(contact_callback_t cb)
{
    log_entry("%s\n", __FUNCTION__);
    contact_callback = cb;
    tf_register_dual_button_cb_client_right(on_dual_button_state_changed_right);

}


static void on_dual_button_state_changed_right(uint8_t button_l, uint8_t button_r)
{
    if (button_r == DUAL_BUTTON_BUTTON_STATE_PRESSED)
    {
        log_entry("%s\n", "Right button pressed");
        dual_button_set_selected_led_state(
                tf_get_dual_button(),
                DUAL_BUTTON_LED_STATE_AUTO_TOGGLE_OFF,
                DUAL_BUTTON_LED_STATE_ON);
    }
    if (button_r == DUAL_BUTTON_BUTTON_STATE_RELEASED)
    {
        log_entry("%s\n", "Right button pressed");
        dual_button_set_selected_led_state(
                tf_get_dual_button(),
                DUAL_BUTTON_LED_STATE_AUTO_TOGGLE_OFF,
                DUAL_BUTTON_LED_STATE_OFF);
        contact_callback();
    }
}
