
/* 
 * File:   logger.c
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 23. März 2020, 21:36
 */

#include"logger.h"
#include "stdio.h"

int do_logging = 0;
extern  void log_entry (char* format, const char * s)
{
    if (do_logging)
    {
        printf (format, s);
    }
}