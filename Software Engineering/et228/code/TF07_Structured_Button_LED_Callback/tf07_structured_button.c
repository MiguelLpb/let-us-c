
/* 
 * File:   tf05_structured_extended.c
 * Author: uwe
 *
 * Created on 13. April 2017, 11:09
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 


#include "tf.h"
#include "lamp.h"
#include "switch.h"

static void show_info();

static void on_button_change_function();

#define LAMP_OFF 0
#define LAMP_ON 1

static int lamp_state;

int main(int argc, char **argv)
{
    show_info();
    tf_init();

    switch_init();
    switch_subscribe(on_button_change_function);

    lamp_state = LAMP_OFF;
    lamp_to_off();

    while (1)
    {
        sleep(1);
    }
    tf_end();
    return (EXIT_SUCCESS);
}

static void on_button_change_function()
{
    switch (lamp_state) {
        case LAMP_ON:
            printf("Lamp to off.\n");
            lamp_to_off();
            lamp_state = LAMP_OFF;
            break;
        case LAMP_OFF:
            printf("Lamp to on.\n");
            lamp_to_on();
            lamp_state = LAMP_ON;
            break;
        default:
            printf("Unknown state of lamp!\n");
    }
}

static void show_info()
{
    printf("Example Program: %s\n\n", __FILE__);
    printf("Hardware recognition: Dynamically detecting Rotary Bricklet\n");
    printf("Application logic ..: Callback\n");
    printf("Source files .......: Multiple (Modular)\n\n");
    printf("Left button switches real LED on and off again!\n\n");
    printf("Press return to start ... ");
    getchar();
    printf("\n");
}




