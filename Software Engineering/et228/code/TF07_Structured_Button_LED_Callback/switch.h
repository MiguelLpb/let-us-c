
/* 
 * File:   switch.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on February 16, 2018, 9:02 AM
 */

#ifndef SWITCH_H
#define SWITCH_H

typedef void (*switch_callback_t)(void) ;

extern int switch_is_on();

extern void switch_init(void);

extern void switch_subscribe(switch_callback_t cb);


#endif /* SWITCH_H */

