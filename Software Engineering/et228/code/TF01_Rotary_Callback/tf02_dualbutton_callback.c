//Miguel L.
//Aufgabe zu Praktikum 03

#include <stdio.h>
#include <unistd.h> 

#include "ip_connection.h"
#include "bricklet_dual_button.h"

#define HOST "localhost"
#define PORT 4223
#define UID "vUL" // Change XYZ to the UID of your Rotary Poti Bricklet

static IPConnection ipcon;
static DualButton db;

int init();
void callback();
void loop();
static void beenden_db();

int main(int argc, char **argv) {
    init();
    loop();
    beenden_db();
    return (EXIT_SUCCESS);
}

init() {
    ipcon_create(&ipcon); /* Create IP connection */
    dual_button_create(&db, UID, &ipcon); /* Create device object */
    if (ipcon_connect(&ipcon, HOST, PORT) < 0) /* Connect to brickd */ {
        fprintf(stderr, "Error: Could not connect to brickd. Exit.\n");
        exit(EXIT_FAILURE);
    }
    fprintf(stderr, "Verbindung läuft\n");
    dual_button_set_led_state(&db, 3, 3); //Auto Toggle off für beide LEDS
    dual_button_register_callback(&db, DUAL_BUTTON_CALLBACK_STATE_CHANGED, (void *) callback, NULL); //Callback registrieren
}

callback(uint8_t button_l, uint8_t button_r, uint8_t led_l, uint8_t led_r) {
    if (button_l == DUAL_BUTTON_BUTTON_STATE_PRESSED) {
        if (led_r == DUAL_BUTTON_LED_STATE_OFF) {
            dual_button_set_selected_led_state(&db, 1, 2); //LED anschalten
            fprintf(stderr, "LED ON\n");
        } else {
            dual_button_set_selected_led_state(&db, 1, 3); // LED ausschalten
            fprintf(stderr, "LED OFF\n");
        }
    }
}

loop() {
    while (1) {
        usleep(50);
    }
}

beenden_db() {
    dual_button_destroy(&db);
    ipcon_destroy(&ipcon);
}
