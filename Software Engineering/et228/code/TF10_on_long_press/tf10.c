

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h> 
#include <time.h>

#include "tf.h"
#include "lamp.h"
#include "switch.h"


static void show_info();

static void on_button_change_function();

void on_bricklet_state_changed(uint8_t button_l, uint8_t button_r, uint8_t led_l, uint8_t led_r,
        void *user_data);

#define LAMP_OFF 0
#define LAMP_ON 1

static int lamp_state;

int main(int argc, char **argv)
{
    printf("%s\n", __FUNCTION__);

    show_info();
    tf_init();

    switch_init();
    switch_subscribe(on_button_change_function);

    lamp_state = LAMP_OFF;
    lamp_to_off();

    while (1)
    {
        sleep(1);
    }


    tf_exit();
    return (EXIT_SUCCESS);
}

static void show_info();

static void on_button_change_function();

void on_bricklet_state_changed(uint8_t button_l, uint8_t button_r, uint8_t led_l, uint8_t led_r,
        void *user_data);

static void on_button_change_function()
{
    printf("%s\n", __FUNCTION__);

    switch (lamp_state) {
        case LAMP_ON:
            printf("Lamp to off.\n");
            lamp_to_off();
            lamp_state = LAMP_OFF;
            break;
        case LAMP_OFF:
            printf("Lamp to on.\n");
            lamp_to_on();
            lamp_state = LAMP_ON;

            break;
        default:
            printf("Unknown state of lamp!\n");
    }
}

static void show_info()
{
    printf("%s\n", __FUNCTION__);

    printf("Example Program: %s\n\n", __FILE__);
    printf("Hardware recognition: Dynamically detecting Rotary Bricklet\n");
    printf("Application logic ..: Callback and Hysteresis\n");
    printf("Source files .......: Multiple (Modular)\n");
    printf("Light is toggled after pressing the button for more than 'some' seconds.\n\n");
    printf("Press return to start ... ");
    getchar();
    printf("\n");
}

