/* 
 * File:   light.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on February 16, 2018, 9:04 AM
 */

#ifndef LIGHT_H
#define LIGHT_H

extern void lamp_to_on();
extern void lamp_to_off();

#endif /* LIGHT_H */

