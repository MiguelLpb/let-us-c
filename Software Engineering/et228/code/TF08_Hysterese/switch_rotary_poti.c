
/* 
 * File:   switch_rotary_poti.c
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on February 16, 2018, 9:03 AM
 */

#include <stdio.h>

#include "switch.h"
#include "tf.h"

static switch_callback_t notify;
static unsigned int state = 0;


extern void switch_init(void)
{
    notify = 0;
}


extern void switch_subscribe(switch_callback_t cb)
{
    notify = cb;
}


#define LIGHT_TO_ON_POTI_VALUE 20
#define LIGHT_TO_OFF_POTI_VALUE -20


void cb_position_has_changed(int16_t position, void *user_data)
{
    (void) user_data; // avoid unused parameter warning
    printf("Position: %+4hd, state: %u\n", position, state);
    if (state)
    {
        if (position < LIGHT_TO_OFF_POTI_VALUE)
        {
            state = 0;
        }
    }
    else
    {
        if (position > LIGHT_TO_ON_POTI_VALUE)
        {
            state = 1;
        }
    }
    if (notify != 0)
    {
        notify();
    }
}


extern int switch_is_on()
{
    return state;
}