
/* 
 * File:   tf.c
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on February 16, 2018, 9:05 AM
 */

#include <stdio.h>
#include "ip_connection.h"
#ifdef __CYGWIN__
#include <unistd.h>
#endif

/*
 * TF specific defines 
 */
#define HOST "localhost"
#define PORT 4223

static IPConnection ipcon;

#include "ip_connection.h"
#include "bricklet_rotary_poti.h"
#include "bricklet_dual_button.h"
#include "bricklet_rgb_led.h"

/* Print incoming enumeration information */
static void cb_enumerate(const char *uid, const char *connected_uid,
        char position, uint8_t hardware_version[3],
        uint8_t firmware_version[3], uint16_t device_identifier,
        uint8_t enumeration_type, void *user_data);

static void cb_connect(uint8_t connect_reason, void *user_data);
static void cb_disconnect(uint8_t connect_reason, void *user_data);


void cb_position_has_changed(int16_t position, void *user_data);


RotaryPoti rp;
int rotary_poti_is_valid;
static char rotary_poti_uid[8];

RGBLED rl;
int rgb_led_is_valid;
static char rgb_led_uid[8];

extern void tf_init()
{
    /* */
    rotary_poti_is_valid = 0;
    rgb_led_is_valid = 0;

    /* Create IP connection */
    ipcon_create(&ipcon);

    /* Connect to brickd */
    if (ipcon_connect(&ipcon, HOST, PORT) < 0)
    {
        fprintf(stderr, "Error: Could not connect to brickd. Exit.\n");
        exit(EXIT_FAILURE);
    }
    /* Register enumeration callback to "cb_enumerate" */
    ipcon_register_callback(&ipcon,
            IPCON_CALLBACK_ENUMERATE,
            (void *) cb_enumerate,
            NULL);
    ipcon_register_callback(&ipcon, IPCON_CALLBACK_CONNECTED, (void *) cb_connect, NULL);
    ipcon_register_callback(&ipcon, IPCON_CALLBACK_DISCONNECTED, (void *) cb_disconnect, NULL);
#ifdef __CYGWIN__
    printf("Compiled with CYGWIN. Wait, then enumerate ...\n");
    sleep(1);
    ipcon_enumerate(&ipcon);
#endif
}


extern void tf_end()
{
    ipcon_destroy(&ipcon);
}


static void cb_connect(uint8_t connect_reason, void *user_data)
{
    printf("Connected to demon. Enumeration requested.\n");
    ipcon_enumerate(&ipcon);
}


static void cb_disconnect(uint8_t connect_reason, void *user_data)
{
    printf("Disconnected from demon.\n");
}


static void cb_enumerate(const char *uid, const char *connected_uid,
        char position, uint8_t hardware_version[3],
        uint8_t firmware_version[3], uint16_t device_identifier,
        uint8_t enumeration_type, void *user_data)
{
    (void) user_data;

    if (enumeration_type == IPCON_ENUMERATION_TYPE_DISCONNECTED)
    {
        if (rotary_poti_is_valid)
        {
            if (!strcmp(uid, rotary_poti_uid))
            {
                rotary_poti_is_valid = 0;
                rotary_poti_destroy(&rp);
                printf(" %s %s disconnected.\n", ROTARY_POTI_DEVICE_DISPLAY_NAME, uid);
            }
        }
        if (rgb_led_is_valid)
        {
            if (!strcmp(uid, rgb_led_uid))
            {
                rgb_led_is_valid = 0;
                rgb_led_destroy(&rl);
                printf(" %s %s disconnected.\n", RGB_LED_DEVICE_DISPLAY_NAME, uid);
            }
        }
    }
    else
    {
        if (device_identifier == ROTARY_POTI_DEVICE_IDENTIFIER)
        {
            printf(" %s %s available.\n", ROTARY_POTI_DEVICE_DISPLAY_NAME, uid);
            rotary_poti_is_valid = 1;
            rotary_poti_create(&rp, uid, &ipcon);
            strcpy(rotary_poti_uid, uid);
            rotary_poti_register_callback(&rp,
                                  ROTARY_POTI_CALLBACK_POSITION,
                                  (void *) cb_position_has_changed,
                                  NULL);
            rotary_poti_set_position_callback_period(&rp, 50);
        }
        else if (device_identifier == RGB_LED_DEVICE_IDENTIFIER)
        {
            printf(" %s %s available.\n", RGB_LED_DEVICE_DISPLAY_NAME, uid);
            rgb_led_is_valid = 1;
            rgb_led_create(&rl, uid, &ipcon);
            strcpy(rgb_led_uid, uid);
            rgb_led_set_rgb_value(&rl, 0, 0, 0);
        }
    }
}
