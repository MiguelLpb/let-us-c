#ifndef  JOYSTICKS_H
#define JOYSTICKS_H

#include "orientation.h"


extern int joystick_has_been_released(orientation_t orientation);

extern orientation_t joystick_wants_move(orientation_t orientation);

extern void joystick_reset();

extern int joysticks_one_is_long_pressed ();



extern void cb_released_left(void *user_data);

extern void cb_pressed_left(void *user_data);

extern void cb_released_right(void *user_data);

extern void cb_pressed_right(void *user_data);



extern void cb_position_reached_left(int16_t x, int16_t y, void *user_data);

extern void cb_position_reached_right(int16_t x, int16_t y, void *user_data);



#endif