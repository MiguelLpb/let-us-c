
/* 
 * File:   mem_status.c
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 20. Juni 2018, 18:53
 */

#include "mem_status.h"

#include <sys/time.h>

#include "delay.h"
#include "rgb_matrix.h"

/*
 * In erster Zeile Rechts  aktueller Level binaer
 *      1
 *     10
 *     11
 *    100
 *    101
 *    110
 *    111 
 *   1000
 *   1001
 *   1010
 *   1011
 *   1100
 */
extern void mem_status_show (int current, int total)
{
    
}


static struct timeval last_start_toggle_time;

static const unsigned int TOGGLE_STARTING_TIME = 75;

static int field_starting_position = 0;
static int field_starting_is_visible = 0;


extern void mem_status_draw_starting()
{
    struct timeval now;

    gettimeofday(&now, 0);
    double dif = timedifference_ms(last_start_toggle_time, now);
    if (dif >= TOGGLE_STARTING_TIME)
    {
        last_start_toggle_time = now;
        if (field_starting_is_visible)
        {
            rgb_matrix_set_dot(0, field_starting_position, BLACK);
            rgb_matrix_set_dot(1, field_starting_position++, BLACK);
            field_starting_is_visible = 0;
            field_starting_position %= RGB_MATRIX_WIDTH;
        }
        else
        {
            rgb_matrix_set_dot(0, field_starting_position, WHITE);
            rgb_matrix_set_dot(1, field_starting_position, WHITE);
            field_starting_is_visible = 1;
        }
    }
}


extern void mem_status_clear_starting()
{
    rgb_matrix_set_dot(0, field_starting_position, BLACK);
    rgb_matrix_set_dot(1, field_starting_position, BLACK);
}

static struct timeval last_draw_toggle_time;

static const unsigned int TOGGLE_FIELD_TIME = 312;
static int field_wait_position = 0;
static int field_wait_is_visible = 0;


extern void mem_status_draw_wait()
{
    struct timeval now;

    gettimeofday(&now, 0);
    double dif = timedifference_ms(last_draw_toggle_time, now);
    if (dif >= TOGGLE_FIELD_TIME)
    {
        last_draw_toggle_time = now;
        if (field_wait_is_visible)
        {
            rgb_matrix_set_dot(1, field_wait_position++, BLACK);
            field_wait_is_visible = 0;
            field_wait_position %= RGB_MATRIX_WIDTH;
        }
        else
        {
            rgb_matrix_set_dot(1, field_wait_position, WHITE);
            field_wait_is_visible = 1;
        }
    }
}


extern void mem_status_clear_wait()
{
    rgb_matrix_set_dot(1, field_wait_position, BLACK);
}


extern void mem_status_clear_wait_and_reset()
{
    mem_status_clear_wait();
    field_wait_position = 0;
    field_wait_is_visible = 0;
}

static uint8_t status[RGB_MATRIX_SIZE] = {
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};

static void build_line(color_t color)
{
    rgb_matrix_draw_figure_by_color(status,  color);
}


#define WAIT_LOST_TIME 50


extern void mem_status_show_lost()
{
    build_line(RED);
    sleep_ms(WAIT_LOST_TIME);
}

#define WAIT_TIMEOUT_TIME 500


extern void mem_status_show_timeout()
{
    build_line(YELLOW);
    sleep_ms(WAIT_TIMEOUT_TIME);
}

#define WAIT_WON_TIME 250


extern void mem_status_show_won()
{
    build_line(GREEN);
    sleep_ms(WAIT_WON_TIME);
}


extern void mem_status_clear()
{
    build_line(BLACK);
}

