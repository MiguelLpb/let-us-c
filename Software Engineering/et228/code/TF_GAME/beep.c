
/* 
 * File:   beep.c
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 1. Mai 2018, 18:30
 */

#include "beep.h"
#include "tf.h"


// int piezo_speaker_beep(PiezoSpeaker *piezo_speaker, uint32_t duration, uint16_t frequency) 

void beep()
{
    if (piezo_speaker_is_valid)
    {
        piezo_speaker_beep(&piezo_speaker, 100, 1000);
    }
}

void beep_f(uint16_t frequency)
{
    if (piezo_speaker_is_valid)
    {
        piezo_speaker_beep(&piezo_speaker, 100, frequency);
    }
}
