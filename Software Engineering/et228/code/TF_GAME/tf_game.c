#include <stdio.h>
#include <stdlib.h>


#include "tf.h"
#include "games.h"

#include "c4.h"

#include "delay.h"

static int tf_game_aborted_by_player();

const unsigned int HW_FAILURE_DELAY = 20; 


static int get_game();

int main(void)
{
    games_show_info();
    tf_init();
    while (1)
    {
        if (tf_game_aborted_by_player())
        {
            printf ("Exit by user. Gaming has ended. Goodbye!\n");
            break;
        }
        else if (!games_hw_ready())
        {
            printf("Insufficient hardware. Waiting ... \n");
            while (!games_hw_ready())
            {
                sleep_ms(HW_FAILURE_DELAY);
            }
            printf("\nHardware is ready. Let's play!!!\n\n");
        }
        tf_game_t * game = games_get_game();
        games_play_game(game);
    }
    void tf_exit();
    return (EXIT_SUCCESS);
}


static int tf_game_aborted_by_player()
{
    return 0;
}





