
/* 
 * File:   arrow.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 7. April 2018, 09:59
 */

#ifndef ARROW_H
#define ARROW_H


#include "orientation.h"
#include "colors.h"

extern void arrow_toggle(orientation_t o, color_t c);

extern void arrow_pulse(orientation_t o, color_t c);

#endif /* ARROW_H */

