
/* 
 * File:   rgb_matrix.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 5. April 2018, 12:00
 */

#ifndef RGB_MATRIX_H
#define RGB_MATRIX_H

#include "colors.h"
#include <stdint.h>

#define RGB_MATRIX_HEIGHT (8)
#define RGB_MATRIX_WIDTH  (8)
#define RGB_MATRIX_SIZE   ((RGB_MATRIX_WIDTH) * (RGB_MATRIX_HEIGHT) ) 


extern void rgb_matrix_draw_by_rgb(uint8_t red[RGB_MATRIX_SIZE], uint8_t green[RGB_MATRIX_SIZE], uint8_t blue[RGB_MATRIX_SIZE] );

// extern void rgb_matrix_draw(color_t color[RGB_MATRIX_SIZE]);


extern void rgb_matrix_draw_figure_by_color(uint8_t figure[RGB_MATRIX_SIZE], color_t color) ;

extern void rgb_matrix_clear();

extern void rgb_matrix_set_dot (uint8_t x, uint8_t y, color_t color);

extern void rgb_matrix_callback(uint32_t frame_number, void *user_data);

#endif /* RGB_MATRIX_H */

