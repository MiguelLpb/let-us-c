
/* 
 * File:   c4_tie.c
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on April 9, 2018, 3:18 PM
 */

#include <stdio.h>
#include <sys/time.h>


#include "c4_tie.h"
#include "c4_field.h"

#include "delay.h"
#include "rgb_matrix.h"

extern int tie_is_tie()
{
    uint8_t column = field_get_free_top_column();
    int is_tie = (column >= C4_FIELD_WIDTH);
    return is_tie;
}


static uint8_t tie_matrix[RGB_MATRIX_SIZE] = {
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 1, 0, 0, 1, 0, 0,
    0, 1, 1, 0, 0, 1, 1, 0,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    0, 1, 1, 0, 0, 1, 1, 0,
    0, 0, 1, 0, 0, 1, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
};

static const unsigned int TOGGLE_TIE_FREQUENCY = 100;

static struct timeval last_toggle_time;

extern void tie_toggle()
{
    static int tie_is_visible = 0;

    struct timeval now;
    gettimeofday(&now, 0);
    double dif = timedifference_ms(last_toggle_time, now);
    if (dif >= TOGGLE_TIE_FREQUENCY)
    {
        last_toggle_time = now;
        if (tie_is_visible)
        {
            rgb_matrix_clear();
            tie_is_visible = 0;
        }
        else
        {
            rgb_matrix_draw_figure_by_color(tie_matrix, WHITE);
            tie_is_visible = 1;
        }
    }

}

