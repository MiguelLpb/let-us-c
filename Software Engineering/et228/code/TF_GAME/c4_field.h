#ifndef C4_FIELD_H
#define C4_FIELD_H

#define C4_FIELD_HEIGHT (6)
#define C4_FIELD_WIDTH  (7)
#define C4_FIELD_SIZE ( (C4_FIELD_HEIGHT) * (C4_FIELD_WIDTH)) 

#include <stdlib.h>

#include "colors.h"

extern void field_empty();

extern uint8_t field_get_free_top_column();

extern void field_display();

extern void field_put (uint8_t x, uint8_t y, color_t color);

extern color_t field_get (uint8_t x, uint8_t y);

extern int field_below_is_free(uint8_t row, uint8_t column);


extern void field_clear (color_t  field[C4_FIELD_HEIGHT][C4_FIELD_WIDTH]);

extern int field_extract (color_t destination[C4_FIELD_HEIGHT][C4_FIELD_WIDTH], color_t color);

extern int field_is_usable_column(uint8_t column);


#endif