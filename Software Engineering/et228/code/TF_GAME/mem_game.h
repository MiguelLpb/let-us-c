
/* 
 * File:   mem_game.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 3. Juni 2018, 17:13
 */

#ifndef MEM_GAME_H
#define MEM_GAME_H

#include <stdlib.h>
#include <sys/time.h>

#define MEM_X_SIZE  (4)
#define MEM_Y_SIZE  (3)
#define MEM_SIZE  (MEM_X_SIZE * MEM_Y_SIZE)

/*
 * MEM_X_SIZE=4, MEM_Y_SIZE=3
 * 
 * Array (x,y) and index index
 *                  X 
 *         |    0   1   2   3
 * --------------------------
 *      0  |    0   1   2   3
 *  Y   1  |    4   5   6   7
 *      2  |    8   9   10  11
 * 
 * mem_field_t is (x, y, index, color) (Both index / x,y is planned redundancy!)
 * 
 * mem_game_t is an array 0 .. MEM_SIZE of of mem_field_t 
 * 
 * A "position" in the game is an index 0 ... MEM_SIZE in a mem_game_t 
 */

typedef struct _field
{
    uint8_t x;
    uint8_t y;
    uint8_t index;
    uint8_t color;
} mem_field_t;

typedef struct mem_game_
{
    uint8_t positions_to_memorize; /* aka "level" 1 .. MEM_SIZE    */
    uint8_t current_position; /* 1 .. fields_to_remember      */
    struct timeval last_action_end_time;
    mem_field_t mem_field[MEM_SIZE];

} mem_game_t;

extern void mem_game_init(mem_game_t *mem_game);

extern int mem_game_next_level(mem_game_t *mem_game);


extern void mem_game_draw_by_touch_field_index(mem_game_t *mem_game, uint8_t index);

extern void mem_game_clear_by_touch_field_index(mem_game_t *mem_game, uint8_t index);


extern uint8_t mem_game_get_solution_field_index(mem_game_t *mem_game);

#endif /* MEM_GAME_H */

