
/* 
 * File:   mem.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 1. Juni 2018, 20:51
 */

#ifndef MEM_H
#define MEM_H


extern void mem();

extern int mem_hw_is_ready();

extern void mem_show_info();



#endif /* MEM_H */

