/* 
 * File:   c4_win.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on April 9, 2018, 3:16 PM
 */

#ifndef C4_WIN_H
#define C4_WIN_H


extern int win_is_won();


#endif /* C4_WIN_H */

