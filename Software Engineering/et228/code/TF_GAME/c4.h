
#ifndef C4_CONTROL_H
#define C4_CONTROL_H


#include "colors.h"
#include "orientation.h"



extern void c4();

extern int c4_hw_is_ready();

extern void c4_show_info();

#endif