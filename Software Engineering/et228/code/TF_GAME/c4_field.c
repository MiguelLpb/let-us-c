#include <stdio.h>
#include <stdlib.h>
#include <time.h>         
#include "c4_field.h"
#include "c4_disk.h"

#include "c4_win.h"
#include "c4_game.h"
#include "c4_timing.h"

#include "colors.h"
#include "rgb_matrix.h"

static color_t field[C4_FIELD_SIZE];


static uint8_t translate_xy(uint8_t row, uint8_t column)
{
    return row * (C4_FIELD_WIDTH) + column;
}


static void translate_index(uint8_t idx, uint8_t * column, uint8_t * row)
{
    *column = idx % (C4_FIELD_WIDTH);
    *row = idx / C4_FIELD_WIDTH;
}


extern void field_empty()
{
    for (int i = 0; i < C4_FIELD_SIZE; i++)
    {
        field[i] = BLACK;
    }
    rgb_matrix_clear();
}


static int top_of_colum_is_free(uint8_t column)
{
    return (field[translate_xy(0, column)] == BLACK);
}


extern uint8_t field_get_free_top_column()
{
    uint8_t candidate_column = C4_FIELD_WIDTH / 2;
    if (top_of_colum_is_free(candidate_column))
    {
        return candidate_column;
    }
    for (uint8_t offset = 1; offset < C4_FIELD_WIDTH / 2; offset++)
    {
        candidate_column = C4_FIELD_WIDTH / 2 - offset;
        if (top_of_colum_is_free(candidate_column))
        {
            return candidate_column;
        }
        candidate_column = C4_FIELD_WIDTH / 2 + offset;
        if (top_of_colum_is_free(candidate_column))
        {
            return candidate_column;
        }
    }
    return C4_FIELD_WIDTH + 1;
}

static const uint8_t MAX_INTENSITY = 255;


extern void field_display()
{
    uint8_t field_red[C4_FIELD_SIZE];
    uint8_t field_blue[C4_FIELD_SIZE];
    uint8_t field_green[C4_FIELD_SIZE];
    for (int i = 0; i < C4_FIELD_SIZE; i++)
    {
        switch (field[i])
        {
            case RED:
                field_red[i] = MAX_INTENSITY;
                break;
            case GREEN:
                field_green[i] = MAX_INTENSITY;
                break;
            case BLUE:
                field_blue[i] = MAX_INTENSITY;
                break;
            case WHITE:
                field_red[i] = MAX_INTENSITY;
                field_green[i] = MAX_INTENSITY;
                field_blue[i] = MAX_INTENSITY;
                break;
            default:
                field_red[i] = 0;
                field_green[i] = 0;
                field_blue[i] = 0;
                break;
        }
        rgb_matrix_draw_by_rgb(field_red, field_green, field_blue);
    }
}


extern color_t field_get(uint8_t row, uint8_t column)
{

    uint8_t zz = translate_xy(row, column);
    color_t c = *(field + translate_xy(row, column));

    return * (field + translate_xy(row, column));
}


extern void field_put(uint8_t row, uint8_t column, color_t color)
{
    uint8_t zz = translate_xy(row, column);
    * (field + translate_xy(row, column)) = color;
}


extern int field_extract(color_t destination[C4_FIELD_HEIGHT][C4_FIELD_WIDTH], color_t color)
{
    int n = 0;
    for (int r = 0; r < C4_FIELD_HEIGHT; r++)
    {
        for (int c = 0; c < C4_FIELD_WIDTH; c++)
        {
            int idx = translate_xy(r, c);
            if (field[idx] == color)
            {
                n++;
                destination[r][c] = color;
            }
            else
            {
                destination[r][c] = BLACK;
            }
        }
    }

    return n;
}


extern int field_below_is_free(uint8_t row, uint8_t column)
{
    uint8_t next_row = row + 1;
    return (next_row < C4_FIELD_HEIGHT) && (field_get(next_row, column) == BLACK);
}


extern int field_is_usable_column(uint8_t column)
{
    return field[translate_xy(0, column)] != BLACK;
}


