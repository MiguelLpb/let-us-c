
/* 
 * File:   arrow.c
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 7. April 2018, 10:00
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "arrow.h"

#include "rgb_matrix.h"
#include "orientation.h"
#include "colors.h"
#include "delay.h"

static const unsigned int TOGGLE_ARROW_TIME = 2000;

static const unsigned int PULSE_ARROW_TIME = 50;


static uint8_t arrow_left_matrix[RGB_MATRIX_SIZE] = {
    0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00,
    0x00, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0x00, 0xFF, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00, 0x00,
};


static uint8_t arrow_right_matrix[RGB_MATRIX_SIZE] = {
    0x00, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0x00,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF,
    0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0x00,
    0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0xFF, 0x00, 0x00, 0x00,
};

static struct timeval last_toggle_time;


extern void arrow_toggle(orientation_t p, color_t c)
{
    static int arrow_is_visible = 0;

    struct timeval now;
    gettimeofday(&now, 0);
    double dif = timedifference_ms(last_toggle_time, now);
    if (dif >= TOGGLE_ARROW_TIME)
    {
        last_toggle_time = now;
        if (arrow_is_visible)
        {
            rgb_matrix_clear();
            arrow_is_visible = 0;
            printf("arrow invisible\n");
        }
        else
        {
            if (p == LEFT)
            {
                rgb_matrix_draw_figure_by_color(arrow_left_matrix, c);
            }
            else
            {
                rgb_matrix_draw_figure_by_color(arrow_right_matrix, c);
            }
            printf("arrow visible\n");
            arrow_is_visible = 1;
        }
    }
}

static uint8_t arrow_matrix[RGB_MATRIX_SIZE];


static int next_intensity (int intensity);
static void set_arrow_intensity (uint8_t * arrow_matrix, int intensity);

extern void arrow_pulse(orientation_t p, color_t c)
{
    static int arrow_intensity = 0;
    struct timeval now;
    gettimeofday(&now, 0);
    double dif = timedifference_ms(last_toggle_time, now);
    if (dif >= PULSE_ARROW_TIME)
    {
        if (p == LEFT)
        {
            memcpy(arrow_matrix, arrow_left_matrix, 64 * sizeof (uint8_t));
        }
        else
        {
            memcpy(arrow_matrix, arrow_right_matrix, 64 * sizeof (uint8_t));
        }
        arrow_intensity = next_intensity(arrow_intensity);
        set_arrow_intensity(arrow_matrix, arrow_intensity);
        rgb_matrix_draw_figure_by_color(arrow_matrix, c);
    }
}

#define MIN_INTENSITY 0x00
#define MAX_INTENSITY 0x0FF
static int next_intensity (int intensity)
{
    static int higher = 1;
    if (higher)
    {
        intensity++;
        if (intensity >= MAX_INTENSITY)
        {
            intensity = MAX_INTENSITY;
            higher = 0;
        }
    }
    else 
    {
        intensity--;
        if (intensity <= MIN_INTENSITY)
        {
            intensity = MIN_INTENSITY;
            higher = 1;
        }
           
    }
    return intensity;
}

static void set_arrow_intensity (uint8_t * arrow_matrix, int intensity)
{
    for (int i = 1; i < RGB_MATRIX_SIZE; i++)
    {
        if (*arrow_matrix != 0)
        {
            *arrow_matrix = intensity; 
        }
        arrow_matrix++;
    }
}


