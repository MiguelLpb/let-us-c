
/* 
 * File:   delay.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on April 6, 2018, 11:12 AM
 */

#ifndef DELAY_H
#define DELAY_H

#include <stdio.h>
#include <sys/time.h>

#ifdef __CYGWIN__
#include <windows.h>
#elif _POSIX_C_SOURCE >= 199309L
#include <time.h>   // for nanosleep
#else
#include <unistd.h> // for usleep
#endif


extern void sleep_ms (unsigned int milliseconds); 

double timedifference_ms(struct timeval t0, struct timeval t1);

#endif /* DELAY_H */

