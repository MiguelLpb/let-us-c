
/* 
 * File:   ambientlight.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 1. Mai 2018, 18:43
 */

#ifndef AMBIENTLIGHT_H
#define AMBIENTLIGHT_H

#include <stdint.h>

#define AMBIENT_LIGHT_CHANGE_INTERVALL 50

extern uint8_t ambientlight_adjusted_brightness(uint8_t brightness);

extern void ambientlight_callback(uint16_t illuminance, void *user_data);

#endif /* AMBIENTLIGHT_H */

