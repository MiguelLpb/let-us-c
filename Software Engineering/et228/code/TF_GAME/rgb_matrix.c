
/* 
 * File:   rgb_matrix.c
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 5. April 2018, 11:59
 */

#include <stdio.h>
#include <stdlib.h>

#include "tf.h"
#include "rgb_matrix.h"
#include "ambientlight.h"

#include "colors.h"

static uint8_t zeros[RGB_MATRIX_SIZE] = {};

#define RGB_ROWS 8
#define RGB_COLUMNS 8

static uint8_t buffer_red[RGB_MATRIX_SIZE];
static uint8_t buffer_green[RGB_MATRIX_SIZE];
static uint8_t buffer_blue[RGB_MATRIX_SIZE];


/*
static int continue_flag = 0;


extern void rgb_matrix_callback(uint32_t frame_number, void *user_data)
{
    continue_flag = 1;
}
 */

#define RGB_LED_MATRIX_CALLBACK_ID 42


static void adjust_brighness(uint8_t *, int);

static uint8_t adjusted_buffer_red[RGB_MATRIX_SIZE];
static uint8_t adjusted_buffer_green[RGB_MATRIX_SIZE];
static uint8_t adjusted_buffer_blue[RGB_MATRIX_SIZE];


static void rgb_matrix_update()
{
    if (rgb_matrix_is_valid)
    {
        /*
                if (continue_flag)
         */
        {
            memcpy(adjusted_buffer_red, buffer_red, RGB_MATRIX_SIZE * sizeof (uint8_t));
            memcpy(adjusted_buffer_green, buffer_green, RGB_MATRIX_SIZE * sizeof (uint8_t));
            memcpy(adjusted_buffer_blue, buffer_blue, RGB_MATRIX_SIZE * sizeof (uint8_t));

            adjust_brighness((uint8_t *) & adjusted_buffer_red, RGB_MATRIX_SIZE);
            rgb_led_matrix_set_red(&rgb_led_matrix, adjusted_buffer_red);

            adjust_brighness((uint8_t *) & adjusted_buffer_green, RGB_MATRIX_SIZE);
            rgb_led_matrix_set_green(&rgb_led_matrix, adjusted_buffer_green);

            adjust_brighness((uint8_t *) & adjusted_buffer_blue, RGB_MATRIX_SIZE);
            rgb_led_matrix_set_blue(&rgb_led_matrix, adjusted_buffer_blue);

            /*
                        continue_flag = 0;
             */
        }
        rgb_led_matrix_draw_frame(&rgb_led_matrix);
    }
}


static void adjust_brighness(uint8_t * buffer, int buffersize)
{
    for (int i = 0; i < buffersize; i++)
    {
        *buffer = ambientlight_adjusted_brightness(*buffer);
        buffer++;
    }
}


extern void rgb_matrix_draw_by_rgb(uint8_t red[RGB_MATRIX_SIZE], uint8_t green[RGB_MATRIX_SIZE], uint8_t blue[RGB_MATRIX_SIZE])
{
    memcpy(buffer_red, red, RGB_MATRIX_SIZE * sizeof (uint8_t));
    memcpy(buffer_green, green, RGB_MATRIX_SIZE * sizeof (uint8_t));
    memcpy(buffer_blue, blue, RGB_MATRIX_SIZE * sizeof (uint8_t));
    rgb_matrix_update();
}


extern void rgb_matrix_draw_by_color(color_t color[RGB_MATRIX_SIZE])
{
    rgb_matrix_update();
}


extern void rgb_matrix_clear()
{
    memcpy(buffer_red, zeros, RGB_MATRIX_SIZE * sizeof (uint8_t));
    memcpy(buffer_green, zeros, RGB_MATRIX_SIZE * sizeof (uint8_t));
    memcpy(buffer_blue, zeros, RGB_MATRIX_SIZE * sizeof (uint8_t));
    rgb_matrix_update();
}


extern void rgb_matrix_draw_figure_by_color(uint8_t figure[RGB_MATRIX_SIZE], color_t color)
{
    uint8_t r, g, b;
    switch (color)
    {
        case RED:
            memcpy(buffer_red, figure, RGB_MATRIX_SIZE * sizeof (uint8_t));
            break;
        case GREEN:
            memcpy(buffer_green, figure, RGB_MATRIX_SIZE * sizeof (uint8_t));
            break;
            memcpy(buffer_blue, figure, RGB_MATRIX_SIZE * sizeof (uint8_t));
            break;
        case WHITE:
            memcpy(buffer_red, figure, RGB_MATRIX_SIZE * sizeof (uint8_t));
            memcpy(buffer_green, figure, RGB_MATRIX_SIZE * sizeof (uint8_t));
            memcpy(buffer_blue, figure, RGB_MATRIX_SIZE * sizeof (uint8_t));
            break;
        case BLACK:
            rgb_matrix_clear();
            return;
        case LIME:
        case YELLOW:
        case CYAN:
        case MAGENTA:
        case MAROON:
        case OLIVE:
        case PURPLE:
        case TEAL:
        case NAVY:
            colors_color_to_rgb(color, &r, &g, &b);
            memcpy(buffer_red, figure, RGB_MATRIX_SIZE * sizeof (uint8_t));
            memcpy(buffer_green, figure, RGB_MATRIX_SIZE * sizeof (uint8_t));
            memcpy(buffer_blue, figure, RGB_MATRIX_SIZE * sizeof (uint8_t));
            for (uint8_t i = 0; i < RGB_MATRIX_SIZE; i++)
            {
                buffer_red[i] *= r;
                buffer_green[i] *= g;
                buffer_blue[i] *= b;
            }
            break;
        default:
            break;
    }
    rgb_matrix_update();
}


static uint8_t translate_xy(uint8_t x, uint8_t y)
{
    return x * RGB_ROWS + y;
}


static void translate_index(uint8_t idx, uint8_t * x, uint8_t * y)
{
    *x = idx % RGB_ROWS;
    *y = idx / RGB_COLUMNS;
}


extern void rgb_matrix_set_dot(uint8_t x, uint8_t y, color_t color)
{

    uint8_t idx = translate_xy(x, y);

    buffer_red[idx] = 0;
    buffer_green[idx] = 0;
    buffer_blue[idx] = 0;
    colors_color_to_rgb(color, &buffer_red[idx], &buffer_green[idx], &buffer_blue[idx]);
    rgb_matrix_update();
}