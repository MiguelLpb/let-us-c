
/* 
 * File:   c4_win.c
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on April 9, 2018, 5:11 PM
 */


#include <stdio.h>
#include <sys/time.h>


#include "c4_win.h"
#include "c4_field.h"
#include "c4_timing.h"
#include "c4_disk.h"
#include "c4_game.h"
#include "c4_game.h"

#include "orientation.h"
#include "delay.h"
#include "rgb_matrix.h"


static int is_outside(uint8_t row, uint8_t col)
{
    return (col < 0) || (col >= C4_FIELD_WIDTH) || (row < 0) || (row >= C4_FIELD_HEIGHT);
}

static int direction_unit[][2] = {
    {0, -1},
    {1, -1},
    {1, 0},
    {1, 1}
};


static int check(color_t color, uint8_t row, uint8_t col, color_t winner_field[C4_FIELD_HEIGHT][C4_FIELD_WIDTH])
{
    int is_won = 0;

    for (int direction = 0; direction < 4; direction++)
    {
        int matching_disks = 1;
        int dc = direction_unit[direction][0];
        int dr = direction_unit[direction][1];
        for (int plus_minus = -1; plus_minus <= 1; plus_minus += 2)
        {
            int col_step = dc * plus_minus;
            int row_step = dr * plus_minus;
            for (int distance = 1; distance <= 5; distance++)
            {
                int c = col + distance * col_step;
                int r = row + distance * row_step;
                if (is_outside(r, c))
                {
                    break;
                }
                if (field_get(r, c) == color)
                {
                    matching_disks++;
                }
                else
                {
                    break;
                }
            }
            if (matching_disks >= 4)
            {
                is_won = 1;
            }
        }
    }
    return is_won;
}


extern int win_is_won(player_t p)
{
    color_t winner_field[C4_FIELD_HEIGHT][C4_FIELD_WIDTH];
    int is_won = 0;

    color_t color = c4_player_to_color(p);

    int n = field_extract(winner_field, color);

    for (uint8_t col = 0; col < C4_FIELD_WIDTH; col++)
    {
        for (uint8_t row = 0; row < C4_FIELD_HEIGHT; row++)
        {
            color_t c = field_get(row, col);
            if (field_get(row, col) == color)
            {
                if (check(color, row, col, winner_field))
                {
                    printf("winner field found: column %hhu, row %hhu\n", col, row);
                    return 1;
                }
            }
        }
    }
    return is_won;
}
