
/* 
 * File:   tf_games.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 1. Juni 2018, 17:05
 */

#ifndef TF_GAMES_H
#define TF_GAMES_H


typedef struct _tf_game
{
    char name[20];
    void (*start)(void);
    int (*hw_ready)(void);
    void (*show_info)(void);
    
} tf_game_t;




extern int games_hw_ready();

extern void games_show_info();

extern unsigned int games_get_no_of_games();

extern void games_play_game(tf_game_t * game);

extern tf_game_t * games_get_game();

#endif /* TF_GAMES_H */

