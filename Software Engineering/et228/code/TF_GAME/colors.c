
/* 
 * File:   colors.c
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 19. Juni 2018, 14:25
 */

#include <stdlib.h>

#include "colors.h"

/*
        Black   #000000 (0,0,0)
        White#  FFFFFFF (255,255,255)
 
        Red	#FF0000	(255,0,0)
        Lime	#00FF00	(0,255,0)
        Blue	#0000FF	(0,0,255)
 
        Yellow	#FFFF00	(255,255,0)
        Cyan 	#00FFFF	(0,255,255)
        Magenta #FF00FF	(255,0,255)

        Maroon	#800000	(128,0,0)
        Olive	#808000	(128,128,0)
        Green	#008000	(0,128,0)
        Purple	#800080	(128,0,128)
        Teal	#008080	(0,128,128)
        Navy	#000080	(0,0,128)
 */

typedef struct _rgb
{
    uint8_t r; 
    uint8_t g; 
    uint8_t b;
    char * name;
} rgb_t;

static rgb_t rgb_values[] = {
    {0x00,  0x00,   0x00, "Black"}, 
    {0xFF,  0xFF,   0xFF, "White"},
    
    {0xFF,  0x00,   0x00, "Red"},
    {0x00,  0xFF,   0x00, "Lime"},
    {0x00,  0x00,   0xFF, "Blue"},
    
    {0xFF,  0xFF,   0x00, "Yellow"},
    {0x00,  0xFF,   0xFF, "Cyan"},
    {0xFF,  0x00,   0xFF, "Magenta"},
    
    {0x8F,  0x00,   0x00, "Maroon"},
    {0x8F,  0x8F,   0x00, "Olive"},
    {0x00,  0x8F,   0x00, "Green"},
    {0x8F,  0x00,   0x8F, "Purple"},
    {0x00,  0x8F,   0x8F, "Teal"},
    {0x00,  0x00,   0x8F, "Navy"},
};


extern void colors_color_to_rgb(color_t c, uint8_t * r, uint8_t * g, uint8_t * b)
{
    int index = c;
    *r = rgb_values[index].r;
    *g = rgb_values[index].g;
    *b = rgb_values[index].b;
}

extern char * colors_get_name (color_t c)
{
    int index = c;
    return rgb_values[index].name;
}