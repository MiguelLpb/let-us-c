
/* 
 * File:   tf.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 5. April 2018, 10:25
 */

#ifndef TF_H
#define TF_H


#include "bricklet_joystick.h"
#include "bricklet_rgb_led_matrix.h"
#include "bricklet_ambient_light.h"
#include "bricklet_piezo_speaker.h"
#include "bricklet_multi_touch.h"

extern RGBLEDMatrix rgb_led_matrix;
extern int rgb_matrix_is_valid;

extern Joystick left;
extern int left_is_valid;

extern Joystick right;
extern int right_is_valid;

extern AmbientLight ambientlight;
extern int ambient_light_is_valid;

extern PiezoSpeaker piezo_speaker;
extern int piezo_speaker_is_valid;

extern MultiTouch multi_touch;
extern int multi_touch_is_valid;


extern void tf_init();

extern void tf_tear_down();

extern int tf_hardware_ready();

#endif /* TF_H */

