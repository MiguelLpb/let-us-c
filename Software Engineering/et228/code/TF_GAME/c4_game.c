
/* 
 * File:   c4_game.c
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 1. Juni 2018, 17:32
 */

#include "c4_game.h"

#include "tf.h"

extern color_t c4_player_to_color (player_t o)
{
    switch (o)
    {
        case LEFT : 
            return GREEN;
        case RIGHT :
            return RED;
        default:
            return BLACK;
    }
}

extern player_t c4_color_to_player(color_t color)
{
    switch (color)
    {
        case COLOR_LEFT : 
            return LEFT;
        case COLOR_RIGHT :
            return RIGHT;
        default:
            return NO_ORIENTATION;
    }
}

