
/* 
 * File:   c4_frame.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on April 13, 2018, 8:47 PM
 */

#ifndef C4_FRAME_H
#define C4_FRAME_H

extern void frame_draw();

#endif /* C4_FRAME_H */

