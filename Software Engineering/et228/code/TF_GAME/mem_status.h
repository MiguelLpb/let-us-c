
/* 
 * File:   mem_status.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 20. Juni 2018, 18:52
 */

#ifndef MEM_STATUS_H
#define MEM_STATUS_H

extern void mem_status_show (int current, int total);

extern void mem_status_draw_starting ();
extern void mem_status_clear_starting ();


extern void mem_status_draw_wait ();
extern void mem_status_clear_wait ();
extern void mem_status_clear_wait_and_reset ();

extern void mem_status_show_lost();
extern void mem_status_show_timeout();
extern void mem_status_show_won();
extern void mem_status_clear();



#endif /* MEM_STATUS_H */

