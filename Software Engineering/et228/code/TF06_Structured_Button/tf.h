
/* 
 * File:   tf.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on February 16, 2018, 9:04 AM
 */

#ifndef TF_H
#define TF_H

#include "bricklet_rotary_poti.h"
#include "bricklet_dual_button.h"
#include "bricklet_rgb_led.h"


extern RGBLED rl;
extern int rgb_led_is_valid;

extern RotaryPoti rp;
extern int rotary_poti_is_valid;

extern DualButton db;
extern int dual_button_is_valid;
 

extern void init_tf();

extern void exit_tf();

#endif /* TF_H */

