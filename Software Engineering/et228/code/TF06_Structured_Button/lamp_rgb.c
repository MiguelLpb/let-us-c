
/* 
 * File:   lamp_rgb.c
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on February 16, 2018, 9:03 AM
 */

#include <stdio.h>

#include "tf.h"


extern void lamp_to_on()
{
    if (rgb_led_is_valid)
    {
        rgb_led_set_rgb_value(&rl, 128, 128, 128);
    }
}


extern void lamp_to_off()
{
    if (rgb_led_is_valid)
    {
        rgb_led_set_rgb_value(&rl, 0, 0, 0);
    }
}
