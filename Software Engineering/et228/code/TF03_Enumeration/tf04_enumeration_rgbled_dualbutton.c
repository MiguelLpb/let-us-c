/* 
 * File:   tf04_enumeration_rgbled_dualbutton.c
 * Author: Miguel Lenero
 *
 * Created on 14/05/2020
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <bricklet_rgb_led.h>
#include <bricklet_dual_button.h>
#include "ip_connection.h"
#define UID "DEr"
#define HOST "localhost"
#define PORT 4223

// Create IP Connection
IPConnection ipcon;
//Create RGB LED
RGBLED rl;
//Create Dual Button
DualButton db;

// Print incoming enumeration information

void cb_enumerate(const char *uid, const char *connected_uid,
        char position, uint8_t hardware_version[3],
        uint8_t firmware_version[3], uint16_t device_identifier,
        uint8_t enumeration_type, void *user_data) {
    (void) user_data;

    printf("UID:               %s\n", uid);
    printf("Enumeration Type:  %d\n", enumeration_type);

    if (enumeration_type == IPCON_ENUMERATION_TYPE_DISCONNECTED) {
        printf("\n");
        return;
    }

    //Create device object
    rgb_led_create(&rl, UID, &ipcon);
    //Change led light to green
    rgb_led_set_rgb_value(&rl, 0, 255, 0);

    printf("Connected UID:     %s\n", connected_uid);
    printf("Position:          %c\n", position);
    printf("Hardware Version:  %d.%d.%d\n", hardware_version[0],
            hardware_version[1],
            hardware_version[2]);
    printf("Firmware Version:  %d.%d.%d\n", firmware_version[0],
            firmware_version[1],
            firmware_version[2]);
    printf("Device Identifier: %d\n", device_identifier);
    printf("\n");
}

void on_dual_button_state_changed(uint8_t button_l, uint8_t button_r, uint8_t led_l, uint8_t led_r,
        void *user_data) {
    if (button_l == DUAL_BUTTON_BUTTON_STATE_PRESSED && led_r != 2) {
        rgb_led_set_rgb_value(&rl, 20, 0, 0);
        dual_button_set_selected_led_state(&db, 1, 2);
        printf("rot\n");

    } else if (button_l == DUAL_BUTTON_BUTTON_STATE_PRESSED && led_r == 2) {
        rgb_led_set_rgb_value(&rl, 0, 20, 0);
        dual_button_set_selected_led_state(&db, 1, 3);
        printf("grün\n");
    }
}

int main(void) {

    ipcon_create(&ipcon);

    // Connect to brickd
    if (ipcon_connect(&ipcon, HOST, PORT) < 0) {
        fprintf(stderr, "Could not connect to brickd\n");
        return 1;
    }

    // Register enumeration callback to "cb_enumerate"
    ipcon_register_callback(&ipcon,
            IPCON_CALLBACK_ENUMERATE,
            (void (*)(void))cb_enumerate,
            NULL);
    // Trigger enumerate
    ipcon_enumerate(&ipcon);

    /*
    dual_button_register_callback(&db,
            DUAL_BUTTON_CALLBACK_STATE_CHANGED,
            (void *) on_dual_button_state_changed,
            NULL);
     */

    printf("Press key to exit\n");
    getchar();
    ipcon_destroy(&ipcon); // Calls ipcon_disconnect internally
    return 0;
}

