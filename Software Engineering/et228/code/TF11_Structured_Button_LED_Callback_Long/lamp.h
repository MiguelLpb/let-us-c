/* 
 * File:   light.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on February 16, 2018, 9:04 AM
 */

#ifndef LIGHT_H
#define LIGHT_H

#include <stdint.h>


extern void lamp_init();

extern void lamp_on();
extern void lamp_off();

extern void lamp_dim(uint8_t intensity);

extern void lamp_set_next_color();

typedef enum
{
    LAMP_RED, LAMP_GREEN, LAMP_BLUE, LAMP_WHITE
} lamp_color_t;

#endif /* LIGHT_H */

