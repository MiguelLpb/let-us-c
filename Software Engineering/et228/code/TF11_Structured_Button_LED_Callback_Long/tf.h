
/* 
 * File:   tf.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on February 16, 2018, 9:04 AM
 */

#ifndef TF_H
#define TF_H

#include "bricklet_rotary_poti.h"
#include "bricklet_dual_button.h"
#include "bricklet_rgb_led.h"


extern int tf_rgb_led_is_valid();
extern RGBLED * tf_get_rgb_led();
extern void tf_register_rgb_led_on_connect(void (*)(RGBLED *) );
extern void tf_register_rgb_led_on_disconnect(void (*)() );


extern int tf_dual_button_is_valid();
extern DualButton * tf_get_dual_button();
extern void tf_register_dual_button_cb_client_left();
extern void tf_register_dual_button_cb_client_right();
extern void tf_register_dual_button_on_connect(void (*)(DualButton *) );
extern void tf_register_dual_button_on_disconnect(void (*)() );


extern int tf_rotary_poti_is_valid();
extern RotaryPoti * tf_get_rotary_poti();
extern void tf_register_rotary_poti_cb(void (*) );


extern void tf_init();

extern void tf_exit();

#endif /* TF_H */

