/* 
 * File:   la_play.h
 * 
 * responsibilities
 * -- perform the game: 
 *      initialize lamps
 *      check switch, 
 *      turn lamps on off 
 *      recognize end 
 */

#ifndef LA_PLAY_H
#define LA_PLAY_H

extern void la_play();

#endif /* LA_PLAY_H */

