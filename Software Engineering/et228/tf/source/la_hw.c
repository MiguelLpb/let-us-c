
/* 
 * File:   la_game_hw.c
 * 
 * Responsibilities 
 * -- implement promised API 
 * 
 * Note: depends on TF HW
 */

#include "la_hw.h"
#include "la_config.h"
#include "la_lamp.h"
#include "la_display.h"

#include "logger.h"


extern void la_hw_iinit()
{
    log_entry(__FUNCTION__);
}

extern int la_hw_is_ready()
{
    log_entry(__FUNCTION__);

    return (
            la_config_output_to_console() && la_config_input_from_console()
            )
            || (la_display_is_ready() && la_lamps_are_ready());
}

