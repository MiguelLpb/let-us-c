
/* 
 * File:   la_tf.c
 * 
 * Responsibilities 
 * 
 * -- implement promised API 
 */
#include "la_tf.h"

#include <stdio.h>
#include <stdint.h>

#include "la_config.h"

#include "logger.h"
#include "timer.h"

#include "ip_connection.h"
#include "bricklet_rgb_led_button.h"
#include "bricklet_oled_128x64_v2.h"
#include "brick_master.h" 
#include "brick_hat.h"
#include "brick_hat_zero.h"

#ifdef __CYGWIN__
#include <windows.h>
#endif


/*
 * **************************************************************************
 */

/*
 * external callback to be set by users 
 */
static tf_button_released_callback_t button_released_callback;


extern void la_tf_register(tf_button_released_callback_t cb)
{
    log_entry(__FUNCTION__);

    button_released_callback = cb;
}

/*
 * **************************************************************************
 */

#define UID_LEN (8)

static IPConnection ipcon;

typedef struct
{
    OLED128x64V2 oled;
    int valid;
    char uid[UID_LEN];
} display_t;

static display_t display;

typedef struct
{
    RGBLEDButton button;
    int valid;
    char uid[UID_LEN];
    int lamp;
} rgb_button_t;

static rgb_button_t buttons[7];

/*
 * internal callbacks to be registered at tf 
 */
static void cb_connect(uint8_t connect_reason, void *user_data);
static void cb_disconnect(uint8_t connect_reason, void *user_data);
static void cb_enumerate(const char *uid, const char *connected_uid,
        char position, uint8_t hardware_version[3],
        uint8_t firmware_version[3], uint16_t device_identifier,
        uint8_t enumeration_type, void *user_data);


/*
 * **************************************************************************
 */

#define PORT (4223)


extern void la_tf_init()
{
    log_entry(__FUNCTION__);
    /* Create IP connection */

    button_released_callback = 0;

    display.valid = 0;
    for (int i = 0; i < NO_OF_LAMPS; i++)
    {
        buttons[i].valid = 0;
    }
    ipcon_create(&ipcon);

    /* Connect to brickd */
    char msg[100];
    char * host;
    if (ipcon_connect(&ipcon, host = la_config_get_tf_host(), PORT) < 0)
    {
        sprintf(msg, "Could not connect to brickd at %s", host);
        log_msg(ERROR, msg);
        exit(EXIT_FAILURE);
    }
    else
    {
        sprintf(msg, "TF connected to %s", host);
        log_msg(INFO, msg);
    }
    ipcon_register_callback(&ipcon,
            IPCON_CALLBACK_ENUMERATE,
            (void *) cb_enumerate,
            NULL);

    ipcon_register_callback(&ipcon, IPCON_CALLBACK_CONNECTED, (void *) cb_connect, NULL);
    ipcon_register_callback(&ipcon, IPCON_CALLBACK_DISCONNECTED, (void *) cb_disconnect, NULL);
#ifdef __CYGWIN__
    log_msg(INFO, "Compiled with CYGWIN. Wait a moment...");
    timer_sleep_ms(500);
    log_msg(INFO, "... now enumerating");
    ipcon_enumerate(&ipcon);
#endif
    ipcon_enumerate(&ipcon);
}


/* ************************************************************************** */

static void cb_connect(uint8_t connect_reason, void *user_data)
{
    log_entry(__FUNCTION__);

    ipcon_enumerate(&ipcon);
}


/* ************************************************************************** */

static void cb_disconnect(uint8_t connect_reason, void *user_data)
{
    log_entry(__FUNCTION__);

    for (int i = 0; i < NO_OF_LAMPS; i++)
    {
        buttons[i].valid = 0;
        rgb_led_button_destroy(&buttons[i].button);
    }
    display.valid = 0;
    oled_128x64_v2_destroy(&display.oled);
}

/* ************************************************************************** */

static void check_for_connection(const char * uid, char position, uint16_t device_identifier);
static void check_for_disconnection(const char * uid);


static void cb_enumerate(const char *uid, const char *connected_uid,
        char position, uint8_t hardware_version[3],
        uint8_t firmware_version[3], uint16_t device_identifier,
        uint8_t enumeration_type, void *user_data)
{
    log_entry(__FUNCTION__);

    switch (enumeration_type)
    {
        case IPCON_ENUMERATION_TYPE_DISCONNECTED:
            check_for_disconnection(uid);
            break;
        case IPCON_ENUMERATION_TYPE_CONNECTED:
        case IPCON_ENUMERATION_TYPE_AVAILABLE:
            check_for_connection(uid, position, device_identifier);
            break;
        default:
            /* illegal enumeration_type */
            break;
    }
}


/* ************************************************************************** */

static void check_for_disconnection(const char * uid)
{
    for (int i = 0; i < NO_OF_LAMPS; i++)
    {
        if (!strcmp(uid, buttons[i].uid))
        {
            buttons[i].valid = 0;
            rgb_led_button_destroy(&buttons[i].button);
            return;
        }
    }
    if (!strcmp(uid, display.uid))
    {
        display.valid = 0;
        oled_128x64_v2_destroy(& (display.oled));
    }
}


/* ************************************************************************** */

static void register_oled(display_t *, const char * uid, char position);

static void register_rgb_button(rgb_button_t *buttons, const char * uid, char position);

static void log_bricklet(const char *identifier, const char *uid, char position);


/*
 *  A -> oled
 *  rgb_button-position -> lamp 
 *  B -> 2  C -> 3  D -> 4  E -> 1  F -> 0  G -> 6  H -> 5
 */
static void check_for_connection(const char * uid, char position, uint16_t device_identifier)
{
    log_entry(__FUNCTION__);

    switch (device_identifier)
    {
        case OLED_128X64_V2_DEVICE_IDENTIFIER:
            register_oled(&display, uid, position);
            break;
        case RGB_LED_BUTTON_DEVICE_IDENTIFIER:
            register_rgb_button(buttons, uid, position);
            break;
        case HAT_DEVICE_IDENTIFIER:
        case HAT_ZERO_DEVICE_IDENTIFIER:
        case MASTER_DEVICE_IDENTIFIER:
            /* ignore these */
            break;
        default:
            log_bricklet("wrong bricklet", uid, position);
    }
}

/* ************************************************************************** */


static void set_uid(const char * connected_uid, char * uid);


static void register_oled(display_t * display, const char * uid, char position)
{
    log_entry(__FUNCTION__);

    if ((!display->valid) && (position == 'a'))
    {
        oled_128x64_v2_create(&(display->oled), uid, &ipcon);
        set_uid(uid, display->uid);
        display->valid = 1;
        oled_128x64_v2_clear_display(& (display->oled));
        log_bricklet("OLED_128X64_V2_DEVICE_IDENTIFIER", uid, position);
    }
    else
    {
        log_msg(ERROR, "OLED_128X64_V2 duplicate or at wrong position");
    }
}


static void set_uid(const char * connected_uid, char * uid)
{
    log_entry(__FUNCTION__);

#ifdef __CYGWIN__
    strncpy(uid, connected_uid, UID_LEN);
#else    
    strlcpy(uid, connected_uid, UID_LEN);
#endif
}


static void log_bricklet(const char *identifier, const char *uid, char position)
{
    log_entry(__FUNCTION__);

    char msg[100];
    sprintf(msg, "position %c, uid '%s',  %s", position, uid, identifier);
    log_msg(INFO, msg);
}

/* ************************************************************************** */

static int rgb_to_lamp_map[] = {2, 3, 4, 1, 0, 6, 5};

static int lamp_idx_in_callback;

static void cb_button_state_changed(uint8_t state, void *user_data);


static void register_rgb_button(rgb_button_t * buttons, const char * uid, char position)
{
    log_entry(__FUNCTION__);

    int pos_idx = position - 'b'; /* 'b' -> 0, 'c' -> 1 etc. */
    if ((pos_idx >= 0) && (pos_idx < NO_OF_LAMPS)
        && !buttons[rgb_to_lamp_map[pos_idx]].valid)
    {
        lamp_idx_in_callback = rgb_to_lamp_map[pos_idx]; /* 0 -> 2, 1 -> 2 etc. */ 
        
        rgb_led_button_create(&buttons[lamp_idx_in_callback].button, uid, &ipcon);
        
        buttons[lamp_idx_in_callback].valid = 1;
        buttons[lamp_idx_in_callback].lamp = lamp_idx_in_callback;
        set_uid(uid, buttons[lamp_idx_in_callback].uid);
        rgb_led_button_register_callback(
                &buttons[lamp_idx_in_callback].button,
                RGB_LED_BUTTON_CALLBACK_BUTTON_STATE_CHANGED,
                (void (*)(void))cb_button_state_changed,
                &buttons[lamp_idx_in_callback].lamp
                );
        rgb_led_button_set_color(& (buttons[lamp_idx_in_callback].button), 
                0, 0, 0);
        log_bricklet("RGB_LED_BUTTON_DEVICE_IDENTIFIER", uid, position);
    }
    else
    {
        log_msg(ERROR, "RGB_LED_BUTTON duplicate or at wrong position");
    }
}


static void cb_button_state_changed(uint8_t state, void *user_data)
{
    log_entry(__FUNCTION__);

    if (state == RGB_LED_BUTTON_BUTTON_STATE_RELEASED)
    {
        if (button_released_callback != 0)
        {
            button_released_callback(*((int*) user_data));
        }
    }
}


/*
 * **************************************************************************
 */

extern int la_tf_display_is_valid()
{
    log_entry(__FUNCTION__);
    
    return display.valid;
}


/*
 * **************************************************************************
 */

extern void la_tf_message(char * msg, uint8_t line)
{
    log_entry(__FUNCTION__);

    if (la_tf_display_is_valid())
    {
        char text[22] = "                   ";
        sprintf(text, msg, 22);
        oled_128x64_v2_write_line(&display.oled, line, 0, text);
    }
}

/*
 * **************************************************************************
 */

extern void la_tf_display_clear()
{
    log_entry(__FUNCTION__);

    if (la_tf_display_is_valid())
    {
        oled_128x64_v2_clear_display(&display.oled);
    }
}


/*
 * **************************************************************************
 */

extern int la_tf_buttons_are_valid()
{
    log_entry(__FUNCTION__);

    for (int i = 0; i < NO_OF_LAMPS; i++)
    {
        if (!la_tf_button_is_valid(i))
        {
            return 0;
        }
    }
    return 1;
}


/*
 * **************************************************************************
 */

extern int la_tf_button_is_valid(int nr)
{
    log_entry(__FUNCTION__);

    if ((nr >= 0) && (nr < NO_OF_LAMPS))
    {
        return buttons[nr].valid;
    }
    return 0;
}


/*
 * **************************************************************************
 */

extern void la_tf_button_set_light(int nr, uint8_t red, uint8_t green, uint8_t blue)
{
    log_entry(__FUNCTION__);

    if ((nr >= 0) && (nr < NO_OF_LAMPS))
    {
        if (buttons[nr].valid)
        {
            rgb_led_button_set_color(& (buttons[nr].button), red, green, blue);
        }
    }
}

/*
 * **************************************************************************
 */

