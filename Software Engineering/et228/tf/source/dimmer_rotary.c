
/* 
 * File:   dimmer_rotary.c
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 4. Mai 2019, 17:24
 */
#include <stdio.h>
#include <stdlib.h>


#include "dimmer.h"
#include "tf.h"
#include "logger.h"

static dimmer_callback_t dimmer_callback;


extern void dimmer_subscribe(dimmer_callback_t cb)
{
    log_entry("%s\n", __FUNCTION__);
    dimmer_callback = cb;
}


void cb_position_has_changed(int16_t position, void *user_data)
{
    (void) user_data; /* avoid unused parameter warning */
    float intensity = position + 150.f;
    intensity = (intensity / 300.f) * 255.f;
    dimmer_callback((uint8_t) intensity);
}


extern void dimmer_init(void)
{
    log_entry("%s\n", __FUNCTION__);
    dimmer_callback = 0;
    tf_register_rotary_poti_cb(cb_position_has_changed);
}
