
/* Author: Miguel L.
 * File:   la_solver.c
 */

#include "la_config.h"
#include "logger.h"

#include <stdio.h>
#include <string.h>


static void init_solution(int* solution, int size);
static void solve_all_lamps(int *lamps, int size, int *solution);
static int normalize_solution(int * solution, int size);

extern int la_get_solution(int * states, int size, int * solution) {
    log_entry(__FUNCTION__);

    init_solution(solution, NO_OF_LAMPS);
    solve_all_lamps(states, size, solution);
    int result = normalize_solution(solution, size);
    char msg[100];
    sprintf(msg, "la_solve yields %d draws to solve: ", result);
    for (int i = 0; i < NO_OF_LAMPS; i++) {
        char state[10];
        if (solution[i]) {
            sprintf(state, "%d ", i);
            strcat(msg, state);
        }
    }
    log_msg(INFO, msg);
    return result;
}

/*
 * zero the array <solution> of size <size>
 */
static void init_solution(int* solution, int size) {
    // ToDo use library function memset 
    for (int i = 0; i < NO_OF_LAMPS; i++) {
        *(solution + i) = 0;
    }
}

/*
 * solve one lamp <nr> with <size> lamps
 */
static void solve_one_lamp(int nr, int size, int * solution);

static void solve_all_lamps(int *lamps, int size, int *solution) {
 
    for (int nr = 0; nr < size; nr++)
    {
        if (lamps[nr])
        {
            solve_one_lamp(nr, size, solution);
        }
    }
}

static int toggle[] = {0, 1, 3, 4, 6};


static void solve_one_lamp(int nr, int size, int * solution) {
    
    for (int i = 0; i < size; i++)
    {
        int position = (nr + toggle[i]) % size;
        solution[position]++;
    }
}

/*
 * pushing a button 2, 4, 6, ... times is like not pushing at all
 * pushing a button 3, 5, 7, ... times is like pushing just once
 * return total number of pushes
 */
static int normalize_solution(int * solution, int size) {
    int result = 0;
    for (int nr = 0; nr < size; nr++) {
        solution[nr] = solution[nr] % 2;
        result += solution[nr];
    }
    return result;
}