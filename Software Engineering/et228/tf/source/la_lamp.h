
/* 
 * File:   la_lamp.h
 * 
 * responsibilities
 * -- provide individual lamps (bulb / switch)
 * -- provide array of lamps
 */

#ifndef LA_LAMP_H
#define LA_LAMP_H

#include <stdio.h>
#include <stdint.h> 

/*
 * perform any necessary initialisation
 */
extern void la_lamp_init();

/*
 * returns 1 if lamp <nr> is ready
 * returns 0 if not or <nr> invalid
 */
extern int la_lamp_is_ready(int nr);

/*
 * returns 1 if all lamps are ready, 0 otherwise
 */
extern int la_lamps_are_ready();

/*
 * returns 1 if lamp <nr> is to toggle
 * clears the toggle: subsequent call will return 0
 * returns 0 if not or <nr> invalid
 */
extern int la_lamp_to_toggle(int nr);

/*
 * toggles lamp <nr> on<->off according to rule
 */
extern void la_lamp_toggle_lamp(int nr);

/*
 * set lamp <nr> to <on> 
 */
extern void la_lamp_set_lamp(int nr, int on);

/*
 * set lamp <nr> to hint
 */
extern void la_lamp_set_hint(int nr);

/*
 * switch all lamps to <on>
 */
extern void la_lamp_all(int on);

/*
 * returns 1 if lamp <nr> is on, o otherwise
 */
extern int la_lamp_is_on(int nr);

/*
 * returns 1 if all lamps are on, o otherwise
 */
extern int la_lamps_on();

/*
 * returns the total number of lamps on 0 .. 7
 */
extern int la_lamp_get_no_off_lamps_on();

#endif /* LA_LAMP_H */

