
/* 
 * File:   tf.c
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on February 16, 2018, 9:05 AM
 */

#include <stdio.h>
#include <unistd.h> 
#include "logger.h"
#include "ip_connection.h"
#include "bricklet_dual_button.h"
#include "bricklet_rgb_led.h"
#include "bricklet_rotary_poti.h"

#define UID_LEN (8)

/* *************************************************************************
 * 
 * Dual button
 */
static DualButton db;
static int dual_button_valid;
static char dual_button_uid[UID_LEN];
static void (* cb_dual_button_client_left)(uint8_t button_l, uint8_t button_r) = 0;
static void (* cb_dual_button_client_right)(uint8_t button_l, uint8_t button_r) = 0;
static void (*dual_button_on_connect_cb)(DualButton) = 0;
static void (*dual_button_on_disconnect_cb)() = 0;


extern int tf_dual_button_is_valid()
{
    return dual_button_valid;
}


extern DualButton * tf_get_dual_button()
{
    return dual_button_valid ? &db : 0;
}


extern void tf_register_dual_button_cb_client_left(void (* cb) (uint8_t button_l, uint8_t button_r))
{
    cb_dual_button_client_left = cb;
}


extern void tf_register_dual_button_cb_client_right(void (* cb) (uint8_t button_l, uint8_t button_r))
{
    cb_dual_button_client_right = cb;
}


extern void tf_register_dual_button_on_connect(void (*cb)(DualButton))
{
    dual_button_on_connect_cb = cb;
}


extern void tf_register_dual_button_on_disconnect(void (*cb)())
{
    dual_button_on_disconnect_cb = cb;
}


static void dual_button_disconnect()
{
    dual_button_valid = 0;
    dual_button_destroy(&db);
    if (dual_button_on_disconnect_cb != 0)
    {
        (*dual_button_on_disconnect_cb)();
    }
}

/* *************************************************************************
 * 
 * RGB LED
 */

static RGBLED rl;
static int rgb_led_valid;
static char rgb_led_uid[UID_LEN];
static void (*rgb_led_on_connect_cb)(RGBLED) = 0;
static void (*rgb_led_on_disconnect_cb)() = 0;


extern int tf_rgb_led_is_valid()
{
    return rgb_led_valid;
}


extern RGBLED * tf_get_rgb_led()
{
    return rgb_led_valid ? & rl : 0;
}


extern void tf_register_rgb_led_on_connect(void (*cb)(RGBLED))
{
    rgb_led_on_connect_cb = cb;
}


extern void tf_register_rgb_led_on_disconnect(void (*cb)())
{
    rgb_led_on_disconnect_cb = cb;
}


static void rgb_led_disconnect()
{
    rgb_led_valid = 0;
    rgb_led_destroy(&rl);
    if (rgb_led_on_disconnect_cb != 0)
    {
        (*rgb_led_on_disconnect_cb)();
    }
}

/* *************************************************************************
 * 
 * Rotary Poti
 */

static RotaryPoti rp;
static int rotary_poti_valid;
static char rotary_poti_uid[UID_LEN];
static void (* poti_cb_client) (int16_t) = 0;


extern int tf_rotary_poti_is_valid()
{
    return rotary_poti_valid;
}


extern RotaryPoti * tf_get_rotary_poti()
{
    return rotary_poti_valid ? & rp : 0;
}


extern void tf_register_rotary_poti_cb(void (* cb) (int16_t))
{
    poti_cb_client = cb;
}


static void rotary_poti_disconnect()
{
    rotary_poti_valid = 0;
    rotary_poti_destroy(&rp);
}



/* *************************************************************************
 * 
 * Print incoming enumeration information
 */

static void cb_enumerate(const char *uid, const char *connected_uid,
        char position, uint8_t hardware_version[3],
        uint8_t firmware_version[3], uint16_t device_identifier,
        uint8_t enumeration_type, void *user_data);

static void cb_connect(uint8_t connect_reason, void *user_data);
static void cb_disconnect(uint8_t connect_reason, void *user_data);

/*
 * TF specific defines 
 */
#define HOST "localhost"
#define PORT 4223

static IPConnection ipcon;


extern void tf_init()
{
    dual_button_valid = 0;
    rgb_led_valid = 0;
    rotary_poti_valid = 0;

    cb_dual_button_client_left = 0;
    cb_dual_button_client_right = 0;

    /* Create IP connection */
    ipcon_create(&ipcon);

    /* Connect to brickd */
    if (ipcon_connect(&ipcon, HOST, PORT) < 0)
    {
        fprintf(stderr, "Error: Could not connect to brickd. Exit.\n");
        exit(EXIT_FAILURE);
    }
    /* Register enumeration callback to "cb_enumerate" */
    ipcon_register_callback(&ipcon,
            IPCON_CALLBACK_ENUMERATE,
            (void *) cb_enumerate,
            NULL);

    ipcon_register_callback(&ipcon, IPCON_CALLBACK_CONNECTED, (void *) cb_connect, NULL);
    ipcon_register_callback(&ipcon, IPCON_CALLBACK_DISCONNECTED, (void *) cb_disconnect, NULL);
#ifdef __CYGWIN__
    printf("Compiled with CYGWIN. Wait, then enumerate ...\n");
    sleep(1);
    ipcon_enumerate(&ipcon);
#endif
}


static void cb_connect(uint8_t connect_reason, void *user_data)
{
    log_entry("%s\n", __FUNCTION__);
    ipcon_enumerate(&ipcon);
}


static void cb_disconnect(uint8_t connect_reason, void *user_data)
{
    log_entry("%s\n", __FUNCTION__);
    rgb_led_disconnect();
    dual_button_disconnect();
    rotary_poti_disconnect();
}

void on_dual_button_state_changed(uint8_t button_l, uint8_t button_r, uint8_t led_l, uint8_t led_r,
        void *user_data);

static void on_poti_state_changed(int16_t position, void *user_data);



static void check_for_disconnection(const char * disconnected_uid);
static void check_for_connection(const char * connected_uid, uint16_t device_identifier);


static void cb_enumerate(const char *uid, const char *connected_uid,
        char position, uint8_t hardware_version[3],
        uint8_t firmware_version[3], uint16_t device_identifier,
        uint8_t enumeration_type, void *user_data)
{
    switch (enumeration_type)
    {
        case IPCON_ENUMERATION_TYPE_DISCONNECTED:
            check_for_disconnection(uid);
            break;
        case IPCON_ENUMERATION_TYPE_CONNECTED:
        case IPCON_ENUMERATION_TYPE_AVAILABLE:
            check_for_connection(uid, device_identifier);
            break;
        default:
            /* illegal enumeration_type */
            break;
    }
}

static int disconnected_uid(const char * disconnected_uid, int * valid, const char * uid, const char * name);


static void check_for_disconnection(const char * uid)
{
    if (disconnected_uid(uid, &dual_button_valid, dual_button_uid, DUAL_BUTTON_DEVICE_DISPLAY_NAME))
    {
        dual_button_disconnect();
    }
    if (disconnected_uid(uid, &rgb_led_valid, rgb_led_uid, RGB_LED_DEVICE_DISPLAY_NAME))
    {
        rgb_led_disconnect();
    }
    if (disconnected_uid(uid, &rotary_poti_valid, rotary_poti_uid, ROTARY_POTI_DEVICE_DISPLAY_NAME))
    {
        rotary_poti_disconnect();
    }
}


static int disconnected_uid(const char * disconnected_uid, int * valid, const char * uid, const char * name)
{
    if (*valid)
    {
        if (!strcmp(disconnected_uid, uid))
        {
            * valid = 0;
            return 1;
        }
    }
    return 0;
}

static void connect_uid(const char * disconnected_uid, int * valid, char * uid, const char * name);


static void check_for_connection(const char * uid, uint16_t device_identifier)
{

    switch (device_identifier)
    {
        case DUAL_BUTTON_DEVICE_IDENTIFIER:

            if (!dual_button_valid)
            {
                connect_uid(uid, &dual_button_valid, dual_button_uid, DUAL_BUTTON_DEVICE_DISPLAY_NAME);
                dual_button_create(&db, uid, &ipcon);
                dual_button_register_callback(&db,
                        DUAL_BUTTON_CALLBACK_STATE_CHANGED,
                        (void *) on_dual_button_state_changed,
                        NULL);
                dual_button_set_led_state(&db, 3, 3);
                if (dual_button_on_connect_cb != 0)
                {
                    
                    dual_button_on_connect_cb(db);
                }
            }
            break;
        case RGB_LED_DEVICE_IDENTIFIER:
            if (!rgb_led_valid)
            {
                connect_uid(uid, &rgb_led_valid, rgb_led_uid, RGB_LED_DEVICE_DISPLAY_NAME);
                rgb_led_create(&rl, uid, &ipcon);
                rgb_led_set_rgb_value(&rl, 0, 0, 0);
                if (rgb_led_on_connect_cb != 0)
                {
                    rgb_led_on_connect_cb(rl);
                }
            }
            break;
        case ROTARY_POTI_DEVICE_IDENTIFIER:
            if (!rotary_poti_valid)
            {
                connect_uid(uid, &rotary_poti_valid, rotary_poti_uid, ROTARY_POTI_DEVICE_DISPLAY_NAME);

                rotary_poti_create(&rp, uid, &ipcon);
                rotary_poti_register_callback(&rp,
                        ROTARY_POTI_CALLBACK_POSITION,
                         (void*) on_poti_state_changed, 
                        NULL);
                uint32_t period = 50;
                rotary_poti_set_position_callback_period(&rp, period);
            }
            break;
        default:
            /* ignore all other */
            break;
    }
}


static void connect_uid(const char * connected_uid, int * valid, char * uid, const char * name)
{
    *valid = 1;
#ifdef __CYGWIN__
    strncpy (uid, connected_uid, UID_LEN);
#else    
    strlcpy(uid, connected_uid, UID_LEN);
#endif
}


static int button_l_last = DUAL_BUTTON_BUTTON_STATE_RELEASED;
static int button_r_last = DUAL_BUTTON_BUTTON_STATE_RELEASED;


void on_dual_button_state_changed(uint8_t button_l, uint8_t button_r,
        uint8_t led_l, uint8_t led_r,
        void *user_data)
{
    if (button_l_last != button_l)
    {
        button_l_last = button_l;
        if (cb_dual_button_client_left != 0)
        {
            cb_dual_button_client_left(button_l, button_r);
        }
    }
    if (button_r_last != button_r)
    {
        button_r_last = button_r;
        if (cb_dual_button_client_right != 0)
        {
            cb_dual_button_client_right(button_l, button_r);
        }
    }
}

static int poti_last = 0;

void on_poti_state_changed(int16_t position, void *user_data)
{
    if (position != poti_last)
    {
        poti_last = position;
        int * i_ptr = (int *) user_data;
        if (poti_cb_client != 0)
        {
            poti_cb_client(poti_last);
        }
    }
}

extern void tf_exit()
{
    ipcon_destroy(&ipcon);
}

