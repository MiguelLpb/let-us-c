
/* 
 * File:   la_lamp.c
 * 
 * Responsibilities 
 * -- implement promised API 
 * 
 * Note: depends on HW
 */
#include "la_lamp.h"
#include "la_rule.h"
#include "la_display.h"
#include "la_tf.h"
#include "la_config.h"
#include "logger.h"

#include <string.h>
#include <stdio.h>
#include <stdint.h>

/*
 * **************************************************************************
 */

/*
 * a lamp 
 * notes: 
 *  -- only application specific properties, independent from underlying HW 
 *  -- info may point to struct containing implementation specific entries
 */
typedef struct
{
    int has_been_pressed;
    int is_on;
} lamp_t;
/*
 * the array of 7 lamps 
 */
static lamp_t lamps[NO_OF_LAMPS];


/*
 * **************************************************************************
 */
static void button_callback(int nr)
{
    log_entry(__FUNCTION__);

    lamps[nr].has_been_pressed = 1;
}


extern int la_lamp_is_on(int nr)
{

    log_entry(__FUNCTION__);

    int res = 0;
    if ((nr >= 0) && (nr < NO_OF_LAMPS))
    {
        res = lamps[nr].is_on;
    }
    return res;
}


extern int la_lamps_on()
{

    log_entry(__FUNCTION__);

    int res = 0;
    for (int i = 0; i < NO_OF_LAMPS; i++)
    {
        res += la_lamp_is_on(i);
    }
    return res;
}


extern int la_lamp_is_ready(int nr)
{
    return la_tf_button_is_valid(nr);
}


extern int la_lamps_are_ready()
{
    return la_tf_buttons_are_valid();
}

typedef struct
{
    uint8_t R;
    uint8_t G;
    uint8_t B;
} rgb_t;

static rgb_t color_on = {200, 200, 200};
static rgb_t color_off = {0, 0, 0};


void la_lamp_toggle_lamp(int nr)
{
    log_entry(__FUNCTION__);

    int state[NO_OF_LAMPS];
    for (int i = 0; i < NO_OF_LAMPS; i++)
    {
        state[i] = lamps[i].is_on;
    }

    la_rule_toogle(state, NO_OF_LAMPS, nr);
    for (int i = 0; i < NO_OF_LAMPS; i++)
    {
        la_lamp_set_lamp(i, state[i]);
    }
}


extern void la_lamp_set_lamp(int nr, int on)
{
    log_entry(__FUNCTION__);

    if ((nr >= 0) && (nr < NO_OF_LAMPS))
    {
        lamps[nr].is_on = on;
        if (la_tf_button_is_valid(nr))
        {
            if (on)
            {
                la_tf_button_set_light(nr, color_on.R, color_on.G, color_on.B);
            }
            else
            {
                la_tf_button_set_light(nr, color_off.R, color_off.G, color_off.B);
            }
        }
    }
}


extern void la_lamp_set_hint(int nr)
{
    log_entry(__FUNCTION__);

    if ((nr >= 0) && (nr < NO_OF_LAMPS))
    {
        if (la_tf_button_is_valid(nr))
        {
            la_tf_button_set_light(nr, color_on.R, 0, 0);
        }
    }
}


extern void la_lamp_all(int on)
{
    log_entry(__FUNCTION__);

    for (int i = 0; i < NO_OF_LAMPS; i++)
    {
        if (on)
        {
            la_tf_button_set_light(i, color_on.R, color_on.G, color_on.B);
        }
        else
        {
            la_tf_button_set_light(i, color_off.R, color_off.G, color_off.B);
        }
    }
}


void la_lamp_init()
{
    log_entry(__FUNCTION__);

    la_tf_register(button_callback);
}


extern int la_lamp_get_no_off_lamps_on()
{
    log_entry(__FUNCTION__);

    int n = 0;
    for (int i = 0; i < NO_OF_LAMPS; i++)
    {
        if (lamps[i].is_on)
        {
            n++;
        }
    }
    return n;
}


extern int la_lamp_to_toggle(int nr)
{
    log_entry(__FUNCTION__);

    int res = 0;
    if ((nr >= 0) && (nr < NO_OF_LAMPS))
    {
        res = lamps[nr].has_been_pressed;
        lamps[nr].has_been_pressed = 0;
    }
    return res;
}