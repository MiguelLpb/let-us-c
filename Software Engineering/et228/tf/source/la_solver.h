/* 
 * File:   la_solver.h
* 
 * responsibilities
 * -- solve a given setup
 */

#ifndef LA_SOLVER_H
#define LA_SOLVER_H
/*
 * build the solution of <size>-may lamps 
 * <states> 1/0 on/off states of the lamps
 * <solution> 1/0 : 1 lamp is to toggle
 */
extern int la_get_solution(int * states, int size, int * solution);

#endif /* LA_SOLVER_H */
