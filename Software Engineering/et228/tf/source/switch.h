/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   switch.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 4. Mai 2019, 16:16
 */

#ifndef SWITCH_H
#define SWITCH_H

typedef enum
{
    SWITCH_STATE_OFF, SWITCH_STATE_ON
}
switch_state_t;


extern void switch_init(void);

extern int switch_is_on();

extern double switch_get_minimum_press_time();

extern void switch_run();

typedef void (*switch_callback_t)(switch_state_t);

extern void switch_subscribe(switch_callback_t cb);


#endif /* SWITCH_H */

