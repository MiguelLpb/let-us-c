
/* 
 * File:   timer.c
 * 
 * Responsibilities 
 * 
 * -- implement promised API 
 */

#include "timer.h"


#ifdef __CYGWIN__
#include <windows.h>
#include <stdio.h>
#elif _POSIX_C_SOURCE >= 199309L
#include <time.h>   // for nanosleep
#else
#include <unistd.h> // for usleep
#endif


void timer_sleep_ms(unsigned int milliseconds) // cross-platform sleep function
{
#ifdef __CYGWIN__
    Sleep(milliseconds);
#elif _POSIX_C_SOURCE >= 199309L
    struct timespec ts;
    ts.tv_sec = milliseconds / 1000;
    ts.tv_nsec = (milliseconds % 1000) * 1000000;
    nanosleep(&ts, NULL);
#else
    usleep(milliseconds * 1000);
#endif
}


double timer_get_difference_ms(struct timeval t0, struct timeval t1)
{
    return (t1.tv_sec - t0.tv_sec) * 1000.0 + (t1.tv_usec - t0.tv_usec) / 1000.0;
}

