
/* 
 * File:   logger.c
 * 
 * Responsibilities 
 * 
 * -- implement promised API 
 */

#include"logger.h"

#include <stdio.h>
#include <time.h>


static log_level_t log_level = ERROR;

extern void log_set_level (log_level_t lvl)
{
    log_level = lvl;
}

static char * get_log_level_name(log_level_t lvl);


static void _log(log_level_t lvl, char* format, const char * s)
{
    if (lvl >= log_level )
    {
        char temp[100]; 
        time_t current_time = time(NULL);
        struct tm *tm = localtime(&current_time); 
        strftime(temp, sizeof(temp), "%F %T", tm);        
        printf ("%s %s: ", temp, get_log_level_name(lvl));
        printf(format, s);
    }
}

#define MAX 80 
extern void log_entry(const char * s)
{
    _log (FLOW, "->%s\n", s);
}

extern void log_msg(log_level_t lvl, const char * s)
{
    _log (lvl, "%s\n", s);
}

extern void log_prompt(const char * s)
{
    _log (PROMPT, "%s", s);
}

static char * levels[6] = {"FLOW", "INFO", "ERROR", "NONE", "INPUT"};

static char * get_log_level_name(log_level_t lvl)
{
    return levels[lvl];
}
