
/* 
 * File:   la_display.c
 * 
 * Responsibilities 
 * -- implement promised API 
 * 
 * Note: depends on HW
 */

#include <stdio.h>
#include <string.h>

#include "la_display.h"
#include "la_config.h"
#include "la_tf.h"

#include "logger.h"


/*
 * **************************************************************************
 */
extern void la_display_init()
{
    log_entry(__FUNCTION__);
}


extern int la_display_is_ready()
{
    return la_tf_display_is_valid();
}

/*
 * prelimminary 
 */


/*
 * ************************************************************************** 
 */
extern void la_display_clear()
{
    la_tf_display_clear();
}


/*
 * **************************************************************************
 */
extern void la_display_draw(int nr)
{
    log_entry(__FUNCTION__);

    char msg[100];
    sprintf(msg, "Taster %d gedrueckt! ", nr);
    if (la_config_output_to_console())
    {
        log_msg(INFO, msg);
    }
    la_tf_message(msg, 5);
}


/*
 * **************************************************************************
 */

extern void la_display_msg(char * msg)
{
    log_entry(__FUNCTION__);
    if (la_config_output_to_console())
    {
        log_msg(INFO, msg);
    }
    la_tf_message(msg, 3);
}


/*
 * **************************************************************************
 */
extern void la_display_status(int draw, int on)
{
    log_entry(__FUNCTION__);

    if (la_config_output_to_console())
    {
        char msg1[100];
        char msg2[100];

        sprintf(msg2, "Lampen: ");
        for (int nr = 0; nr < NO_OF_LAMPS; nr++)
        {
            char state[20];
            sprintf(state, "|%d-%-3s%s", 
                    nr, 
                    la_lamp_is_on(nr) ? "ON" : "OFF", 
                    (nr == NO_OF_LAMPS-1) ? "|" : ""
                    );
            strcat(msg2, state);
        }
        if (on > 0)
        {
            sprintf(msg1, "Zug #%d, noch %d von %d Lampen an!", draw, on, NO_OF_LAMPS);
        }
        else
        {
            sprintf(msg1, "Zug #%d, FERTIG!", draw);
        }
        log_msg(INFO, msg1);
        log_msg(INFO, msg2);
    }
    char msg[100];
    sprintf(msg, "Zug # %d, %d an!", draw, on);
    la_tf_message(msg, 7);
}


/*
 * **************************************************************************
 */
extern void la_display_title(char * msg)
{
    log_entry(__FUNCTION__);
    
    if (la_config_output_to_console())
    {
        log_msg(INFO, msg);
    }
    la_tf_message(msg, 0);
}


/*
 * **************************************************************************
 */
extern void la_display_won()
{
    log_entry(__FUNCTION__);
    
    if (la_config_output_to_console())
    {
        log_msg(INFO, " === WON === ");
    }

    la_tf_message(" === GEWONNEN === ", 2);
}
