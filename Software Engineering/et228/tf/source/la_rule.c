
/* 
 * File:   la_rule.c
 * 
 * Responsibilities 
 * -- implement promised API 
 */

#include "la_rule.h"
#include "la_config.h"

#include "logger.h"


extern void la_rule_toogle(int * toggles, int size, int nr)
{
    log_entry(__FUNCTION__);
    
    if ((nr >= 0) && (nr < size))
    {
        int next = nr + 1;
        int prev = nr - 1 + NO_OF_LAMPS;
        next = next % NO_OF_LAMPS;
        prev = prev % NO_OF_LAMPS;
        
        *(toggles + prev) = ! *(toggles + prev);
        
        *(toggles + nr) = ! *(toggles + nr);

        *(toggles + next) = ! *(toggles + next);
    }
}
