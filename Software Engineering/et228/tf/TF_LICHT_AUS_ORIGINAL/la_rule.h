
/* 
 * File:   la_rule.h
 * 
 * responsibilities
 * -- just one switch lamp according to the rules
 */

#ifndef LA_RULE_H
#define LA_RULE_H

#include "la_lamp.h"

/*
 * switch lamp <nr> in array of lamps <lamps> according to the rules
 * 
 * <lamps> is an array of lamp_t  
 */
extern void la_rule_toogle_lamp(lamp_t * lamps, int nr);

#endif /* LA_RULE_H */

