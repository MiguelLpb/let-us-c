
/* 
 * File:   la_lamp.h
 * 
 * responsibilities
 * -- provide individual lamps (bulb / switch)
 * -- provide array of lamps
 */

#ifndef LA_LAMP_H
#define LA_LAMP_H
#include <stdio.h>
#include <unistd.h> 

extern void la_lamp_init();
extern void cb_button_state_changed(uint8_t state, void *user_data);
/*
 * a lamp .. prelimminary
 * notes: 
 *  -- only application specific properties, independent from underlying HW 
 *  -- info may point to struct containing implementation specific entries
 */
typedef struct
{
    void * info;
    int has_been_pressed;
    int is_on;
} lamp_t;

/*
 * returns 1 if lamp nr is ready
 * returns 0 is not or nr invalid
 * 
 */
extern int la_lamp_is_ready(int nr);

/*
 * toggles lamp nr on<->off
 */
extern void la_lamp_toogle_lamp(int nr);

/*
 * returns the total number of lamps on 0 .. 7
 */
extern int la_lamp_get_no_off_lamps_on();

/*
 * triggers output of all lamps' status to display
 */
extern void la_lamp_display_all();

#endif /* LA_LAMP_H */

