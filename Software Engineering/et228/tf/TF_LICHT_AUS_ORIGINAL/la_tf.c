
/* 
 * File:   la_tf.c
 * 
 * Responsibilities 
 * 
 * -- implement promised API 
 */
#include "la_tf.h"

#define UID_LEN (8)

//IPConnection
#define HOST "localhost"
#define PORT 4223
static IPConnection ipcon;

//Display
// Create device object
static OLED128x64V2 display;
static int display_valid;
static char display_uid[UID_LEN];

typedef struct {
    RGBLEDButton button;
    int button_valid;
    char button_uid[UID_LEN];

} rgb_button_t;

//RGB Buttons 
rgb_button_t buttons[7];

extern void la_tf_init() {
    log_msg(FLOW, __FUNCTION__);
    /* Create IP connection */

    for (int i = 0; i < 7; i++) {
        buttons[i].button_valid=0;
    }
    display_valid = 0;


    ipcon_create(&ipcon);

    /* Connect to brickd */
    if (ipcon_connect(&ipcon, HOST, PORT) < 0) {
        fprintf(stderr, "Error: Could not connect to brickd. Exit.\n");
        exit(EXIT_FAILURE);
    }
    /* Register enumeration callback to "cb_enumerate" */
    ipcon_register_callback(&ipcon,
            IPCON_CALLBACK_ENUMERATE,
            (void *) cb_enumerate,
            NULL);

    ipcon_register_callback(&ipcon, IPCON_CALLBACK_CONNECTED, (void *) cb_connect, NULL);
    ipcon_register_callback(&ipcon, IPCON_CALLBACK_DISCONNECTED, (void *) cb_disconnect, NULL);
#ifdef __CYGWIN__
    printf("Compiled with CYGWIN. Wait, then enumerate ...\n");
    sleep(1);
    ipcon_enumerate(&ipcon);
#endif
}

static void cb_connect(uint8_t connect_reason, void *user_data) {
    log_msg(FLOW, __FUNCTION__);
    ipcon_enumerate(&ipcon);
}

static void cb_disconnect(uint8_t connect_reason, void *user_data) {
    log_msg(FLOW, __FUNCTION__);
    
    for (int i = 0; i < 7; i++) {
        buttons[i].button_valid=0;
        rgb_led_button_destroy(&buttons[i].button);
    }
    display_valid = 0;
    oled_128x64_v2_destroy(&display);
}

static void cb_enumerate(const char *uid, const char *connected_uid,
        char position, uint8_t hardware_version[3],
        uint8_t firmware_version[3], uint16_t device_identifier,
        uint8_t enumeration_type, void *user_data) {
    log_msg(FLOW, __FUNCTION__);
    switch (enumeration_type) {
        case IPCON_ENUMERATION_TYPE_DISCONNECTED:
            check_for_disconnection(uid);
            break;
        case IPCON_ENUMERATION_TYPE_CONNECTED:
        case IPCON_ENUMERATION_TYPE_AVAILABLE:
            check_for_connection(uid, position);
            break;
        default:
            /* illegal enumeration_type */
            break;
    }
}

static void check_for_connection(const char * uid, char position) {
    log_msg(FLOW, __FUNCTION__);
    int i;
    switch (position) {
        case 'a':
            if (!display_valid) {
                oled_128x64_v2_create(&display, uid, &ipcon);
                display_valid = 1;
                set_uid(uid, display_uid);
                //Display lÃ¶schen?
            }
            break;
        case 'b':
            i = 2;
            break;
        case 'c':
            i = 3;
            break;
        case 'd':
            i = 4;
            break;
        case 'e':
            i = 1;
            break;
        case 'f':
            i = 0;
            break;
        case 'g':
            i = 6;
            break;
        case 'h':
            i = 5;
            break;
        default:
            log_msg(ERROR, &position);
            /* ignore all other */
            break;
    }
    if (!buttons[i].button_valid) {
        buttons[i].button_valid = 1;
        set_uid(uid, buttons[i].button_uid);
        rgb_led_button_create(&buttons[i].button, uid, &ipcon);
        rgb_led_button_register_callback(&buttons[i].button, RGB_LED_BUTTON_CALLBACK_BUTTON_STATE_CHANGED,
                (void (*)(void))cb_button_state_changed,
                &uid);
    }
}

static void set_uid(const char * connected_uid, char * uid) {
    log_msg(FLOW, __FUNCTION__);
#ifdef __CYGWIN__
    strncpy(uid, connected_uid, UID_LEN);
#else    
    strlcpy(uid, connected_uid, UID_LEN);
#endif
}

static void check_for_disconnection(const char * uid) {
    
    for (int i = 0; i < 7; i++) {
        if (!strcmp(uid, buttons[i].button_uid)) {
        buttons[i].button_valid = 0;
        rgb_led_button_destroy(&buttons[i].button);
        }
    }
    if (!strcmp(uid, display_uid)) {
        display_valid = 0;
        oled_128x64_v2_destroy(&display);
    }
}

extern int tf_display_is_valid() {
    return display_valid;
}

extern int tf_button_is_valid(int buttonindex) {
    if (buttonindex >= 0 && buttonindex <= 7) {
    return buttons[buttonindex].button_valid;
}
    else { log_msg(ERROR, "Ungültige Button Nummer");
    return -1;
    }
}


extern OLED128x64V2 * tf_get_display() {
    return display_valid ? &display : 0;
}

extern RGBLEDButton * tf_get_button(int buttonindex) {
    if (buttonindex >= 0 && buttonindex <= 7) {
    return buttons[buttonindex].button_valid ? &buttons[buttonindex].button : 0;
}
    else { log_msg(ERROR, "Ungültige Button Nummer");
    return 0;
    }
}


