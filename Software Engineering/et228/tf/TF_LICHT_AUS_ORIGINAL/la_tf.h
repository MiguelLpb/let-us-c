
/* 
 * File:   la_tf.h
 * 
 * responsibilities
 * -- provide TF harware and information about it
 */

#ifndef LA_TF_H
#define LA_TF_H

#include "ip_connection.h"
#include "bricklet_oled_128x64_v2.h"
#include "bricklet_rgb_led_button.h"
#include <stdio.h>
#include <unistd.h>  //ansonsten geht sleep Funktion nicht

#include "logger.h"
#include "la_lamp.h"
extern void la_tf_init();
static void cb_connect(uint8_t connect_reason, void *user_data);
static void cb_disconnect(uint8_t connect_reason, void *user_data);
static void cb_enumerate(const char *uid, const char *connected_uid,
        char position, uint8_t hardware_version[3],
        uint8_t firmware_version[3], uint16_t device_identifier,
        uint8_t enumeration_type, void *user_data);

static void check_for_connection(const char * uid, char position);
static void set_uid(const char * connected_uid, char * uid);
static void check_for_disconnection(const char * uid);

extern int tf_display_is_valid();
extern int tf_button_is_valid(int buttonindex);

extern OLED128x64V2 * tf_get_display();
extern RGBLEDButton * tf_get_button(int buttonindex);


#endif /* LA_TF_H */

