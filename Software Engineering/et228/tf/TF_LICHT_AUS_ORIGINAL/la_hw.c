
/* 
 * File:   la_game_hw.c
 * 
 * Responsibilities 
 * -- implement promised API 
 * 
 * Note: depends on HW
 */

#include "la_hw.h"
#include "la_config.h"
#include <stdlib.h>
#include "la_tf.h"

extern void la_hw_init() {
}

static int tf_is_ready();

extern int la_hw_is_ready() {
    return la_config_output_to_console() || tf_is_ready();
}

static int tf_is_ready() {
    for (int i = 0; i < 7; i++) {
        if (!tf_button_is_valid(i)) {
            return 0;
        }
    }
    return tf_display_is_valid();
}
