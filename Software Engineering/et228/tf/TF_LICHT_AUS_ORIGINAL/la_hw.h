
/* 
 * File:   la_hw.h
 * 
 * responsibilities
 * -- provide info if hw is ready the the application 
 * -- respect "virtual" lamps, display  
 * -- respect console output
 */

#ifndef LA_HW_H
#define LA_HW_H

extern void la_hw_init();

extern int la_hw_is_ready();

#endif /* LA_HW_H */

