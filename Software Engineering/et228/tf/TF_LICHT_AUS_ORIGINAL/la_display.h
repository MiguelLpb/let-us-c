
/* 
 * File:   la_display.h
 * responsibilities
 * -- provide interface to the display
 * -- provide output to console
 */

#ifndef LA_DISPLAY_H
#define LA_DISPLAY_H

#include "la_lamp.h"

extern void la_display_init();
extern void la_display_is_ready();

/*
 * prelimminary 
 */

extern void la_display_msg(char * s);
extern void la_display_won();
extern void la_display_lamps(lamp_t * lamps);

#endif /* LA_DISPLAY_H */

