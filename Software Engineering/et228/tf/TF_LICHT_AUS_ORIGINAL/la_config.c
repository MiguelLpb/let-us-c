
/* 
 * File:   la_config.c
 * 
 * Responsibilities 
 * -- implement promised API 
 */

#include "la_config.h"
#include "logger.h"

#include <ctype.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const unsigned int HW_FAILURE_DELAY_MS = 20;
const unsigned int GAME_POLLING_DELAY_MS = 50;

static char tf_host [20] = "localhost";
static log_level_t log_level = NONE;
static int output_to_console = 0;
static int slow_mode = 0;
static int input_from_console = 0;

static void log_option_error(char c);


/*
 * https://www.gnu.org/software/libc/manual/html_node/Getopt.html#Getopt
 */
void la_config_init(int argc, char** argv)
{
    opterr = 0; /* do not print message */
    int c;
    while ((c = getopt(argc, argv, "soil:h:")) != -1)
        switch (c)
        {
            case 'o':
                output_to_console = 1;
                break;
            case 'i':
                input_from_console = 1;
                break;
            case 'l':
                log_level = atoi(optarg);
                break;
            case 'h':
                strcpy(tf_host, optarg);
                break;
            case 's':
                slow_mode = 1;
                break;
            default:
                log_option_error(optopt);
        }
}


static void log_option_error(char c)
{
    char msg[100];
    sprintf(msg, "unknown option '%c'", c);
    log_msg(ERROR, msg);
}


int la_config_log_level_as_int()
{
    return log_level;
}


extern unsigned int la_config_get_game_polling_delay_ms()
{
    return GAME_POLLING_DELAY_MS * ((slow_mode) ? 10 : 1);
}

extern int la_config_output_to_console()
{
    return output_to_console;
}
extern int la_config_input_from_console()
{
    return input_from_console;
}


