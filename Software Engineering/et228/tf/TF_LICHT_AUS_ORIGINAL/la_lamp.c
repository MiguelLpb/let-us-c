
/* 
 * File:   la_lamp.c
 * 
 * Responsibilities 
 * -- implement promised API 
 * 
 * Note: depends on HW
 */
#include "la_lamp.h"
#include "la_rule.h"
#include "la_display.h"

#include "logger.h"

#include <string.h>
#include <stdio.h>

#include "bricklet_rgb_led_button.h"

/*
 * the array of 7 lamps 
 */
static lamp_t lamps[7];

/* 
 * TF RGBLEDButtons
 * 
 * if valid, lamps[i].info will be &tf_lamps[i]
 */
static RGBLEDButton tf_lamps[7];


extern int la_lamp_is_ready(int nr)
{
    return (lamps[nr].info != NULL);
}


void la_lamp_toogle_lamp(int nr)
{
    log_entry(__FUNCTION__);

    la_rule_toogle_lamp(lamps, nr);
    char msg[100];
    sprintf(msg, "lamp %d to %s", nr, lamps[nr].is_on ? "on" : "off");
    la_display_msg(msg);
}


void la_lamp_init()
{
    log_entry(__FUNCTION__);

    /* all lamps well defined */
    for (int i = 0; i < 7; i++)
    {
        lamps[i].info = NULL;
    }
}



extern void la_lamp_display_all()
{
    log_entry(__FUNCTION__);
    la_display_lamps(lamps);
}


extern int la_lamp_get_no_off_lamps_on()
{
    log_entry(__FUNCTION__);

    int n = 0;
    for (int i = 0; i < 7; i++)
    {
        if (lamps[i].is_on)
        {
            n++;
        }
    }
    return n;
}

extern void cb_button_state_changed(uint8_t state, void *user_data){
    printf(user_data); // Nur zum testen, kann später gelöscht werden
}
