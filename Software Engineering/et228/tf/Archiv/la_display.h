
/* 
 * File:   la_display.h
 * responsibilities
 * -- provide interface to the display
 * -- provide output to console
 */

#ifndef LA_DISPLAY_H
#define LA_DISPLAY_H

#include "la_lamp.h"

/*
 * perform any necessary initialisation
 */
extern void la_display_init();

/*
 * returns 1 if display is ready, 0 otherwise
 */
extern int la_display_is_ready();


/*
 * prelimminary 
 */

/*
 * clears the hardware display
 */
extern void la_display_clear();

/*
 * output a "draw" (<nr> pressed ) on line 5
 */
extern void la_display_draw(int nr);


/*
 * output <s> on line 3
 */
extern void la_display_msg(char * s);

/*
 * output a "draw" (<nr> pressed ) on line 5
 */
extern void la_display_status(int draw , int on);

/*
 * output <s> on line 0
 */
extern void la_display_title(char * s);

/*
 * output winning message on line 2
 */
extern void la_display_won();


#endif /* LA_DISPLAY_H */

