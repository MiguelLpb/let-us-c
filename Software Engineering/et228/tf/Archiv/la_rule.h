
/* 
 * File:   la_rule.h
 * 
 * responsibilities
 * -- just one switch lamp according to the rules
 */

#ifndef LA_RULE_H
#define LA_RULE_H

#include "la_lamp.h"


/*
 * toggle <nr> within <size>-many 1/0 <toggles> according to LICHT AUS rule if 
 * 0 <= <nr> < <size>
 * 
 * do nothing otherwise
 * 
 * Example :
 * size 7,  nr 6            x         X x
 * toggles 1 0 1 1 0 0 1 -> 0 0 1 1 0 1 0
 */
extern void la_rule_toogle(int * toggles, int size, int nr);

#endif /* LA_RULE_H */

