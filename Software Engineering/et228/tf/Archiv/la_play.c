
/* 
 * File:   la_play.c
 * 
 * Responsibilities 
 * -- implement promised API 
 * 
 * Note: does NOT on HW
 */
#include "la_play.h"

#include "logger.h"
#include "timer.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "la_config.h"
#include "la_hw.h"
#include "la_lamp.h"
#include "la_display.h"

#include "la_solver.h"


/*
 * **************************************************************************
 */

/*
 * basic game states
 * 
 * remark: future modes (e.g. fight count down) may require additional 
 * states (e.g. LOST)
 */
typedef enum
{
    HW_IS_NOT_READY, INIT, PLAY, SOLVE, WON,
} la_state_t;

/* ************************************************************************** */

static la_state_t handle_HW_IS_NOT_READY();
static la_state_t handle_INIT();
static la_state_t handle_PLAY();
static la_state_t handle_SOLVE();
static la_state_t handle_WON();
static la_state_t handle_illegal_state(la_state_t state);

/* ************************************************************************** */

static int no_of_draws;
static struct timeval start_time;


/*
 * play the game
 * 
 * notes: 
 * -- this is a loop to make timed events possible
 * -- the loop handles the current state using switch .. case
 * -- the handling functions return the next state, which can be the same
 * -- async behaviour is handled by callbacks in the hw specific parts
 * todo
 * -- if hw becomes unavailable and available again, this has to happen smoothly
 * -- right now: start over (INIT) when hw becomes available
 */
extern void la_play()
{
    log_entry(__FUNCTION__);

    la_state_t la_state = INIT;
    while (1)
    {
        switch (la_state)
        {
            case HW_IS_NOT_READY:
                la_state = handle_HW_IS_NOT_READY();
                break;
            case INIT:
                la_state = handle_INIT();
                break;
            case PLAY:
                la_state = handle_PLAY();
                break;
            case SOLVE:
                la_state = handle_SOLVE();
                break;
            case WON:
                la_state = handle_WON();
                break;
            default:
                la_state = handle_illegal_state(la_state);
                break;
        }
        if (!la_hw_is_ready())
        {
            la_state = HW_IS_NOT_READY;
        }
        timer_sleep_ms(la_config_get_game_polling_delay_ms());
    }
}


/* ************************************************************************** */

static la_state_t handle_HW_IS_NOT_READY()
{
    log_entry(__FUNCTION__);

    return la_hw_is_ready() ? INIT : HW_IS_NOT_READY;
}

/* ************************************************************************** */


static void choose_and_set_lamps(int max);


static la_state_t handle_INIT()
{
    log_entry(__FUNCTION__);
    la_display_clear();
    la_display_title("LICHT AUS!");
    la_display_msg("Druecke die Taster! ");
    la_lamp_all(0);
    choose_and_set_lamps(NO_OF_LAMPS);
    no_of_draws = 0;

    gettimeofday(&start_time, NULL);
    la_display_status(no_of_draws, la_lamps_on());
    return PLAY;
}

static int choose_and_set_at_random(int max);


static void choose_and_set_lamps(int max)
{
    srand(time(NULL));
    int lamps_on;
    do
    {
        lamps_on = choose_and_set_at_random(max);
    }
    while (!lamps_on);
}


static int choose_and_set_at_random(int max)
{
    int lamps_on = 0;
    for (int i = 0; i < max; i++)
    {
        int r = rand() % 2;
        if (r)
        {
            la_lamp_toggle_lamp(i);
            lamps_on++;
            timer_sleep_ms(250);
        }
    }
    return lamps_on;
}

/* ************************************************************************** */

static int get_no_of_lamp_to_switch();
static void perform_draw(int nr);

static const double MAX_IDLE_MS_BEFORE_SOLVE = 5.0 * 1000;

static la_state_t handle_PLAY()
{
    la_state_t result = PLAY;

    struct timeval now;
    int nr = get_no_of_lamp_to_switch();
    if (nr >= 0)
    {
        perform_draw(nr);
    }
    else
    {
        gettimeofday(&now, NULL);
        double t;
        if ((t = timer_get_difference_ms(start_time, now)) > MAX_IDLE_MS_BEFORE_SOLVE)
        {
            char msg[100];
            sprintf(msg, "TIMEOUT keine Eingabe seit %.1f sec., Loesung...", t/1000);
            log_msg (INFO, msg);
            result = SOLVE;
        }
        else {
            char msg[100];
            sprintf(msg, "Verbleibende Zeit %.1f sec.", (MAX_IDLE_MS_BEFORE_SOLVE-t)/1000);
            log_msg (INFO, msg);
        }
    }
    if (la_lamp_get_no_off_lamps_on() == 0)
    {
        result = WON;
    }
    return result;
}

static int get_no_from_console(int max);
static int get_no_from_lamps(int max);


static int get_no_of_lamp_to_switch()
{
    int lamp_to_switch;
    if (la_config_input_from_console())
    {
        lamp_to_switch = get_no_from_console(NO_OF_LAMPS);
    }
    else
    {
        lamp_to_switch = get_no_from_lamps(NO_OF_LAMPS);
    }
    return lamp_to_switch;
}


static void perform_draw(int nr)
{
    gettimeofday(&start_time, NULL);
    no_of_draws++;
    la_display_draw(nr);
    la_lamp_toggle_lamp(nr);
    la_display_status(no_of_draws, la_lamps_on());
}


static int get_no_from_console(int max)
{
    int lamp_to_switch;
    char c;
    char none = '-';
    do
    {
        char msg[100];
        sprintf(msg, "Eingabe Nummer (0..%d) oder '%c' fuer keine Eingabe: ", max-1, none );
        log_prompt(msg);
        c = getchar();
        getchar(); /* read enter char */
        if (c == none)
        {
            return -1;
        }
    }
    while (c < '0' || c > ('0' + max -1));
    lamp_to_switch = c - '0';
    return lamp_to_switch;
}


static int get_no_from_lamps(int max)
{
    int lamp_to_switch = -1;
    for (int i = 0; i < max; i++)
    {
        if (la_lamp_to_toggle(i))
        {
            lamp_to_switch = i;
            break;
        }
    }
    return lamp_to_switch;
}
/* ************************************************************************** */

static void query_lamp_states(int * states, int cnt);
static void solve_it(int* draw, int cnt);


static la_state_t handle_SOLVE()
{
    log_entry(__FUNCTION__);

    int states[NO_OF_LAMPS];
    query_lamp_states(states, NO_OF_LAMPS);
    int draws[NO_OF_LAMPS];
    int total = la_get_solution(states, NO_OF_LAMPS, draws);
    solve_it(draws, NO_OF_LAMPS);
    return WON;
}


static void query_lamp_states(int * states, int cnt)
{
    for (int i = 0; i < NO_OF_LAMPS; i++)
    {
        states[i] = la_lamp_is_on(i);
    }
}


static void solve_it(int* draws, int cnt)
{
    for (int nr = 0; nr < cnt; nr++)
    {
        if (draws[nr])
        {
            no_of_draws++;
            int on = la_lamp_is_on(nr);
            la_lamp_set_hint(nr);
            timer_sleep_ms(500);
            la_lamp_set_lamp(nr, on);
            timer_sleep_ms(500);
            la_display_draw(nr);
            char msg[100];
            sprintf(msg, "toggle lamp  %d", nr);
            log_msg(INFO, msg);
            la_lamp_toggle_lamp(nr);
            la_display_status(no_of_draws, la_lamps_on());
            timer_sleep_ms(500);
        }
    }
}


/* ************************************************************************** */

static la_state_t handle_WON()
{
    log_entry(__FUNCTION__);

    la_display_clear();
    la_display_won();
    int on = 1;
    for (int i = 0; i < 5; i++)
    {
        la_lamp_all(on);
        on = !on;
        timer_sleep_ms(100);
    }
    timer_sleep_ms(500);

    return INIT;
}


/* ************************************************************************** */


la_state_t handle_illegal_state(la_state_t state)
{
    log_entry(__FUNCTION__);

    char msg[100];
    sprintf(msg, "illegal state %d", state);
    log_msg(ERROR, msg);
    return INIT;
}
