
/* 
 * File:   switch.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on February 16, 2018, 9:02 AM
 */

#ifndef DIMMER_H
#define DIMMER_H

#include <stdint.h>


extern void dimmer_init(void);

typedef void (*dimmer_callback_t)(uint8_t ); 

extern void dimmer_subscribe(dimmer_callback_t cb);

#endif /* DIMMER_H */

