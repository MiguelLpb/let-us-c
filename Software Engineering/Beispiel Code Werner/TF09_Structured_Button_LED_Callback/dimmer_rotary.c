
/* 
 * File:   dimmer_rotary.c
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 4. Mai 2019, 17:24
 */
#include <stdio.h>

#include "dimmer.h"
#include "logger.h"
#include "tf.h"

static dimmer_callback_t dimmer_callback;


extern void dimmer_subscribe(dimmer_callback_t cb)
{
    dimmer_callback = cb;
}


void cb_position_has_changed(int16_t position)
{
    log_entry ("%s\n", __FUNCTION__);
    if (do_logging) 
    {
        printf("Position: %hd\n", position);
    }
    float intensity = position + 150.f;
    intensity = (intensity / 300.f) * 255.f;
    dimmer_callback ((uint8_t) intensity);
}


extern void dimmer_init(void)
{
    dimmer_callback = 0;
    tf_register_rotary_poti_cb_client(cb_position_has_changed);
}
