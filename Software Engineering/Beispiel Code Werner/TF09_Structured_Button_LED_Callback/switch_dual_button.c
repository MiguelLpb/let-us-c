
/* 
 * File:   switch.c
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on February 16, 2018, 9:03 AM
 */

#include <stdio.h>

#include "switch.h"
#include "tf.h"
#include "logger.h"

static switch_callback_t switch_callback;
static switch_state_t switch_state;

static void on_dual_button_state_changed_left(uint8_t, uint8_t);


extern void switch_init(void)
{
    log_entry("%s\n", (char*) __FUNCTION__);
    switch_callback = 0;
    switch_state = SWITCH_STATE_OFF;
}


extern void switch_subscribe(switch_callback_t cb)
{
    log_entry("%s\n", (char*) __FUNCTION__);
    tf_register_dual_button_cb_client_left(on_dual_button_state_changed_left);
    switch_callback = cb;
}


static void on_dual_button_state_changed_left(uint8_t button_l, uint8_t button_r)
{
    log_entry("%s\n", (char*) __FUNCTION__);
    if (button_l == DUAL_BUTTON_BUTTON_STATE_RELEASED)
    {
        log_entry("%s\n", "Left button released");
        if (switch_state == SWITCH_STATE_OFF)
        {
            switch_state = SWITCH_STATE_ON;
        }
        else
        {
        log_entry("%s\n", "Left button pressed");
            switch_state = SWITCH_STATE_OFF;
        }
        dual_button_set_selected_led_state(
                tf_get_dual_button(), 
                DUAL_BUTTON_LED_STATE_AUTO_TOGGLE_ON, 
                switch_state ? DUAL_BUTTON_LED_STATE_ON : DUAL_BUTTON_LED_STATE_OFF);
        switch_callback(switch_state);
    }
}


extern int switch_is_on()
{
    return switch_state;
}