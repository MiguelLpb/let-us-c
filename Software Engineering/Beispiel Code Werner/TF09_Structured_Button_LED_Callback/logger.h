
/* 
 * File:   logger.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 23. März 2020, 21:18
 */

#ifndef LOGGER_H
#define LOGGER_H

#ifdef __cplusplus
extern "C"
{
#endif

    extern int do_logging;
    extern void log_entry (char* format, const char * s);

#ifdef __cplusplus
}
#endif

#endif /* LOGGER_H */

