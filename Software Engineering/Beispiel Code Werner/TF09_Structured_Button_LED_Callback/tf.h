
/* 
 * File:   tf.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on February 16, 2018, 9:04 AM
 */

#ifndef TF_H
#define TF_H

#include "bricklet_rotary_poti.h"
#include "bricklet_dual_button.h"
#include "bricklet_rgb_led.h"


extern int tf_rgb_led_is_valid();
extern RGBLED * tf_get_rgb_led();

extern int tf_dual_button_is_valid();
extern DualButton * tf_get_dual_button();
extern void tf_register_dual_button_cb_client_left();
extern void tf_register_dual_button_cb_client_right();


extern int tf_rotary_poti_is_valid();
extern RotaryPoti * tf_get_rotary_poti();
extern void tf_register_rotary_poti_cb_client(void (* cb) (int16_t));

extern void tf_init();

extern void tf_exit();

#endif /* TF_H */

