
#ifndef CONTACT_H
#define CONTACT_H

extern void contact_init(void);

typedef void (*contact_callback_t)();

extern void contact_subscribe(contact_callback_t cb);


#endif /* CONTACT_H */

