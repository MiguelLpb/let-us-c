
/* 
 * File:   switch.c
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on February 16, 2018, 9:03 AM
 */

#include <stdio.h>

#include "switch.h"
#include "tf.h"

static callback notify;

typedef enum _state
{
    OFF, TO_ON, ON, TO_OFF
} state_t;

static state_t state = OFF;

extern void switch_init(void)
{
    notify = 0;
}

#define SECONDS_TO_PRESS 2

extern void switch_subscribe(callback cb)
{
    notify = cb;
}

static void start_counting_time();
static unsigned int get_time_count();


void on_bricklet_state_changed(uint8_t button_l, uint8_t button_r, uint8_t led_l, uint8_t led_r,
        void *user_data)
{
    printf("%s\n", __FUNCTION__);

    (void) led_l; // avoid unused parameter warning
    (void) led_r; // avoid unused parameter warning
    (void) user_data; // avoid unused parameter warning

    switch (state) {
        case OFF:
            if (button_l == DUAL_BUTTON_BUTTON_STATE_PRESSED)
            {
                printf("Left button pressed\n");
                start_counting_time();
                state = TO_ON;
            }
            break;
        case TO_ON:
            if (button_l == DUAL_BUTTON_BUTTON_STATE_RELEASED)
            {
                printf("Left button released\n");
                unsigned int time = get_time_count();
                printf("Time passed %u\n", time);
                if (time >= SECONDS_TO_PRESS)
                {
                    state = ON;
                    dual_button_set_selected_led_state(&db, 0, 2);
                    notify();
                }
                else
                {
                    state = OFF;
                }
            }
            break;
        case ON:
            if (button_l == DUAL_BUTTON_BUTTON_STATE_PRESSED)
            {
                printf("Left button pressed\n");
                start_counting_time();
                state = TO_OFF;
            }
            break;
        case TO_OFF:
            if (button_l == DUAL_BUTTON_BUTTON_STATE_RELEASED)
            {
                printf("Left button released\n");
                unsigned int time = get_time_count();
                if (time > SECONDS_TO_PRESS)
                {
                    state = OFF;
                    dual_button_set_selected_led_state(&db, 0, 3);
                    notify();
                }
                else
                {
                    state = ON;
                }
            }
            break;
        default:
            printf("Upps. Illegal state %u\n", state);
            break;
    }
}


static time_t start;

static void start_counting_time()
{
    time(&start);
}

static unsigned int get_time_count()
{
    time_t end;
    time(&end);
    double dif = difftime(end, start);
    return dif;
}

extern int switch_is_on()
{
    return (state == ON);
}