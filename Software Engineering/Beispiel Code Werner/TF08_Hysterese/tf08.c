
/* 
 * File:   tf08.c
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on March 6, 2018, 5:11 PM
 */

#include <stdio.h>
#include <stdlib.h>


#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 


#include "tf.h"
#include "lamp.h"
#include "switch.h"

static void show_info();

static void on_poti_change_function();


#define LAMP_OFF 0
#define LAMP_ON 1

static int lamp_state;


int main(int argc, char **argv)
{
    show_info();
    tf_init();

    switch_init();
    switch_subscribe(on_poti_change_function);

    lamp_state = LAMP_OFF;
    lamp_to_off();

    while (1)
    {
        sleep(1);
    }


    tf_end();
    return (EXIT_SUCCESS);
}


static void on_poti_change_function()
{
    switch (lamp_state)
    {
        case LAMP_ON:
            if (!switch_is_on())
            {
                printf("Lamp to off.\n");
                lamp_to_off();
                lamp_state = LAMP_OFF;
            }
            break;
        case LAMP_OFF:
            if (switch_is_on())
            {
                printf("Lamp to on.\n");
                lamp_to_on();
                lamp_state = LAMP_ON;
            }
            break;
        default:
            printf("Unknown state of lamp!\n");
    }
}


static void show_info()
{
    printf("Example Program: %s\n\n", __FILE__);
    printf("Hardware recognition: Dynamically detecting Rotary Bricklet\n");
    printf("Application logic ..: Callback and Hysteresis\n");
    printf("Source files .......: Multiple (Modular)\n\n");
    printf("Rotary Poti switches LED on and off again!\n\n");
    printf("Press return to start ... ");
    getchar();
    printf("\n");
}

