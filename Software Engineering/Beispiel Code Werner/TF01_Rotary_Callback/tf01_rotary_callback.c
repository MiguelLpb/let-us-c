/* 
 * file:   tf01_rotary_callback.c
 * author: uwe
 * 
 * values from a Rotary Bricklet with given UID using callback 
 *
 * created on 13. April 2017, 11:09
 */

#include <stdio.h>
#include <unistd.h> 

#include "ip_connection.h"
#include "bricklet_rotary_poti.h"

#define HOST "localhost"
#define PORT 4223
#define UID "y6V" // Change XYZ to the UID of your Rotary Poti Bricklet


int usleep(useconds_t useconds); /* pass in microseconds */


/*
 *******************************************************************************
 * 
 * typedef of the callback function 
 *
 *******************************************************************************
 */

typedef void (*cb_function_t)( int16_t, void *) ;


/*
 *******************************************************************************
 * 
 * prototypes 
 *
 *******************************************************************************
 */

static void cb_position_has_changed(int16_t position, void *user_data);

static int check_for_limit(int argc, char **argv);

static void show_info();
static void init_tf( cb_function_t, void *) ;
static void do_loop(int * no_of_changes, int limit);
static void end_tf();


static IPConnection ipcon;
static RotaryPoti rp;

/*
 *******************************************************************************
 * 
 * main 
 *
 * call example: <pgm name> 2
 * 
 *******************************************************************************
 */

int main(int argc, char **argv)
{
    int no_of_changes = 0; /* counter only visible in main !!! */
    
    int limit = check_for_limit(argc, argv);
    show_info(limit, UID);
    init_tf(cb_position_has_changed, &no_of_changes);
    do_loop(&no_of_changes, limit);
    end_tf();
    return (EXIT_SUCCESS);
}


/*
 * application "main logic" print value on change and number of change
 */

void cb_position_has_changed(int16_t position, void *user_data)
{
    int * i_ptr = (int *) user_data;
    (*i_ptr)++; /* count change */
    printf("(%d) position: %hd\n", *i_ptr, position);
}




/*
 * returns the number if provided as first argument in argv or zero
 */
static int check_for_limit(int argc, char **argv)
{
    const int POSITION = 1;
    int limit = 0;
    if (argc > POSITION)
    {
        limit = atoi(argv[POSITION]);
    }
    return limit;
}

/*
 * print information
 */

static void show_info(int limit, char * uid)
{
    char buffer[50];
    sprintf(buffer, "%d times", limit);
    printf("Example Program: %s\n\n", __FILE__);
    printf("Hardware recognition: Static UID '%s' \n", uid);
    printf("Application logic ..: Callback - " );
    if (limit)
    {
        printf ("for %d change%s", limit, (limit ? "s" : "") );
    }
    else
    {
        printf (" forever.");
    }
    printf ("\n");
    printf("Source files .......: Just one.\n\n");
    printf("Press return to start ... ");
    getchar();
    printf("\n");
}

/*
 * initialize IPConnection ipcon 
 * exit program on fail
 */

static void init_tf(cb_function_t cb, void * ptr)
{
    /* Create IP connection */
    ipcon_create(&ipcon);

    /* Create device object */
    rotary_poti_create(&rp, UID, &ipcon);

    /* Connect to brickd */
    if (ipcon_connect(&ipcon, HOST, PORT) < 0)
    {
        fprintf(stderr, "Error: Could not connect to brickd. Exit.\n");
        exit(EXIT_FAILURE);
    }
    rotary_poti_register_callback(&rp,
            ROTARY_POTI_CALLBACK_POSITION,
            (void *) cb,
            ptr);

    uint32_t period = 50;
    printf("Period for position callback set to %.2fs (%ums)\n", period / 1000., period);
    printf("(Note: The position callback is only called every %.2f seconds "
            "if the position has changed since the last call!)\n", period / 1000.);
    rotary_poti_set_position_callback_period(&rp, 50);
}


/*
 * empty loop with one second sleep
 * <limit>: finish after more than limit changes , forever if zero
 */

static void do_loop(int * no_of_changes, int limit)
{
    while (1)
    {
        /* sleep(1); */
        usleep(10);
        if (limit && (* no_of_changes >= limit))
        {
            return;
        }
    }
}



/*
 * end tf_ipcon 
 */

static void end_tf()
{
    rotary_poti_destroy(&rp);
    ipcon_destroy(&ipcon);
}

