/* 
 * File:   lamp.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on February 16, 2018, 9:04 AM
 */

#ifndef LAMP_H
#define LAMP_H

extern void lamp_to_on();
extern void lamp_to_off();

#endif /* LAMP_H */

