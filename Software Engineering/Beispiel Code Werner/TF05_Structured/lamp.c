
/* 
 * File:   lamp.c
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on February 16, 2018, 9:03 AM
 */

#include <stdio.h>

extern void lamp_to_on()
{
    printf("V I R T U A L  Lamp on...\n");
}


extern void lamp_to_off()
{
    printf("V I R T U A L  Lamp off.\n");
}
