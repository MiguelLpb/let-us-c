
/* 
 * File:   tf.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on February 16, 2018, 9:04 AM
 */

#ifndef TF_H
#define TF_H

#include "bricklet_dual_button.h"

extern DualButton db;
extern int dual_button_is_valid;
 

extern void init_tf();

extern void end_tf();

#endif /* TF_H */

