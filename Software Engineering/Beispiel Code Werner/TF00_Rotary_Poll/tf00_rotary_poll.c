
/* 
 * file:   tf00_rotary_poll.c
 * author: uwe
 * 
 * polling a Rotary Bricklet with given UID
 */

#include <stdio.h>
#include <unistd.h> 

#include "ip_connection.h"
#include "bricklet_rotary_poti.h"

#define HOST "localhost"
#define PORT 4223
#define UID "y6V" 

#define DELAY "DELAY"

/*
 *******************************************************************************
 * 
 * prototypes 
 *
 *******************************************************************************
 */

static int check_for_delay_as_argument(int argc, char **argv);
static int check_for_limit(int argc, char **argv);
static void show_info(int with_delay, int limit, char * uid);
static void build_up_connection(IPConnection *ipcon, char * host, uint16_t port);
static void do_loop();
static void tear_down_connection(IPConnection *ipcon);


/*
 *******************************************************************************
 * 
 * main 
 *
 * call example <pgm name> DELAY 2
 * 
 *******************************************************************************
 */

int main(int argc, char **argv)
{
    IPConnection ipcon;
    RotaryPoti rp;

    int with_delay = check_for_delay_as_argument(argc, argv);
    int limit = check_for_limit(argc, argv);
    show_info(with_delay, limit, UID);

    build_up_connection(&ipcon, HOST, PORT);
    rotary_poti_create(&rp, UID, &ipcon);

    do_loop(&rp, with_delay, limit);

    rotary_poti_destroy(&rp);
    tear_down_connection(&ipcon);
    return (EXIT_SUCCESS);
}


/*
 * returns 1 if "DELAY" is found as first argument in argv 
 */

static int check_for_delay_as_argument(int argc, char **argv)
{
    int with_delay = 0;
    if (argc > 1)
    {
        printf("Arguments provided!\n");
        for (int i = 0; i < argc; i++)
        {
            printf("%d:%s\n", i, argv[i]);
        }
        with_delay = (strcmp(argv[1], DELAY) == 0);
    }
    return with_delay;
}


/*
 * returns the number if provided as second argument in argv or zero
 */
static int check_for_limit(int argc, char **argv)
{
    const int POSITION = 2; /* second argument is third with index 2 in argv */
    int limit = 0;
    if (argc > POSITION)
    {
        limit = atoi(argv[POSITION]);
    }
    return limit;
}


/*
 * print information
 */

static void show_info(int with_delay, int limit, char * uid)
{
    char buffer[50];
    sprintf(buffer, "%d times", limit);
    printf("Program file: %s\n\n", __FILE__);
    printf("Hardware recognition: Static UID '%s' \n", uid);
    printf("Application logic ..: Polling %s%s, %s delay between polls. \n", 
            limit ? buffer : "indefinitely", 
            limit ? " times" : "",
            with_delay ? "WITH" : "without");
    printf("Source files .......: Just one.\n");
    printf("Press return to start ... ");
    getchar();
    printf("\n");
}


/*
 * initialize IPConnection ipcon 
 * exit program on fail
 */

static void build_up_connection(IPConnection *ipcon, char * host, uint16_t port)
{
    ipcon_create(ipcon);
    if (ipcon_connect(ipcon, host, port) < 0)
    {
        fprintf(stderr, "Error: Could not connect to brickd. Exit.\n");
        exit(EXIT_FAILURE);
    }
}


/*
 * repeatedly get and print rotary position 
 * <with_>: if not zero then delay repetition by one sec. 
 * <limit>: finish after more than limit repetitions 
 */

static void do_loop(RotaryPoti * rp, int with_delay, int limit)
{
    /* Get current position (range is -150 to 150) */
    int16_t position;
    int err;
    int cnt = 0;
    while (1)
    {
        if ((err = rotary_poti_get_position(rp, &position)) < 0)
        {
            fprintf(stderr, "Error: Could not get position from bricklet. Error %d. Exit.\n", err);
            exit(EXIT_FAILURE);
        }
        printf("Position: %hd\n", position);
        if (with_delay)
        {
            sleep(1);
        }
        cnt++;
        if (limit && (cnt >= limit))
        {
            return;
        }
    }
}


/*
 * tear down IPConnection ipcon 
 */

static void tear_down_connection(IPConnection *ipcon)
{
    ipcon_destroy(ipcon);
}

