
/* 
 * File:   tf05_structured.c
 * Author: uwe
 *
 * Created on 13. April 2017, 11:09
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 


#include "tf.h"
#include "lamp.h"
#include "switch.h"


static void show_info();

int main(int argc, char **argv)
{
    show_info();
    init_tf();

    while (1)
    {
        if (switch_is_on())
        {
            lamp_to_on();
        } else
        {
            lamp_to_off();
        }
        sleep(1);
    }
    exit_tf();
    return (EXIT_SUCCESS);
}

static void show_info()
{
    printf("Example Program: %s\n", __FILE__);
    printf("Hardware recognition: Dynamically detecting Rotary Bricklet\n");
    printf("Application logic ..: Polling\n");
    printf("Source files .......: Multiple (Modular)\n\n");
    printf("Left button switches 'virtual lamp' on and off again!\n\n");
    printf("Left button switches 'virtual lamp' on and off again, toggles button led!\n");
    printf("Note the  D E L A Y  of the lamp -- due to 1Hz polling!\n\n");
    printf("Press return to start ... ");
    getchar();
    printf("\n");
}




