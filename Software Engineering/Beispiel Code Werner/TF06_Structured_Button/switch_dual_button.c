
/* 
 * File:   switch.c
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on February 16, 2018, 9:03 AM
 */

#include <stdio.h>

#include "switch.h"
#include "tf.h"

static int state = 0;

void on_bricklet_state_changed(uint8_t button_l, uint8_t button_r, uint8_t led_l, uint8_t led_r,
        void *user_data)
{
    (void) led_l; // avoid unused parameter warning
    (void) led_r; // avoid unused parameter warning
    (void) user_data; // avoid unused parameter warning

    if (button_l == DUAL_BUTTON_BUTTON_STATE_PRESSED)
    {
        state = ! state;
        dual_button_set_selected_led_state(&db, 0, state?2:3);
    }
}


extern int switch_is_on()
{
    return state;
}