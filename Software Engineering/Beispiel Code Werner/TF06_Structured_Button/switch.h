
/* 
 * File:   switch.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on February 16, 2018, 9:02 AM
 */

#ifndef SWITCH_H
#define SWITCH_H

extern int switch_is_on();

#endif /* SWITCH_H */

