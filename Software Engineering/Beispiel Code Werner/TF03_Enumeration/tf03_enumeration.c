
/* 
 * File:   tf02_enumeration.c
 * Author: uwe
 *
 * Created on 13. April 2017, 11:09
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 

#include "ip_connection.h"

/*
 *******************************************************************************
 * 
 * prototypes 
 *
 *******************************************************************************
 */
static void cb_enumerate(const char *uid, const char *connected_uid,
        char position, uint8_t hardware_version[3],
        uint8_t firmware_version[3], uint16_t device_identifier,
        uint8_t enumeration_type, void *user_data);

static void cb_connect(uint8_t connect_reason, void *user_data);
static void cb_disconnect(uint8_t connect_reason, void *user_data);

static void show_info();
static void tf_init();
static void tf_end();
static void build_up_callbacks();

/*
 *******************************************************************************
 * 
 * super simple main, no arguments
 *
 *******************************************************************************
 */

int main(int argc, char **argv)
{
    show_info();
    tf_init();
    sleep(1);
    printf("Press return to exit ...\n");
    getchar();
    tf_end();
   
    return (EXIT_SUCCESS);
}

static void show_info()
{
    printf("Example Program: %s\n", __FILE__);
    printf("Hardware recognition: Dynamically enumerating Tinkerforge Bricks and Bricklets\n"
           "                      USB disconnect and connect triggers callback!\n");
    printf("Application logic ..: N/A (no application)\n");
    printf("Source files .......: Just one.\n\n");
    printf("Press return to start ... ");
    getchar();
    printf("\n");
}


/*
 * Tinkerforge  
 */

#define HOST "localhost"
#define PORT 4223

static IPConnection ipcon;

static void build_up_callbacks();

static void tf_init()
{
    /* Create IP connection */
    ipcon_create(&ipcon);

    /* Connect to brickd */
    if (ipcon_connect(&ipcon, HOST, PORT) < 0)
    {
        fprintf(stderr, "Error: Could not connect to brickd. Exit.\n");
        exit(EXIT_FAILURE);
    }
    build_up_callbacks();
    /* Register enumeration callback to "cb_enumerate" */
    ipcon_register_callback(&ipcon,
            IPCON_CALLBACK_ENUMERATE,
            (void *) cb_enumerate,
            NULL);

    ipcon_register_callback(&ipcon, IPCON_CALLBACK_CONNECTED, (void *) cb_connect, NULL);

    ipcon_register_callback(&ipcon, IPCON_CALLBACK_DISCONNECTED, (void *) cb_disconnect, NULL);
}

static void build_up_callbacks()
{
    /* callbacks for enumeration of bricks and bricklets, connect to, and disconnect from demon */
    ipcon_register_callback(&ipcon,
            IPCON_CALLBACK_ENUMERATE,
            (void *) cb_enumerate,
            NULL);
    ipcon_register_callback(&ipcon, IPCON_CALLBACK_CONNECTED, (void *) cb_connect, NULL);
    ipcon_register_callback(&ipcon, IPCON_CALLBACK_DISCONNECTED, (void *) cb_disconnect, NULL);
}

/* print information on connect */
static void cb_connect(uint8_t connect_reason, void *user_data)
{
    printf("Connected to demon. Enumeration requested.\n");
    ipcon_enumerate(&ipcon);
}

/* print information on disconnect */
static void cb_disconnect(uint8_t connect_reason, void *user_data)
{
    printf("Disconnected from demon.\n");
}

/* print incoming enumeration information */
void cb_enumerate(const char *uid, const char *connected_uid,
        char position, uint8_t hardware_version[3],
        uint8_t firmware_version[3], uint16_t device_identifier,
        uint8_t enumeration_type, void *user_data)
{
    (void) user_data;

    printf("%20s : %s\n", "UID", uid);
    printf("%20s : %hhu ", "Enumeration Type", enumeration_type);
    switch (enumeration_type) {
        case IPCON_ENUMERATION_TYPE_DISCONNECTED:
            printf(" disconnected.\n");
            return;
        case IPCON_ENUMERATION_TYPE_AVAILABLE:
            printf(" available.\n");
            break;
        case IPCON_ENUMERATION_TYPE_CONNECTED:
            printf(" connected.\n");
            break;
        default:
            printf(" unknown.\n");
    }
    printf("%20s : %s\n", "Connected UID", connected_uid);
    printf("%20s : %c\n", "Position", position);
    printf("%20s : %hhu.%hhu.%hhu\n", "Hardware Version", hardware_version[0],
            hardware_version[1],
            hardware_version[2]);
    printf("%20s : %hhu.%hhu.%hhu\n", "Firmware Version", firmware_version[0],
            firmware_version[1],
            firmware_version[2]);
    printf("%20s : %hu\n", "Device Identifier", device_identifier);
    printf("\n");
}

static void tf_end()
{
    ipcon_destroy(&ipcon);
}
