
/* 
 * File:   colors.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 5. April 2018, 12:27
 */

#ifndef COLORS_H
#define COLORS_H

#include <stdlib.h>
#include <stdint.h>

typedef enum _color {BLACK, WHITE, RED, LIME, BLUE, YELLOW, CYAN, MAGENTA, MAROON, OLIVE, GREEN, PURPLE, TEAL, NAVY } color_t;

extern void colors_color_to_rgb(color_t c, uint8_t * r, uint8_t * g, uint8_t * b);

extern char * colors_get_name (color_t c);

#endif /* COLORS_H */

