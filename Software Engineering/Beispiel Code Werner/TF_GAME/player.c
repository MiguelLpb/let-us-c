
/* 
 * File:   c4_player.c
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on April 15, 2018, 10:25 AM
 */

#include <stdlib.h>
#include <sys/time.h>

#include "joysticks.h"
#include "player.h"
#include "touchpad.h"

#include "colors.h"
#include "tf.h"


extern void player_set_player(player_t *first)
{
    *first = LEFT;

    srand((unsigned) time(NULL));
    if (rand() % 2)
    {
        *first = RIGHT;

    }
    *first = LEFT;
}


extern unsigned int player_has_released(player_t p)
{
    int res = joystick_has_been_released(p);
    return res;
}


extern unsigned int player_one_player_has_released()
{
    int res = joystick_has_been_released(LEFT) || joystick_has_been_released(RIGHT);
    return res;
}


extern int player_want_to_quit(player_input_t player_input)
{
    switch (player_input)
    {
        case JOYSTICK: 
            return joysticks_one_is_long_pressed();
        case MULTITOUCH : 
            return touchpad_four_corners_pressed();
        default:
            return 0;
    }

}


extern void player_ignore_activity()
{
    joystick_reset();
}


extern player_t player_toggle_current(player_t current_player)
{
    return (current_player == LEFT) ? RIGHT : LEFT;
}


extern orientation_t player_column_change(player_t current_player)
{
    orientation_t dir_of_column_change = joystick_wants_move((orientation_t) current_player);

    return dir_of_column_change;
}


