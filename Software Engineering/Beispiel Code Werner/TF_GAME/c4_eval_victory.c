#include <stdio.h>

#include "c4_win.h"
#include "c4_field.h"

uint8_t winner_discs_on_matrix[64]; /*!< contains the 4 winner stones --> showing victory*/


static void reset_winner_discs();
static uint8_t get_row_victory(uint8_t field[64]);
static uint8_t get_column_victory(uint8_t field[64]);
static uint8_t get_cross_victory(uint8_t field[64]);


uint8_t get_victory(uint8_t field[64]) // returns the victory for all three victory cases
{
    reset_winner_discs();
    return (get_column_victory(field) || get_row_victory(field) || get_cross_victory(field));
}


#define OCCUPIED 255  
#define EMPTY 0

static void reset_winner_discs()
{
    for (int i = 0; i < C4_FIELD_SIZE; i++)
    {
        winner_discs_on_matrix[i] = EMPTY;
    }
}

                                                            


static uint8_t get_row_victory(uint8_t field[64]) // returns the victory in a row 
{
    reset_winner_discs();
    uint8_t ret = 0;
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            if (field[(i * 8) + j] == OCCUPIED)
            {
                ret++;
                winner_discs_on_matrix[(i * 8) + j] = OCCUPIED;
            }
            else
            {
                reset_winner_discs();
                ret = 0;
            }
            if (ret == 4) // 4 in a row --> victory 
            {
                return ret;
            }
        }
        ret = 0;
    }
    return ret;
}


static uint8_t get_column_victory(uint8_t field[64]) // returns the victory in a column
{
    reset_winner_discs();
    uint8_t ret = 0;
    for (int i = 0; i < 8; i++)
    {
        for (int j = 0; j < 8; j++)
        {
            if (field[i + (j * 8)] == OCCUPIED)
            {
                winner_discs_on_matrix[i + (j * 8)] = OCCUPIED;
                ret++;
            }
            else
            {
                reset_winner_discs();
                ret = 0;
            }
            if (ret == 4) // 4 in a column --> victory 
            {
                return ret;
            }
        }
        ret = 0; // no victory in this row 
    }
    return ret;
}


static uint8_t get_cross_victory(uint8_t field[64]) // returns the victory in a cross section
{
    uint8_t ret = 0; // return = 4 --> victory 
    int tmp; // distance to go in one diagonal

    // ------------------- right high to left low diagonales ------------------- 

    // 1. left upper triangle 

    tmp = 3;
    for (int a = 3; a <= 6; a++) // first row 
    {
        ret = 0;
        for (int b = 0; b <= tmp; b++) // steps to go down left
        {
            if (field[a + (7 * b)] == OCCUPIED)
            {
                winner_discs_on_matrix[a + (7 * b)] = OCCUPIED;
                ret++; // counts the stone 
            }
            else
            {
                reset_winner_discs();
                ret = 0; // no stone --> reset 
            }
            if (ret == 4)
            {
                return ret; // victory case 
            }
        }
        tmp++; // distance to go in one diagonal
    }

    // 2. right lower triangle 

    tmp = 7;
    for (int c = 56; c <= 60; c++) // last row
    {
        ret = 0;
        for (int d = 0; d <= tmp; d++) // steps to go down left 
        {
            if (field[c - (7 * d)] == OCCUPIED)
            {
                winner_discs_on_matrix[c - (7 * d)] = OCCUPIED;
                ret++; // counts the stone 
            }
            else
            {
                reset_winner_discs();
                ret = 0; // no stone --> reset 
            }
            if (ret == 4)
            {
                return ret; // victory case 
            }
        }
        tmp--; // distance to go in one diagonal
    }



    // --------------------- left high to right low ----------------------------

    // 1. upper triangle right 
    tmp = 7;
    for (int i = 0; i <= 4; i++) // first row
    {
        ret = 0;
        for (int j = 0; j <= tmp; j++) // steps to go down right 
        {
            if (field[i + (j * 9)] == OCCUPIED)
            {
                winner_discs_on_matrix[i + (j * 9)] = OCCUPIED;
                ret++; // counts the stone 
            }
            else
            {
                reset_winner_discs();
                ret = 0; // no stone --> reset 
            }
            if (ret == 4)
            {
                return ret; // victory case 
            }
        }
        tmp--; // distance to go in one diagonal
    }
    // lower triangle left 
    tmp = 6;
    for (int k = 8; k <= 32; k += 8) // first column
    {
        ret = 0;
        for (int l = 0; l <= tmp; l++)
        {
            if (field[k + (l * 9)] == OCCUPIED)
            {
                winner_discs_on_matrix[k + (l * 9)] = OCCUPIED;
                ret++; // counts one stone 
            }
            else
            {
                reset_winner_discs();
                ret--; // no stone --> reset 
            }
            if (ret == 4)
            {
                return ret; // victory case 
            }
        }
        tmp--; // distance to go in one diagonal
    }
    ret = 0; // no victory found --> return 0
    return ret;
}



