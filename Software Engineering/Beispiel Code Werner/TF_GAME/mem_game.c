
/* 
 * File:   mem_game.c
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 3. Juni 2018, 17:13
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>


#include "mem_game.h"
#include "mem_field.h"
#include "colors.h"



static void shuffle_mem_game(mem_field_t spiel[]);

static void print_game(mem_game_t *mem_game);


extern void mem_game_init(mem_game_t *mem_game)
{
    for (uint8_t i = 0; i < MEM_SIZE; i++)
    {
        mem_game->mem_field[i].x = i % MEM_X_SIZE;
        mem_game->mem_field[i].y = i / MEM_X_SIZE;
        mem_game->mem_field[i].index = i;
        mem_game->mem_field[i].color = i + 2; /* Farben ab Black+2, White+2, ... */
    }
    shuffle_mem_game(mem_game->mem_field);

    mem_game->positions_to_memorize = 1;
    mem_game->current_position = 1;

    print_game(mem_game);
}

static void swap(mem_field_t * p1, mem_field_t * p2);


static void shuffle_mem_game(mem_field_t spiel[])
{
    srand((unsigned) time(NULL));
    const int SHUFFLE = 100;
    /*
        for (int i = 0; i < SHUFFLE; i++)
        {
            int index1 = rand() % MEM_SIZE;
            int index2 = rand() % MEM_SIZE;
            swap(&spiel[index1], &spiel[index2]);
        }
     */
}


static void swap(mem_field_t * p1, mem_field_t * p2)
{
    uint8_t s;
    s = p1->x;
    p1->x = p2->x;
    p2->x = s;

    s = p1->y;
    p1->y = p2->y;
    p2->y = s;

    s = p1->index;
    p1->index = p2->index;
    p2->index = s;

    s = p1->color;
    p1->color = p2->color;
    p2->color = s;
}


static void print_game(mem_game_t *mem_game)
{
    printf("\nCurrent game: (%hhu field%s to remember)\n", mem_game->positions_to_memorize, (mem_game->positions_to_memorize == 1) ? "" : "s");
    for (int i = 0; i < MEM_SIZE; i++)
    {
        printf("(x=%hhu, y=%hhu, i=%hhu, c=%hhu) ", mem_game->mem_field[i].x, mem_game->mem_field[i].y, mem_game->mem_field[i].index, mem_game->mem_field[i].color);
    }
    printf("\n\n");
}


extern int mem_game_next_level(mem_game_t *mem_game)
{
    int do_next_level = 0;

    if (mem_game->positions_to_memorize < MEM_SIZE)
    {
        (mem_game->positions_to_memorize)++;
        mem_game->current_position = 1;
        do_next_level = 1;
    }

    return do_next_level;
}


extern void mem_game_draw_by_touch_field_index(mem_game_t *mem_game, uint8_t position)
{
    printf("\t%s(%hhu)\n", __func__, position);
    mem_field_draw(&mem_game->mem_field[position]);
}


extern void mem_game_clear_by_touch_field_index(mem_game_t *mem_game, uint8_t position)
{
    printf("\t%s(%hhu)\n", __func__, position);
    mem_field_clear(& (mem_game->mem_field[position]));
}




extern uint8_t mem_game_get_solution_field_index(mem_game_t *mem_game)
{
    int index = mem_game->mem_field[mem_game->current_position-1].index; 
    
    printf("%s(level %hhu) is %d\n", __func__, mem_game->current_position, index);
    
    return index;

}