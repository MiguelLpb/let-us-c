
/* 
 * File:   tf_games.c
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 1. Juni 2018, 17:05
 */
#include <stdio.h>
#include <stdlib.h>

#include "games.h"

#include "c4.h"
#include "mem.h"
#include "rgb_matrix.h"
#include "joysticks.h"
#include "touchpad.h"
#include "delay.h"

static tf_game_t games[] = {
    { "Connect Four", c4, c4_hw_is_ready, c4_show_info},
    { "MEM", mem, mem_hw_is_ready, mem_show_info},
    { "Dummy", 0, 0, 0},
};


extern int games_hw_ready()
{
    for (int i = 0; i < games_get_no_of_games(); i++)
    {
        if (games[i].hw_ready != 0)
        {
            if (!games[i].hw_ready())
            {
                return 0;
            }
        }
    }
    return 1;
}


extern void games_show_info()
{
    unsigned int no_of_games = games_get_no_of_games();
    printf("\n%u TF GAME%c  (%s)\n", no_of_games, (no_of_games > 1) ? 's' : ' ', __FILE__);
    for (int i = 0; i < no_of_games; i++)
    {
        printf("%3d. %-15s", i + 1, games[i].name);
        if (games[i].start == 0)
        {
            printf(" (defunct)");
        }
        printf("\n");
    }
    printf("\n");

    for (int i = 0; i < no_of_games; i++)
    {
        if (games[i].show_info != 0)
        {
            games[i].show_info();
        }
        else
        {
            printf("\nNo information for game #%d\n", i);
        }
    }
}


extern unsigned int games_get_no_of_games()
{
    return sizeof (games) / sizeof (tf_game_t);
}

const unsigned int GAME_END_DELAY = 500;


extern void games_play_game(tf_game_t * game)
{
    if (game != 0)
    {
        if (game->start != 0)
        {
            printf("\nNow playing %s ...\n\n", game->name);
            game->start();
            printf("\n... %s has ended.\n", game->name);
        }
        else
        {
            printf("\n\nOoops. Invalid game %s!", game->name);
        }
    }
    else
    {
        printf("\n\nOoops. Invalid game information!");
    }
    sleep_ms(GAME_END_DELAY);

}

static uint8_t array_of_displays [][RGB_MATRIX_SIZE] = {
    {
        0, 1, 0, 0, 0, 0, 0, 0,
        1, 1, 0, 0, 0, 0, 0, 0,
        0, 1, 0, 0, 0, 0, 0, 0,
        0, 1, 0, 0, 0, 1, 0, 1,
        1, 1, 1, 0, 0, 1, 0, 1,
        0, 0, 0, 0, 0, 1, 1, 1,
        0, 0, 0, 0, 0, 0, 0, 1,
        0, 0, 0, 0, 0, 0, 0, 1,
    },
    {
        1, 1, 1, 0, 0, 0, 0, 0,
        0, 0, 1, 0, 0, 0, 0, 0,
        1, 1, 1, 0, 0, 0, 0, 0,
        1, 0, 0, 0, 1, 0, 0, 1,
        1, 1, 1, 0, 1, 1, 1, 1,
        0, 0, 0, 0, 1, 0, 0, 1,
        0, 0, 0, 0, 1, 0, 0, 1,
        0, 0, 0, 0, 1, 0, 0, 1,
    },
    {
        1, 0, 1, 0, 1, 0, 1, 0,
        0, 1, 0, 1, 0, 1, 0, 1,
        1, 0, 1, 0, 1, 0, 1, 0,
        0, 1, 0, 1, 0, 1, 0, 1,
        1, 0, 1, 0, 1, 0, 1, 0,
        0, 1, 0, 1, 0, 1, 0, 1,
        1, 0, 1, 0, 1, 0, 1, 0,
        0, 1, 0, 1, 0, 1, 0, 1,
    }
};


static void display(int no_of_game)
{
    rgb_matrix_clear();
    rgb_matrix_draw_figure_by_color(array_of_displays[no_of_game], WHITE);
    printf("Selected: %s\n", games[no_of_game].name);
}


static void display_clear()
{
    rgb_matrix_clear();
}

const unsigned int GAME_SELECTION_DELAY = 50;

static void show_selection_info();


typedef enum selection_
{
    NONE, NEXT, SELECT
} selection_t;

static selection_t selection;

static selection_t get_selection();

extern tf_game_t * games_get_game()
{
    int no_of_games = games_get_no_of_games();
    show_selection_info();
    int selected_game = 0;
    display(selected_game);
    while (1)
    {
        selection_t selection = get_selection();
        if (selection == NEXT)
        {
            selected_game++;
            selected_game = selected_game % no_of_games;
            display(selected_game);
        }
        else if (selection == SELECT)
        {
            display_clear();
            return & games[selected_game];
        }
        sleep_ms(GAME_SELECTION_DELAY);
    }
    return & games[0];
}




static selection_t get_selection()
{
    selection_t result = NONE;

    if (joystick_has_been_released(LEFT))
    {
        result = NEXT;
    }
    else if (joystick_has_been_released(RIGHT))
    {
        result = SELECT;
    }
    else
    {
        orientation_t orientation;
        if (touchpad_touched_left_or_right(&orientation))
        {
            printf ("PRESSED! %d!\n", orientation);
            result = (orientation == LEFT) ? NEXT : SELECT;
        }
    }

    return result;
}


static void show_selection_info()
{
    printf("\nSELECT A GAME (%s)\n\n", __FILE__);
    printf("Required:\n"
            "\ttwo joystick bricklets, one at position A/C, one at position B/D\n"
            "\tone RGB LED Matrix bricklet\n");
    printf("Usage:\n\tpress and release LEFT joystick to CHANGE selected game\n"
            "\tpress and release RIGHT joystick to PLAY selected game\n"
            "\n");
}
