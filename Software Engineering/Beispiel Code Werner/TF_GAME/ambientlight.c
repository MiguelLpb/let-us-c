
/* 
 * File:   ambientlight.c
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 1. Mai 2018, 18:44
 */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "ambientlight.h"
#include "bricklet_ambient_light.h"


static const uint16_t INITIAL_BRIGHTNESS = 100;

static uint16_t light_level = INITIAL_BRIGHTNESS;
/* 0 .. 9000 */

static uint16_t ambient_level;


extern uint8_t ambientlight_adjusted_brightness(uint8_t brightness)
{
    if (brightness > 0)
    {
        float f = light_level;
        float c = brightness * f / 9000.f; 
        if (c > 255.f) 
            c = 255.f;
        if (c < 5.f) 
            c = 5.f;
        return (uint8_t) c; // Standard: 0x3F;
    }
    return 0x00;
}


void ambientlight_callback(uint16_t illuminance, void *user_data)
{
    light_level = illuminance;
}
