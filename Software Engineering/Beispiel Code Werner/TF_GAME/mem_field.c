
/* 
 * File:   mem_field.c
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 20. Juni 2018, 08:14
 */

#include <stdio.h>


#include "mem_game.h"
#include "mem_field.h"

#include "rgb_matrix.h"
#include "delay.h"

#define DOT_SIZE 2

static void field_index_to_rgb_coordinate(uint8_t position, uint8_t * rgb_x, uint8_t * rgb_y);


static void field_coordinate_to_rgb_coordinate(uint8_t x, uint8_t y, uint8_t * rgb_x, uint8_t * rgb_y)
{
    *rgb_x = x * DOT_SIZE + DOT_SIZE;
    *rgb_y = y * DOT_SIZE;
}


static void draw_2by2_(uint8_t rgb_x, uint8_t rgb_y, color_t color)
{
    rgb_matrix_set_dot(rgb_x, rgb_y, color);
    rgb_matrix_set_dot(rgb_x + 1, rgb_y, color);
    rgb_matrix_set_dot(rgb_x, rgb_y + 1, color);
    rgb_matrix_set_dot(rgb_x + 1, rgb_y + 1, color);

}


static void field_index_to_rgb_coordinate(uint8_t position, uint8_t * rgb_x, uint8_t * rgb_y)
{
    *rgb_x = (position / MEM_X_SIZE) * DOT_SIZE + DOT_SIZE;
    *rgb_y = (position % MEM_X_SIZE) * DOT_SIZE;
}


extern void mem_field_draw(mem_field_t * mem_field)
{
/*
    printf ("%s(index: %2hhu)\n", __func__, mem_field->index);
*/
    
    uint8_t rgb_x;
    uint8_t rgb_y;
    field_index_to_rgb_coordinate(mem_field->index, &rgb_x, &rgb_y);
    draw_2by2_(rgb_x, rgb_y, mem_field->color);
}

extern void mem_field_clear(mem_field_t * mem_field)
{
/*
    printf ("%s(index: %2hhu)\n", __func__, mem_field->index);
*/
    
    uint8_t rgb_x;
    uint8_t rgb_y;
    field_index_to_rgb_coordinate(mem_field->index, &rgb_x, &rgb_y);
    draw_2by2_(rgb_x, rgb_y, BLACK);
}



extern void mem_field_clear_all()
{
    rgb_matrix_clear();
/*
    for (int i = 0; i < MEM_SIZE; i++)
    {
        mem_field_draw_by_index(i, BLACK);
    }
*/
}

