
/* 
 * File:   c4_game.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 1. Juni 2018, 17:12
 */

#ifndef C4_GAME_H
#define C4_GAME_H

#include "player.h"
#include "colors.h"

#define COLOR_LEFT  GREEN
#define COLOR_RIGHT RED

extern color_t c4_player_to_color (player_t o);

extern player_t c4_color_to_player(color_t color);


#endif /* C4_GAME_H */

