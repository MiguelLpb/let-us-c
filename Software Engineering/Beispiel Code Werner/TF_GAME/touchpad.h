
/* 
 * File:   touchpad.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 3. Juni 2018, 15:26
 */

#ifndef TOUCHPAD_H
#define TOUCHPAD_H

#include "orientation.h"

/*
 * touched_index 0 .. 11
 * 
 */
extern int touchpad_touched(uint8_t * index);

extern int touchpad_touched_left_or_right(orientation_t *orientation);

extern int touchpad_four_corners_pressed();

extern void touchpad_reset ();

extern void cb_touched(uint16_t state, void *user_data);
#endif /* TOUCHPAD_H */

