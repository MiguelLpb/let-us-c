
/* 
 * File:   mem.c
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 1. Juni 2018, 20:51
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include "mem.h"
#include "mem_game.h"
#include "mem_field.h"
#include "mem_status.h"

#include "touchpad.h"
#include "delay.h"
#include "beep.h"
#include "rgb_matrix.h"
#include "player.h"


extern int mem_hw_is_ready()
{
    return 1;
}


extern void mem_show_info()
{
    printf("\nA Simple MEMORY game (%s)\n\n", __FILE__);
    printf("Required:\n"
            "\tone 4x3 touchpad\n"
            "\tone RGB LED Matrix bricklet\n");
    printf("Optional:\n"
            "\tone piezo bricklet for sound effects\n"
            "\tone ambient light bricklet for light intensity control\n");
    printf("Usage:\n\tjust touch to proceed and then memorize and touch the fields in the correct order!\n"
            "\ttouch upper left and lower right simultaneously to end the game\n"
            "\n");
}

typedef enum _mem_state
{
    HW_FAIL, STARTING, TIMEOUT, LOST, WON, SHOWING, CHOOSING, QUIT
} mem_state_t;

static const int TIME_TO_NEXT_STATE_UPDATE = 25;
static const int CHOOSE_TIMEOUT = 5000;

static mem_state_t check_quit_or_hwfail(mem_state_t);
static mem_state_t do_hardware_check();
static mem_state_t do_starting(mem_game_t * mem_game);
static mem_state_t do_showing(mem_game_t * mem_game);
static mem_state_t do_choosing(mem_game_t * mem_game);
static mem_state_t do_loosing();
static mem_state_t do_winning(mem_game_t * mem_game);
static mem_state_t do_timeout(mem_game_t * mem_game);
static mem_state_t do_quit(mem_game_t * mem_game);
static mem_state_t do_error(mem_game_t * mem_game);


extern void mem()
{

    mem_game_t mem_game; /* THE GAME */
    mem_state_t state = STARTING;

    mem_show_info();
    while (state != QUIT)
    {
        state = check_quit_or_hwfail(state);
        switch (state)
        {
            case HW_FAIL:
                state = do_hardware_check();
                break;
            case STARTING:
                state = do_starting(&mem_game);
                break;
            case SHOWING:
                state = do_showing(&mem_game);
                break;
            case CHOOSING:
                state = do_choosing(&mem_game);
                break;
            case LOST:
                state = do_loosing(&mem_game);
                break;
            case WON:
                state = do_winning(&mem_game);
                break;
            case TIMEOUT:
                state = do_timeout(&mem_game);
                break;
            case QUIT:
                state = do_quit(&mem_game);
                break;
            default:
                state = do_error(&mem_game);
        }
        sleep_ms(TIME_TO_NEXT_STATE_UPDATE);
    }
}


static mem_state_t check_quit_or_hwfail(mem_state_t state)
{
    mem_state_t next_state = state;

    if (!mem_hw_is_ready())
    {
        next_state = HW_FAIL;
    }
    else if (player_want_to_quit(MULTITOUCH))
    {
        next_state = QUIT;
    }

    return next_state;
}


static mem_state_t do_hardware_check()
{
    mem_state_t next_state = HW_FAIL;

    printf("HW_NOT_READY\n");
    if (mem_hw_is_ready())
    {
        next_state = STARTING;
    }

    return next_state;
}


static mem_state_t do_starting(mem_game_t * mem_game)
{
    mem_state_t next_state = STARTING;

    static int report_starting = 1;
    if (report_starting)
    {
        report_starting = 0;
        printf("\nSTARTING.");
        mem_game_init(mem_game);
        mem_status_clear_starting();
        printf("Waiting for touch ... \n");
    }
    mem_status_draw_starting();

    uint8_t touched_index;
    if (touchpad_touched(&touched_index))
    {
        next_state = SHOWING;
        mem_status_clear_starting();
        report_starting = 1;
    }

    return next_state;
}

static void show_fields(mem_game_t * mem_game);


static mem_state_t do_showing(mem_game_t * mem_game)
{
    mem_state_t next_state = CHOOSING;

    printf("\nSHOWING %hhu position%s\n", mem_game->positions_to_memorize, (mem_game->positions_to_memorize > 1) ? "s" : "");
    show_fields(mem_game);
    gettimeofday(& (mem_game->last_action_end_time), 0);
    touchpad_reset();

    return next_state;
}

static void wait_after_show(int i);


static void show_fields(mem_game_t * mem_game)
{
    uint8_t index_of_last_field = mem_game->positions_to_memorize - 1;

    for (uint8_t i = 0; i <= index_of_last_field; i++)
    {
        mem_game_draw_by_touch_field_index(mem_game, i);
        wait_after_show(i);
        mem_game_clear_by_touch_field_index(mem_game, i);
    }
}


static void wait_after_show(int i)
{
    sleep_ms(1000);
}


static mem_state_t handle_choice(mem_game_t * mem_game, uint8_t chosen_index);

static mem_state_t handle_no_choice(mem_game_t * mem_game);


static mem_state_t do_choosing(mem_game_t * mem_game)
{
    mem_state_t next_state;

    static int report_choosing = 1;
    if (report_choosing)
    {
        report_choosing = 0;
        printf("\nCHOOSING  %hhu of %hhu. Waiting for touch ...  \n", mem_game->current_position, mem_game->positions_to_memorize);
        mem_status_clear_wait();
    }
    mem_status_draw_wait();

    uint8_t touched_field;
    int touched = touchpad_touched(&touched_field);
    if (touched && (touched_field < MEM_SIZE))
    {
        next_state = handle_choice(mem_game, touched_field);
        report_choosing = 1;

    }
    else
    {
        next_state = handle_no_choice(mem_game);
    }
    if (next_state == TIMEOUT)
    {
        report_choosing = 1;
        mem_field_clear_all();
        sleep_ms(100);
    }

    return next_state;
}


static mem_state_t handle_wrong_choice(mem_game_t * mem_game, uint8_t touched_position);

static mem_state_t handle_correct_choice(mem_game_t * mem_game);


static mem_state_t handle_choice(mem_game_t * mem_game, uint8_t touched_field_index)
{
    mem_state_t next_state;

    uint8_t correct_touch_field_index = mem_game_get_solution_field_index(mem_game);

    if (touched_field_index != correct_touch_field_index)
    {
        next_state = handle_wrong_choice(mem_game, touched_field_index);
    }
    else
    {
        next_state = handle_correct_choice(mem_game);
    }

    return next_state;
}


static mem_state_t handle_wrong_choice(mem_game_t * mem_game, uint8_t touched_field_index)
{
    mem_state_t next_state = LOST;

    uint8_t correct_touch_field_index;

    printf("Wrong field %hhu, should be %hhu.\n", touched_field_index, correct_touch_field_index);
    mem_game_draw_by_touch_field_index(mem_game, correct_touch_field_index);
    beep_f(400);
    sleep_ms(1000);
    mem_game_clear_by_touch_field_index(mem_game, correct_touch_field_index);

    mem_status_clear_wait_and_reset();

    return next_state;
}


static mem_state_t handle_correct_choice(mem_game_t * mem_game)
{
    mem_state_t next_state;

    uint8_t index = mem_game->current_position - 1;
    uint8_t correct_position = mem_game->mem_field[index].index;

    mem_game_draw_by_touch_field_index(mem_game, correct_position);
    sleep_ms(50);
    mem_game_clear_by_touch_field_index(mem_game, correct_position);

    if (mem_game->current_position == mem_game->positions_to_memorize)
    {
        mem_status_clear_wait_and_reset();
        beep_f(800);
        next_state = WON;
    }
    else
    {
        (mem_game->current_position)++;
        mem_status_clear_wait();
        next_state = CHOOSING;
    }

    return next_state;
}


static mem_state_t handle_no_choice(mem_game_t * mem_game)
{
    mem_state_t next_state = CHOOSING;

    float elapsed;
    struct timeval now;

    gettimeofday(&now, 0);
    elapsed = timedifference_ms(mem_game->last_action_end_time, now);
    if (elapsed > CHOOSE_TIMEOUT)
    {
        mem_status_clear_wait_and_reset();
        next_state = TIMEOUT;
    }

    return next_state;
}


static mem_state_t do_loosing()
{
    mem_state_t next_state = STARTING;

    printf("LOST.\n");
    mem_status_show_lost();
    mem_status_clear();

    return next_state;
}


static mem_state_t do_winning(mem_game_t * mem_game)
{
    mem_state_t next_state;

    mem_status_show_won();
    mem_status_clear();
    printf("WON - ");
    if (mem_game_next_level(mem_game))
    {
        printf("Next level!\n\n");
        next_state = SHOWING;
    }
    else
    {
        printf("*** LAST LEVEL WON *** \nStart over.\n");
        next_state = STARTING;
    }

    return next_state;
}


static mem_state_t do_timeout(mem_game_t * mem_game)
{
    mem_state_t next_state = SHOWING;

    printf("TIMEOUT.\n");
    mem_status_show_timeout();
    mem_status_clear();
    //mem_game->positions_to_memorize = 1;

    return next_state;
}


static mem_state_t do_quit(mem_game_t * mem_game)
{
    mem_state_t next_state = QUIT;

    printf("QUIT\n");
    rgb_matrix_clear();

    return next_state;
}


static mem_state_t do_error(mem_game_t * mem_game)
{
    mem_state_t next_state;

    printf("Illegal state of MEM.\n");
    next_state = do_quit(mem_game);

    return next_state;
}