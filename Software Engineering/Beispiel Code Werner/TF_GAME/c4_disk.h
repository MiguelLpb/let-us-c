
/* 
 * File:   c4_disc.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on April 8, 2018, 10:38 AM
 */

#ifndef C4_DISK_H
#define C4_DISK_H

#include <stdlib.h>

#include "orientation.h"

#include "player.h"


extern void disk_drop_toggle(uint8_t y, player_t player);


extern void disc_win_toggle(uint8_t row, uint8_t column, player_t player);


extern void disc_win_show(  uint8_t disc_column, uint8_t  disc_row, player_t p);


extern void disk_display_in_field(uint8_t x, uint8_t y, player_t player);


extern void disk_to_drop_display(uint8_t column, player_t player);

extern uint8_t disk_to_drop_move(uint8_t disc_column, player_t player, orientation_t left_or_right);

extern void disk_to_drop_remove(uint8_t column);


extern void disk_clear_in_field(uint8_t x, uint8_t y);


extern uint8_t disk_drop (uint8_t disc_column, player_t player);


extern int disk_timed_out();


extern void disk_timeout_reset();

#endif /* C4_DISK_H */

