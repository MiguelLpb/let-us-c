
/* 
 * File:   touchpad.c
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 3. Juni 2018, 15:26
 */

#include <stdlib.h>
#include <stdio.h>

#include "touchpad.h"

#define CONNECTED_FIELDS 12

static uint16_t last_touch = 0;
static int touched = 0;

static int translation[] = {11, 7, 3, 10, 6, 2, 9, 5, 1, 8, 4, 0};
/* TF-PA
 * 0 11 
 * 1  7
 * 2  3
 * 3 10
 * 4  6
 * 5  2
 * 6  9
 * 7  5
 * 8  1
 * 9  8
 * 10 4
 * 11 0
 * 
 * Corners 0 2 9 11 
 */


static int corners_have_been_pressed = 0;


extern int touchpad_four_corners_pressed()
{
    return corners_have_been_pressed;
}



static int get_first_set_bit(uint16_t state);


extern int touchpad_touched(uint8_t * index)
{
    int result = 0;

    if (touched)
    {
        *index = last_touch;
        touched = 0;
        result = 1;
    }
    return result;
}


extern int touchpad_touched_left_or_right(orientation_t * orientation)
{
    uint8_t index;
    int result;

    if (touchpad_touched(&index))
    {
        switch (index)
        {
            case 0:
            case 4:
            case 8:
                *orientation = LEFT;
                result = 1;
                break;
            case 3:
            case 7:
            case 11:
                *orientation = RIGHT;
                result = 1;
                break;
            default:
                result = 0;
        }
    }
    return result;
}

static int translate_touch_index(int touch_index);


static int get_first_set_bit(uint16_t state)
{
    for (int i = 0; i < CONNECTED_FIELDS; i++)
    {
        if (state & (1 << i))
        {
            return translate_touch_index(i);
        }
    }
    return 0;
}


static int translate_touch_index(int touch_index)
{
    return translation[touch_index];
}


extern void cb_touched(uint16_t state, void *user_data)
{
    if ((state & 0xfff) != 0) /* pressed */
    {
        touched = 1;
        corners_have_been_pressed = (state == 0b101000000101);
        if (!corners_have_been_pressed)
        {
            unsigned int i;
            for (i = 0; i < 12; i++)
            {
                if (state & (1 << i))
                {
                    break;
                }
            }
            last_touch = get_first_set_bit(state);
        }
        else 
        {
            touched = 0;
        }
    }
}

extern void touchpad_reset()
{
    touched = 0;
}