
/* 
 * File:   c4_player.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on April 15, 2018, 8:43 AM
 */

#ifndef PLAYER_H
#define PLAYER_H

#include "orientation.h"

typedef enum player_input_ {JOYSTICK, MULTITOUCH} player_input_t;

typedef orientation_t player_t;

extern void player_set_player(player_t *first);

extern unsigned int player_has_released(player_t p);

extern unsigned int player_one_player_has_released();

extern int player_want_to_quit(player_input_t);

extern void player_ignore_activity();

player_t player_toggle_current(player_t current_player);

orientation_t player_column_change(player_t current_player);

#endif /* PLAYER_H */

