
/* 
 * File:   c4_tie.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on April 9, 2018, 3:16 PM
 */

#ifndef C4_TIE_H
#define C4_TIE_H

extern int tie_is_tie();

extern void tie_toggle();

#endif /* C4_TIE_H */

