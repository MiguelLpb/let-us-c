#include <stdio.h>
#include <sys/time.h>
#include "tf.h"

#include "c4.h"

#include "c4_field.h"
#include "c4.h"
#include "c4_win.h"
#include "c4_game.h"
#include "c4_disk.h"
#include "c4_tie.h"
#include "c4_win.h"
#include "c4_timing.h"
#include "c4_frame.h"
#include "c4_game.h"

#include "orientation.h"
#include "arrow.h"
#include "delay.h"
#include "rgb_matrix.h"

typedef enum _c4_state_t
{
    NO_STATE, INIT, BEGIN, MOVE, DROPPING, DROPPED, TIE, WON, HW_FAIL, QUIT
} c4_state_t;

static c4_state_t process_init(player_t * first_player, c4_state_t last_state);

static c4_state_t process_begin(player_t current_player, c4_state_t last_state);

static c4_state_t process_dropping(
        uint8_t disc_column,
        uint8_t * disc_row,
        player_t current_player,
        c4_state_t last_state);

static c4_state_t process_dropped(player_t * new_current, uint8_t disc_row, uint8_t disc_column);

static c4_state_t process_move(
        player_t current,
        uint8_t *disc_column,
        c4_state_t last_state);

static c4_state_t process_tie();

static c4_state_t process_won(player_t * winner, uint8_t disc_row, uint8_t disc_column, c4_state_t last_state);

static c4_state_t check_quit_or_hwfail(c4_state_t);


extern void c4()
{
    player_t current_player = NO_ORIENTATION;

    uint8_t disk_column;
    uint8_t disk_row;

    c4_state_t c4_state = INIT;
    c4_state_t c4_last_state;
    
    c4_state = INIT;
    c4_last_state = NO_STATE;
    while (c4_state != QUIT)
    {
        c4_state = check_quit_or_hwfail(c4_state);
        switch (c4_state)
        {
            case INIT:
                c4_state = process_init(&current_player, c4_last_state);
                break;
            case BEGIN:
                c4_state = process_begin(current_player, c4_last_state);
                break;
            case MOVE:
                c4_state = process_move(current_player, &disk_column, c4_last_state);
                c4_last_state = c4_state;
                break;
            case DROPPING:
                c4_state = process_dropping(disk_column, &disk_row, current_player, c4_last_state);
                break;
            case DROPPED:
                c4_state = process_dropped(&current_player, disk_row, disk_column);
                break;
            case TIE:
                c4_state = process_tie();
                break;
            case WON:
                c4_state = process_won(&current_player, disk_row, disk_column, c4_last_state);
                break;
            case HW_FAIL:
                printf("state HWFAIL\n");
                c4_state = QUIT;
                break;
            case QUIT:
            case NO_STATE:
                rgb_matrix_clear();
                printf("state QUIT\n");
                break;
            default:
                printf("Illegal state of c4.\n");
                c4_state = QUIT;
        }
        sleep_ms(C4_STATE_UPDATE_DELAY);
    }
}


static c4_state_t check_quit_or_hwfail(c4_state_t state)
{
    c4_state_t next_state = state;
    if (player_want_to_quit(JOYSTICK))
    {
        next_state = QUIT;
    }
    else if (!c4_hw_is_ready())
    {
        next_state = HW_FAIL;
    }
    return next_state;
}


static c4_state_t process_init(player_t *current, c4_state_t last_state)
{
    c4_state_t next_state = BEGIN;
    if (last_state != INIT)
    {
        printf("state INIT\n");
    }
    rgb_matrix_clear();
    player_set_player(current);
    printf("first player is %s\n", *current == LEFT ? "LEFT" : "RIGHT");
    return next_state;
}


static c4_state_t process_begin(
        player_t player,
        c4_state_t last_state)
{
    static int report_state = 1;
    c4_state_t next_state = BEGIN;
    if (report_state)
    {
        report_state = 0;
        printf("state BEGIN\n");
    }
    arrow_pulse(player, c4_player_to_color(player));
    if (player_has_released(player))
    {
        field_empty();
        rgb_matrix_clear();
        frame_draw();
        next_state = MOVE;
    }
    return next_state;
}


static c4_state_t process_move(
        player_t current,
        uint8_t *disk_column,
        c4_state_t last_state
        )
{
    static int report_state = 1;

    if (report_state)
    {
        report_state = 0;
        printf("state MOVE of %s \n", c4_player_to_color(current) == GREEN ? "GREEN" : "RED");
    }

    static player_t last_player = NO_ORIENTATION;
    c4_state_t next_state = MOVE;

    if (last_player != current)
    {
        *disk_column = field_get_free_top_column();
        disk_timeout_reset();
        printf("%s disk on top of column %hhu\n", c4_player_to_color(current) == GREEN ? "GREEN" : "RED", *disk_column);
        disk_to_drop_display(*disk_column, current);
        last_player = current;
    }
    disk_drop_toggle(*disk_column, current);
    if (player_has_released(current) || disk_timed_out())
    {
        next_state = DROPPING;
        disk_timeout_reset();
    }
    orientation_t left_or_right = player_column_change(current);
    switch (left_or_right)
    {
        case LEFT:
        case RIGHT:
            disk_to_drop_remove(*disk_column);
            *disk_column = disk_to_drop_move(*disk_column, current, left_or_right);
            disk_to_drop_display(*disk_column, current);
            break;
        default:
            break;
    }
    player_ignore_activity();
    return next_state;
}


static c4_state_t process_dropping(uint8_t disk_column, uint8_t * disk_row, player_t current_player, c4_state_t last_state)
{
    c4_state_t next_state = DROPPED;
    printf("state DROPPING\n");
    disk_to_drop_remove(disk_column);
    *disk_row = disk_drop(disk_column, current_player);
    player_ignore_activity();
    return next_state;
}


static c4_state_t process_dropped(player_t * new_current, uint8_t disk_row, uint8_t disk_column)
{
    c4_state_t next_state;
    player_t current = *new_current;
    printf("state DROPPED\n");
    if (win_is_won(current))
    {
        disk_display_in_field(disk_row, disk_column, current);
        printf("winner is %s.\n", (current == LEFT) ? "GREEN" : "RED");

        next_state = WON;
    }
    else if (tie_is_tie())
    {
        *new_current = player_toggle_current(current);
        next_state = TIE;
    }
    else
    {
        *new_current = player_toggle_current(current);
        next_state = MOVE;
    }
    return next_state;
}


static c4_state_t process_tie(player_t first, player_t second)
{
    static int report = 1;
    c4_state_t next_state = TIE;
    if (report)
    {
        report = 0;
        printf("state TIE. Game Over. \n\n");
    }
    tie_toggle();
    if (player_one_player_has_released())
    {
        field_empty();
        rgb_matrix_clear();
        next_state = INIT;
    }
    return next_state;
}


static c4_state_t process_won(player_t * current_player, uint8_t disc_row, uint8_t disc_column, c4_state_t last_state)
{
    static int report = 1;
    c4_state_t next_state = WON;
    if (report)
    {
        report = 0;
        printf("state WON\n");
    }
    disc_win_toggle(disc_row, disc_column, *current_player);
    if (player_one_player_has_released())
    {
        field_empty();
        rgb_matrix_clear();
        *current_player = (*current_player == LEFT) ? RIGHT : LEFT;
        next_state = BEGIN;
    }
    return next_state;
}



extern int c4_hw_is_ready()
{
    return (rgb_matrix_is_valid && left_is_valid && right_is_valid);
}


extern void c4_show_info()
{
    printf("\nCONNECT FOUR (%s)\n\n", __FILE__);
    printf("Required:\n"
            "\ttwo joystick bricklets, one at position A/C, one at position B/D\n"
            "\tone RGB LED Matrix bricklet\n");
    printf("Optional:\n"
            "\tone piezo bricklet for sound effects\n"
            "\tone ambient light bricklet for light intensity control\n");
    printf("Usage:\n\tpress and release joystick to trigger effects\n"
            "\tmove joystick left/right to control position of disk\n"
            "\tlong press to exit game\n"
            "\n");
}

