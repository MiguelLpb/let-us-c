
/* 
 * File:   c4_disc.c
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on April 8, 2018, 10:40 AM
 */
#include <stdio.h>
#include <sys/time.h>

#include "c4_disk.h"
#include "c4_game.h"
#include "c4_field.h"
#include "c4_timing.h"
#include "c4_game.h"

#include "orientation.h"
#include "delay.h"
#include "rgb_matrix.h"
#include "joysticks.h"
#include "beep.h"


static struct timeval last_toggle_time;
static struct timeval start_time;

static unsigned int started_yet = 0;
static int disc_is_visible = 0;

#define PERMANENT 0
#define BLINKING  1
#define TIMED_OUT 2


static int time_out_status()
{
    if (!started_yet)
    {
        started_yet = 1;
        gettimeofday(&start_time, 0);
    }
    struct timeval now;
    gettimeofday(&now, 0);
    double dif = timedifference_ms(start_time, now);

    if (dif >= PLAYER_TIMEOUT)
    {
        return TIMED_OUT;
    }
    else if (dif >= PLAYER_WARN_DELAY)
    {
        return BLINKING;
    }
    else
    {
        return PERMANENT;
    }
}


extern void disk_drop_toggle(uint8_t column, player_t player)
{
    color_t color = c4_player_to_color(player);
    struct timeval now;
    switch (time_out_status())
    {
        case PERMANENT:
            rgb_matrix_set_dot(0, column, color);
            break;
        case BLINKING:
            gettimeofday(&now, 0);
            double dif = timedifference_ms(last_toggle_time, now);
            if (dif >= WARN_BLINK_FREQUENCY)
            {
                last_toggle_time = now;
                if (disc_is_visible)
                {
                    disc_is_visible = 0;
                    rgb_matrix_set_dot(0, column, BLACK);
                }
                else
                {
                    rgb_matrix_set_dot(0, column, color);
                    disc_is_visible = 1;
                }
            }
            break;
        case TIMED_OUT:
            break;
        default:
            break;
    }
}


extern int disk_timed_out()
{
    return (time_out_status() == TIMED_OUT);
}


extern void disk_timeout_reset()
{
    started_yet = 0;
}


extern uint8_t disk_drop(uint8_t column, player_t player)
{
    color_t color = c4_player_to_color(player);
    int delay = DROP_DISC_DELAY;
    printf("Dropping %s disc in column %hhu ... ", color == GREEN ? "GREEN" : "RED", column);

    beep();
    rgb_matrix_set_dot(1, column, color);
    sleep_ms(delay);
    uint8_t row = 0;
    while (row < C4_FIELD_HEIGHT)
    {
        if (joystick_has_been_released(player))
        {
            delay = DROP_DISC_DELAY / 5;
        }
        if (field_below_is_free(row, column))
        {
            rgb_matrix_set_dot(row + 1, column, BLACK);
            row++;
            rgb_matrix_set_dot(row + 1, column, color);
            beep();
            sleep_ms(delay);
        }
        else
        {
            field_put(row, column, color);
            printf("to row %hhu\n", row);
            break;
        }
    }
    return row;
}


static void disk_display_on_matrix(uint8_t row, int8_t column, color_t color)
{
    disc_is_visible = (color != BLACK);
    rgb_matrix_set_dot(row, column, color);
}


extern void disk_to_drop_display(uint8_t column, player_t player)
{

    disk_display_on_matrix(0, column, c4_player_to_color(player));
}


extern void disk_to_drop_remove(uint8_t column)
{

    disk_display_on_matrix(0, column, BLACK);
}


extern void disk_display_in_field(uint8_t row, uint8_t column, player_t player)
{

    rgb_matrix_set_dot(row + 1, column, c4_player_to_color(player));
}


extern void disk_clear_in_field(uint8_t row, uint8_t column)
{
    disk_display_on_matrix(row + 1, column, BLACK);
}





const unsigned int TOGGLE_DISK_TIME = 100;


extern void disc_win_toggle(uint8_t row, uint8_t column, player_t player)
{
    static int winner_is_visible = 0;

    struct timeval now;
    gettimeofday(&now, 0);
    double dif = timedifference_ms(last_toggle_time, now);
    if (dif >= TOGGLE_WINNER_FREQUENCY)
    {
        last_toggle_time = now;
        if (winner_is_visible)
        {   beep_f(261);
            disk_clear_in_field(row, column);
            winner_is_visible = 0;
        }
        else
        {
            beep_f(440);
            disk_display_in_field(row, column, player);
            winner_is_visible = 1;
        }
    }
}


static int column_is_valid(uint8_t column)
{
    return (column >= 0) && (column < (C4_FIELD_WIDTH));
}


static int column_is_usable(uint8_t column)
{
    return (field_get(0, column) == BLACK);
}


static uint8_t next_column(uint8_t column, orientation_t left_or_right)
{
    return (left_or_right == LEFT) ? (column - 1) : (column + 1);
}


static uint8_t find_first(uint8_t column, orientation_t left_or_right)
{
    uint8_t start_column = column;
    while (column_is_valid(column = next_column(column, left_or_right)))
    {
        return column;
    }
    return start_column;
}


extern uint8_t disk_to_drop_move(uint8_t column, player_t player, orientation_t left_or_right)
{

    uint8_t new_column = find_first(column, left_or_right);
    printf("Player %s moves disk to drop from %hhu to the %s to %hhu.\n",
            (player == LEFT) ? "LEFT" : "RIGHT",
            column,
            (left_or_right == LEFT) ? "LEFT" : "RIGHT",
            new_column
            );
    return new_column;
}
