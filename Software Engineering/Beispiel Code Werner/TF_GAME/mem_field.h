
/* 
 * File:   mem_field.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 20. Juni 2018, 08:10
 */

#ifndef MEM_FIELD_H
#define MEM_FIELD_H

#include <stdlib.h>

#include "colors.h"



extern void mem_field_draw(mem_field_t * mem_field);
extern void mem_field_clear (mem_field_t * mem_field);
extern void mem_field_clear_all();

extern void mem_field_print (mem_field_t * mem_field);






#endif /* MEM_FIELD_H */

