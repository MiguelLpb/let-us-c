
/* 
 * File:   beep.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 1. Mai 2018, 18:29
 */

#ifndef BEEP_H
#define BEEP_H

#ifdef __cplusplus
extern "C"
{
#endif


#include <stdlib.h>
#include <stdint.h>
    extern void beep();
    
    void beep_f(uint16_t frequency);

#ifdef __cplusplus
}
#endif

#endif /* BEEP_H */

