
/* 
 * File:   orientation.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 7. April 2018, 08:56
 */

#ifndef ORIENTATION_H
#define ORIENTATION_H


typedef enum _orientation {LEFT, RIGHT, UP, DOWN , NO_ORIENTATION} orientation_t;

#endif /* ORIENTATION_H */

