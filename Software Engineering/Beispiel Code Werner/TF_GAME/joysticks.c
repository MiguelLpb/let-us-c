#include <stdio.h>
#include <time.h>
#include <sys/time.h>

#include "bricklet_joystick.h"
#include "joysticks.h"
#include "orientation.h"
#include "delay.h"



static const double LONG_PRESS_TIME_MS = 3000;

static struct timeval time_left_has_pressed;
static uint8_t left_is_pressing = 0;

static void handle_pressed(uint8_t *, struct timeval *);


void cb_pressed_left(void *user_data)
{
    (void) user_data;
    handle_pressed(&left_is_pressing, &time_left_has_pressed);
}

static struct timeval time_right_has_pressed;
static uint8_t right_is_pressing = 0;


void cb_pressed_right(void *user_data)
{
    (void) user_data;
    handle_pressed(&right_is_pressing, &time_right_has_pressed);
}


static void handle_pressed(uint8_t *is_pressing, struct timeval * now)
{
    *is_pressing = 1;
    gettimeofday(now, 0);
}

static uint8_t left_released = 0;


void cb_released_left(void *user_data)
{
    (void) user_data;
    struct timeval now;
    gettimeofday(&now, 0);
    if (left_is_pressing)
    {
        left_is_pressing = 0;
        left_released = 1;
    }
}

static uint8_t right_released = 0;


void cb_released_right(void *user_data)
{
    (void) user_data;
    struct timeval now;
    gettimeofday(&now, 0);
    if (right_is_pressing)
    {
        right_is_pressing = 0;
        right_released = 1;
    }
}


static int interpret_joystick_move(
        int wants_move,
        int16_t x,
        orientation_t * move_to)
{
    if (!wants_move)
    {
        wants_move = 1;
        if (x < 0)
        {
            *move_to = LEFT;
        }
        else if (x > 0)
        {
            *move_to = RIGHT;
        }
        else
        {
            * move_to = NO_ORIENTATION;
        }
    }
    return wants_move;
}

static orientation_t move_of_left = NO_ORIENTATION;
static orientation_t move_of_right = NO_ORIENTATION;

static int joystick_left_wants_move = 0;
static int joystick_right_wants_move = 0;


void cb_position_reached_left(int16_t x, int16_t y, void *user_data)
{
    joystick_left_wants_move = interpret_joystick_move(joystick_left_wants_move, x, &move_of_left);
}


void cb_position_reached_right(int16_t x, int16_t y, void *user_data)
{
    joystick_right_wants_move = interpret_joystick_move(joystick_right_wants_move, x, &move_of_right);
}


extern void joystick_reset()
{
    left_released = 0;
    right_released = 0;
    joystick_left_wants_move = 0;
    joystick_right_wants_move = 0;
}


int joystick_has_been_released(orientation_t o)
{
    int result;
    if (o == LEFT)
    {
        result = left_released;
        left_released = 0;
    }
    else if (o == RIGHT)
    {
        result = right_released;
        right_released = 0;
    }
    return result;
}


static int check_one_long_pressing(int is_pressing, struct timeval time_when_pressed, struct timeval now)
{
    int long_pressed = 0;
    if (is_pressing)
    {
        double pressing_time = timedifference_ms(time_when_pressed, now);
        if (pressing_time >= LONG_PRESS_TIME_MS)
        {
            long_pressed = 1;
        }
    }
    return long_pressed;
}


extern int joysticks_one_is_long_pressed()
{

    struct timeval now;
    gettimeofday(&now, 0);
    int long_pressed = check_one_long_pressing(right_is_pressing, time_right_has_pressed, now) ||
            check_one_long_pressing(left_is_pressing, time_left_has_pressed, now);
    if (long_pressed)
    {
        gettimeofday(&time_left_has_pressed, 0);
        right_is_pressing = 0;
        gettimeofday(&time_right_has_pressed, 0);
        left_is_pressing = 0;
    }
    return long_pressed;
}


extern orientation_t joystick_wants_move(orientation_t orientation)
{
    orientation_t wanted = NO_ORIENTATION;
    if (orientation == LEFT)
    {
        if (joystick_left_wants_move)
        {
            if (move_of_left != NO_ORIENTATION)
            {
                wanted = move_of_left;
                move_of_left = NO_ORIENTATION;
                joystick_left_wants_move = 0;
            }
        }
    }
    else if (orientation == RIGHT)
    {
        if (move_of_right != NO_ORIENTATION)
        {
            wanted = move_of_right;
            move_of_right = NO_ORIENTATION;
            joystick_left_wants_move = 0;
        }
    }

    return wanted;
}
