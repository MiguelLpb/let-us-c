
/* 
 * File:   tf.c
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on 5. April 2018, 10:31
 */


#include <stdio.h>

#include "ip_connection.h"
#include "bricklet_joystick.h"
#include "bricklet_rgb_led_matrix.h"
#include "bricklet_ambient_light.h"
#include "bricklet_piezo_speaker.h"
#include "bricklet_multi_touch.h"
#include "brick_master.h"

#include "joysticks.h"
#include "ambientlight.h"
#include "rgb_matrix.h"
#include "touchpad.h"

#include "games.h"

#include "tf.h"
/*
 * TF specific defines 
 */
#define HOST "localhost"
#define PORT 4223

static IPConnection ipcon;


/* Print incoming enumeration information */
static void cb_enumerate(const char *uid, const char *connected_uid,
        char position, uint8_t hardware_version[3],
        uint8_t firmware_version[3], uint16_t device_identifier,
        uint8_t enumeration_type, void *user_data);

static void cb_connect(uint8_t connect_reason, void *user_data);
static void cb_disconnect(uint8_t connect_reason, void *user_data);


void on_bricklet_state_changed(uint8_t button_l, uint8_t button_r, uint8_t led_l, uint8_t led_r,
        void *user_data);


RGBLEDMatrix rgb_led_matrix;
int rgb_matrix_is_valid;
static char rgb_matrix_uid[8];

Joystick left;
int left_is_valid;
static char left_uid[8];

Joystick right;
int right_is_valid;
static char right_uid[8];

AmbientLight ambient_light;
int ambient_light_is_valid;
static char ambient_light_uid[8];

PiezoSpeaker piezo_speaker;
int piezo_speaker_is_valid;
static char piezo_speaker_uid[8];

MultiTouch multi_touch;
int multi_touch_is_valid;
static char multi_touch_uid[8];


extern void tf_init()
{
    rgb_matrix_is_valid = 0;
    left_is_valid = 0;
    left_is_valid = 0;
    ambient_light_is_valid = 0;
    piezo_speaker_is_valid = 0;
    multi_touch_is_valid = 0;

    /* Create IP connection */
    ipcon_create(&ipcon);

    /* Connect to brickd */
    if (ipcon_connect(&ipcon, HOST, PORT) < 0)
    {
        fprintf(stderr, "Error: Could not connect to brickd on %s. Exit.\n", HOST);
        exit(EXIT_FAILURE);
    }
    /* Register enumeration callback to "cb_enumerate" */
    ipcon_register_callback(&ipcon,
            IPCON_CALLBACK_ENUMERATE,
            (void *) cb_enumerate,
            NULL);
    ipcon_register_callback(&ipcon, IPCON_CALLBACK_CONNECTED, (void *) cb_connect, NULL);
    ipcon_register_callback(&ipcon, IPCON_CALLBACK_DISCONNECTED, (void *) cb_disconnect, NULL);
}


extern void tf_tear_down()
{
    ipcon_destroy(&ipcon);
}


static void cb_connect(uint8_t connect_reason, void *user_data)
{
    // printf("Connected to demon on %s. Enumeration requested.\n", HOST);
    ipcon_enumerate(&ipcon);
}


static void cb_disconnect(uint8_t connect_reason, void *user_data)
{
    printf("Disconnected from demon on %s.\n", HOST);
}


static void handle_disconnected(const char *uid, const char *connected_uid,
        char position, uint8_t hardware_version[3],
        uint8_t firmware_version[3], uint16_t device_identifier,
        uint8_t enumeration_type, void *user_data);

static void handle_connected(const char *uid, const char *connected_uid,
        char position, uint8_t hardware_version[3],
        uint8_t firmware_version[3], uint16_t device_identifier,
        uint8_t enumeration_type, void *user_data);


static void cb_enumerate(const char *uid, const char *connected_uid,
        char position, uint8_t hardware_version[3],
        uint8_t firmware_version[3], uint16_t device_identifier,
        uint8_t enumeration_type, void *user_data)
{
    (void) user_data;
    if (enumeration_type == IPCON_ENUMERATION_TYPE_DISCONNECTED)
    {
        handle_disconnected(uid, connected_uid, position, hardware_version,
                firmware_version, device_identifier,
                enumeration_type, user_data);
    }
    else //connected 
    {
        handle_connected(uid, connected_uid, position, hardware_version,
                firmware_version, device_identifier,
                enumeration_type, user_data);
    }
}


static void handle_disconnected(const char *uid, const char *connected_uid,
        char position, uint8_t hardware_version[3],
        uint8_t firmware_version[3], uint16_t device_identifier,
        uint8_t enumeration_type, void *user_data)
{
    int output_done = 0;

    if (rgb_matrix_is_valid)
    {
        if (!strcmp(uid, rgb_matrix_uid))
        {
            rgb_matrix_is_valid = 0;
            rgb_led_matrix_destroy(&rgb_led_matrix);
            printf("%s %s disconnected.\n", RGB_LED_MATRIX_DEVICE_DISPLAY_NAME, uid);
            output_done = 1;
        }
    }
    if (left_is_valid)
    {
        if (!strcmp(uid, left_uid))
        {
            left_is_valid = 0;
            joystick_destroy(&left);
            printf("%s %s disconnected.\n", JOYSTICK_DEVICE_DISPLAY_NAME, uid);
            output_done = 1;
        }
    }
    if (right_is_valid)
    {
        if (!strcmp(uid, right_uid))
        {
            right_is_valid = 0;
            joystick_destroy(&right);
            printf("%s %s disconnected.\n", JOYSTICK_DEVICE_DISPLAY_NAME, uid);
            output_done = 1;
        }
    }
    if (ambient_light_is_valid)
    {
        if (!strcmp(uid, ambient_light_uid))
        {
            ambient_light_is_valid = 0;
            ambient_light_destroy(&ambient_light);
            printf("%s %s disconnected.\n", AMBIENT_LIGHT_DEVICE_DISPLAY_NAME, uid);
            output_done = 1;
        }
    }
    if (piezo_speaker_is_valid)
    {
        if (!strcmp(uid, piezo_speaker_uid))
        {
            piezo_speaker_is_valid = 0;
            piezo_speaker_destroy(&piezo_speaker);
            printf("%s %s disconnected.\n", PIEZO_SPEAKER_DEVICE_DISPLAY_NAME, uid);
            output_done = 1;
        }
    }
    
    if (multi_touch_is_valid)
    {
        if (!strcmp(uid, multi_touch_uid))
        {
            multi_touch_is_valid = 0;
            multi_touch_destroy(&multi_touch);
            printf("%s %s disconnected.\n", PIEZO_SPEAKER_DEVICE_DISPLAY_NAME, uid);
            output_done = 1;
        }
    }
    
    if (!output_done)
    {
        printf("Brick or Bricklet %s disconnected.\n", uid);
    }
}


static void handle_connected(const char *uid, const char *connected_uid,
        char position, uint8_t hardware_version[3],
        uint8_t firmware_version[3], uint16_t device_identifier,
        uint8_t enumeration_type, void *user_data)
{
    int output_done = 0;
    if (device_identifier == MASTER_DEVICE_IDENTIFIER)
    {
        printf("%s %s available. Hardware %s ready.\n", MASTER_DEVICE_DISPLAY_NAME, uid, games_hw_ready() ? "is" : "is not");
        Master master;
        master_create(&master, uid, &ipcon);
        master_disable_status_led(&master);
        output_done = 1;
    }
    if (device_identifier == RGB_LED_MATRIX_DEVICE_IDENTIFIER)
    {
        if (!rgb_matrix_is_valid)
        {
            rgb_led_matrix_create(&rgb_led_matrix, uid, &ipcon);
            // rgb_led_matrix_register_callback(&rgb_led_matrix, RGB_LED_MATRIX_CALLBACK_FRAME_STARTED, (void *)rgb_matrix_callback, NULL);
            rgb_led_matrix_set_frame_duration(&rgb_led_matrix, 0);  
            rgb_matrix_clear();
            strcpy(rgb_matrix_uid, uid);
            rgb_matrix_is_valid = 1;
            rgb_led_matrix_set_status_led_config(&rgb_led_matrix, RGB_LED_MATRIX_STATUS_LED_CONFIG_OFF);
            printf("%s %s (position %c) available. Hardware %s ready.\n", RGB_LED_MATRIX_DEVICE_DISPLAY_NAME, uid, position - 32, games_hw_ready() ? "is" : "is not");
            output_done = 1;
        }
        else
        {
            printf("Superfluous %s %s (position %c) available.\n", RGB_LED_MATRIX_DEVICE_DISPLAY_NAME, uid, position - 32);
            output_done = 1;
        }
    }
    else if (device_identifier == JOYSTICK_DEVICE_IDENTIFIER)
    {
        if (position == 'a' || position == 'c')
        {
            if (!left_is_valid)
            {
                left_is_valid = 1;
                joystick_create(&left, uid, &ipcon);
                printf("Left  %s %s (position %c) available. Hardware %s ready.\n", JOYSTICK_DEVICE_DISPLAY_NAME, uid, position - 32, games_hw_ready() ? "is" : "is not");
                output_done = 1;
                strcpy(left_uid, uid);
                joystick_register_callback(&left, JOYSTICK_CALLBACK_PRESSED, (void *) cb_pressed_left, NULL);
                joystick_register_callback(&left, JOYSTICK_CALLBACK_RELEASED, (void *) cb_released_left, NULL);
                joystick_register_callback(&left, JOYSTICK_CALLBACK_POSITION_REACHED, (void *) cb_position_reached_left, NULL);
                joystick_set_position_callback_threshold(&left, 'o', -99, 98, -99, 98);
                joystick_set_debounce_period(&left, 200);
            }
            else
            {
                printf("Superfluous %s %s (position %c) available.\n", JOYSTICK_DEVICE_DISPLAY_NAME, uid, position - 32);
            }
        }
        if (position == 'b' || position == 'd')
        {
            if (!right_is_valid)
            {
                right_is_valid = 1;
                joystick_create(&right, uid, &ipcon);
                printf("Right %s %s (position %c) available. Hardware %s ready.\n", JOYSTICK_DEVICE_DISPLAY_NAME, uid, position - 32, games_hw_ready() ? "is" : "is not");
                output_done = 1;
                strcpy(right_uid, uid);
                joystick_register_callback(&right, JOYSTICK_CALLBACK_PRESSED, (void *) cb_pressed_right, NULL);
                joystick_register_callback(&right, JOYSTICK_CALLBACK_RELEASED, (void *) cb_released_right, NULL);
                joystick_register_callback(&right, JOYSTICK_CALLBACK_POSITION_REACHED, (void *) cb_position_reached_right, NULL);
                joystick_set_position_callback_threshold(&right, 'o', -99, 98, -99, 98);
                joystick_set_debounce_period(&right, 200);
            }
            else
            {
                printf("Superfluous %s %s (position %c) available.\n", JOYSTICK_DEVICE_DISPLAY_NAME, uid, position - 32);
            }
        }
        output_done = 1;
    }
    else if (device_identifier == AMBIENT_LIGHT_DEVICE_IDENTIFIER)
    {
        if (!ambient_light_is_valid)
        {
            ambient_light_create(&ambient_light, uid, &ipcon);
            strcpy(ambient_light_uid, uid);
            ambient_light_is_valid = 1;
            printf("%s %s (position %c) available. Hardware %s ready.\n", AMBIENT_LIGHT_DEVICE_DISPLAY_NAME, uid, position - 32, games_hw_ready() ? "is" : "is not");
            output_done = 1;
            ambient_light_set_illuminance_callback_period(&ambient_light, AMBIENT_LIGHT_CHANGE_INTERVALL);
            ambient_light_register_callback( &ambient_light, AMBIENT_LIGHT_CALLBACK_ILLUMINANCE,  (void (*)(void)) ambientlight_callback, 0);
/*
 *              (AmbientLight *ambient_light, int16_t callback_id, void (*function)(void), void *user_data);
*/
         }
        else
        {
            printf("Superfluous %s %s (position %c) available.\n", AMBIENT_LIGHT_DEVICE_DISPLAY_NAME, uid, position - 32);
            output_done = 1;
        }
    }
    else if (device_identifier == PIEZO_SPEAKER_DEVICE_IDENTIFIER)
    {
        if (!piezo_speaker_is_valid)
        {
            piezo_speaker_create(&piezo_speaker, uid, &ipcon);
            strcpy(piezo_speaker_uid, uid);
            piezo_speaker_is_valid = 1;
            printf("%s %s (position %c) available. Hardware %s ready.\n", PIEZO_SPEAKER_DEVICE_DISPLAY_NAME, uid, position - 32, games_hw_ready() ? "is" : "is not");
            output_done = 1;
        }
        else
        {
            printf("Superfluous %s %s (position %c) available.\n", PIEZO_SPEAKER_DEVICE_DISPLAY_NAME, uid, position - 32);
            output_done = 1;
        }
    }
    else if (device_identifier == MULTI_TOUCH_DEVICE_IDENTIFIER)
    {
        if (!multi_touch_is_valid)
        {
            multi_touch_create(&multi_touch, uid, &ipcon);
            strcpy(multi_touch_uid, uid);
            multi_touch_is_valid = 1;
            multi_touch_set_electrode_config(&multi_touch, 0b0111111111111);

            multi_touch_register_callback(&multi_touch, MULTI_TOUCH_CALLBACK_TOUCH_STATE, (void *)cb_touched, NULL);
                    
            printf("%s %s (position %c) available. Hardware %s ready.\n", MULTI_TOUCH_DEVICE_DISPLAY_NAME, uid, position - 32, games_hw_ready() ? "is" : "is not");
            output_done = 1;
        }
        else
        {
            printf("Superfluous %s %s (position %c) available.\n", MULTI_TOUCH_DEVICE_DISPLAY_NAME, uid, position - 32);
            output_done = 1;
        }
    }
    else
    {
        if (!output_done)
        {
            printf("Brick or Bricklet %s of type %hu available. Hardware %s ready.\n", uid, device_identifier,games_hw_ready() ? "is" : "is not");
        }
    }
}

extern int tf_hardware_ready()
{
    return (rgb_matrix_is_valid && left_is_valid && right_is_valid);
}