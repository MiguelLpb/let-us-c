/* 
 * File:   c4_timing.h
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on April 12, 2018, 2:18 PM
 */

#ifndef C4_TIMING_H
#define C4_TIMING_H

static const unsigned int C4_STATE_UPDATE_DELAY = 10;

static const unsigned int PLAYER_WARN_DELAY = 2000;
static const unsigned int WARN_BLINK_FREQUENCY = 250;
static const unsigned int PLAYER_TIMEOUT = 10000;

static const unsigned int DROP_DISC_DELAY = 500;


#define FRAMEDURATION 500                                                       /*!< frameduration in ms*/
#define BLINKFREQUENCY 100                                                      /*!< blinking frequency for arrow in the beginning*/
#define TURNSLEEP 2000           
/*!< time for each player to place stone*/
static const unsigned int TOGGLE_WINNER_FREQUENCY = 500;


#endif /* C4_TIMING_H */

