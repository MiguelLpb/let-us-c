
/* 
 * File:   switch_dual_button.c
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on February 16, 2018, 9:03 AM
 */

#include <stdio.h>
#include <time.h>

#include "logger.h"

#ifdef __CYGWIN__
#include <_timeval.h>
#define timersub(tvp, uvp, vvp)                                         \
        do {                                                            \
                (vvp)->tv_sec = (tvp)->tv_sec - (uvp)->tv_sec;          \
                (vvp)->tv_usec = (tvp)->tv_usec - (uvp)->tv_usec;       \
                if ((vvp)->tv_usec < 0) {                               \
                        (vvp)->tv_sec--;                                \
                        (vvp)->tv_usec += 1000000;                      \
                }                                                       \
        } while (0)
#endif

#include "switch.h"
#include "tf.h"

#include <sys/time.h>

/* *************************************** */
 
static struct timeval start_tval = {0, 0};

static void start_counting_time();
static double get_time_count();
static void stop_counting_time();

/* *************************************** */

static switch_callback_t switch_callback;
static switch_state_t switch_state;

typedef enum
{
    INTERN_OFF, INTERN_TO_ON, INTERN_ON, INTERN_TO_OFF
} internal_state_t;


static internal_state_t internal_state = INTERN_OFF;

static void dual_button_on_disconnect_cb();
static void dual_button_on_connect_cb(DualButton *);


extern void switch_init(void)
{
    switch_callback = 0;
    switch_state = SWITCH_STATE_OFF;
    tf_register_dual_button_on_connect(dual_button_on_connect_cb);
    tf_register_dual_button_on_disconnect(dual_button_on_disconnect_cb);
}


static void dual_button_on_disconnect_cb()
{
    log_entry("%s\n", __FUNCTION__);
    // ggf. _TO verlassen 
    // stop counting time
    switch (internal_state)
    {
        case INTERN_OFF:
        case INTERN_ON:
            break;
        case INTERN_TO_ON:
        case INTERN_TO_OFF:
            internal_state = (internal_state == INTERN_TO_ON) ? INTERN_OFF : INTERN_ON;
            stop_counting_time();
            break;
        default:
            printf("Upps. Illegal state %u\n", internal_state);
            break;
    }
}


static void dual_button_on_connect_cb(DualButton * db)
{
    log_entry("%s\n", __FUNCTION__);
    switch (internal_state)
    {
        case INTERN_OFF:
        case INTERN_TO_OFF:
            dual_button_set_selected_led_state(tf_get_dual_button(), 0, DUAL_BUTTON_LED_STATE_OFF);
            break;
        case INTERN_ON:
        case INTERN_TO_ON:
            dual_button_set_selected_led_state(tf_get_dual_button(), 0, DUAL_BUTTON_LED_STATE_ON);
            break;
        default:
            printf("Upps. Illegal state %u\n", internal_state);
            break;
    }
}



static void on_dual_button_state_changed_left(uint8_t, uint8_t);


extern void switch_subscribe(switch_callback_t cb)
{
    /*
     * printf("%s\n", __FUNCTION__); 
     */
    tf_register_dual_button_cb_client_left(on_dual_button_state_changed_left);
    switch_callback = cb;
}

/* *************************************** */

#define SECONDS_TO_PRESS 1.0


extern double switch_get_minimum_press_time()
{
    return SECONDS_TO_PRESS;
}

static internal_state_t to_intermediate_if_necessary(uint8_t button, internal_state_t current_state);

static internal_state_t from_intermediate(uint8_t button, internal_state_t current_state);


static void on_dual_button_state_changed_left(uint8_t button_l, uint8_t button_r)
{
    switch (internal_state)
    {
        case INTERN_OFF:
        case INTERN_ON:
            internal_state = to_intermediate_if_necessary(button_l, internal_state);
            break;
        case INTERN_TO_ON:
        case INTERN_TO_OFF:
            internal_state = from_intermediate(button_l, internal_state);
            switch_state_t new_switch_state = (internal_state == INTERN_ON) ? SWITCH_STATE_ON : SWITCH_STATE_OFF;
            if (new_switch_state != switch_state)
            {
                switch_state = new_switch_state;
                switch_callback(switch_state);
            }
            break;
        default:
            printf("Upps. Illegal state %u\n", internal_state);
            break;
    }
}


/*
 *  IF PRESSED: old_state -> new_state
 *  INTERN_OFF -> INTERN_TO_ON  on button pressed
 *  INTERN_ON -> INTERN_TO_OFF  on button pressed
 
 *  ELSE: unchanged
 */

static internal_state_t to_intermediate_if_necessary(uint8_t button, internal_state_t old_state)
{
    internal_state_t result = old_state;
    if (button == DUAL_BUTTON_BUTTON_STATE_PRESSED)
    {
        start_counting_time();
        result = (old_state == INTERN_OFF) ? INTERN_TO_ON : INTERN_TO_OFF;
    }
    return result;
}


/* 
 * IF RELEASED AT/AFTER TIME 
 * INTERN_TO_ON  -> INTERN_ON
 * INTERN_TO_OFF -> INTERN_OFF 
 * 
 * ELSE IF RELEASED BEFORE
 * INTERN_TO_ON  -> INTERN_OFF 
 * INTERN_TO_OFF -> INTERN_ON
 * 
 * ELSE unchanged
 */
static internal_state_t from_intermediate(uint8_t button, internal_state_t current_state)
{
    internal_state_t result = current_state;
    if (button == DUAL_BUTTON_BUTTON_STATE_RELEASED)
    {
        if (get_time_count() >= SECONDS_TO_PRESS)
        {
            result = (current_state == INTERN_TO_ON) ? INTERN_ON : INTERN_OFF;
        }
        else
        {
            result = (current_state == INTERN_TO_ON) ? INTERN_OFF : INTERN_ON;
        }
        stop_counting_time();
    }
    return result;
}


/* *************************************** */

static void start_counting_time()
{
    gettimeofday(&start_tval, NULL);
}


static double get_time_count()
{
    struct timeval end_tval, total_tval;
    gettimeofday(&end_tval, NULL);
    timersub(&end_tval, &start_tval, &total_tval);
    return (double) total_tval.tv_sec + (double) total_tval.tv_usec / 1.E6;
}


static void stop_counting_time()
{
    start_tval.tv_sec = 0;
    start_tval.tv_usec = 0;
}


static int is_counting()
{
    return (start_tval.tv_sec != 0) || (start_tval.tv_usec) != 0;
}


/* *************************************** */

extern void switch_run()
{
    if (is_counting() && tf_dual_button_is_valid())
    {
        if (get_time_count() >= SECONDS_TO_PRESS)
        {
            uint8_t led_r, led_l, new_led_state;
            dual_button_get_led_state(tf_get_dual_button(), &led_l, &led_r);
            switch (internal_state)
            {
                case INTERN_TO_ON:
                case INTERN_TO_OFF:
                    new_led_state = (internal_state == INTERN_TO_ON)
                            ? DUAL_BUTTON_LED_STATE_ON :
                            DUAL_BUTTON_LED_STATE_OFF;
                    if (led_l != new_led_state)
                    {
                        dual_button_set_selected_led_state(tf_get_dual_button(), 0, new_led_state);
                    }
                    break;
                default:
                    break;
            }
        }
    }
}


extern int switch_is_on()
{
    return switch_state;
}