/* 
 * File:   lamp_rgb.c
 * Author: Dr. Uwe Werner <uwe.werner@et.hs-fulda.de>
 *
 * Created on February 16, 2018, 9:03 AM
 */

#include <stdio.h>

#include "lamp.h"
#include "tf.h"
#include "logger.h"

#define LAMP_ON 1
#define LAMP_OFF 0

static void set_color(lamp_color_t color, uint8_t intensity);

/*
 * Lamp init, on, off
 */

static int lamp_state;

static lamp_color_t color;
static uint8_t intensity;
static const uint8_t INITIAL_INTENSITY = 127;


static void set_off();
static void rgb_led_on_disconnect_cb();
static void rgb_led_on_connect_cb(RGBLED *);


extern void lamp_init()
{
    log_entry("%s\n", __FUNCTION__);
    lamp_state = LAMP_OFF;
    color = LAMP_WHITE;
    intensity = INITIAL_INTENSITY;
    set_off();
    tf_register_rgb_led_on_connect(rgb_led_on_connect_cb);
    tf_register_rgb_led_on_disconnect(rgb_led_on_disconnect_cb);
}


static void rgb_led_on_disconnect_cb()
{
    log_entry("%s\n", __FUNCTION__);
}


static void rgb_led_on_connect_cb(RGBLED * rl)
{
    log_entry("%s\n", __FUNCTION__);
    if (lamp_state == LAMP_ON)
    {
        set_color(color, intensity);
    }
    else
    {
        set_off();
    }
}


extern void lamp_on()
{
    log_entry("%s\n", __FUNCTION__);
    set_color(color, intensity);
    lamp_state = LAMP_ON;
}


extern void lamp_off()
{
    log_entry("%s\n", __FUNCTION__);
    lamp_state = LAMP_OFF;
    set_off();
}


/*
 * Lamp intensity
 */
extern void lamp_dim(uint8_t new_intensity)
{
    log_entry("%s\n", __FUNCTION__);
    intensity = new_intensity;
    if (lamp_state == LAMP_ON)
    {
        set_color(color, intensity);
    }
}

static const uint8_t max_intensity = -1;


/*
 * Lamp color
 */

extern void lamp_set_next_color()
{
    log_entry("%s\n", __FUNCTION__);
    if (color == LAMP_WHITE)
    {
        color = LAMP_RED;
    }
    else
    {
        color++;
    }
    if (lamp_state == LAMP_ON)
    {
        set_color(color, intensity);
    }
}


/*
 * Access to real lamp
 */
static void set_off()
{
    log_entry("%s\n", __FUNCTION__);
    if (tf_rgb_led_is_valid())
    {
        rgb_led_set_rgb_value(tf_get_rgb_led(), 0, 0, 0);
    }
}


static void set_color(lamp_color_t color, uint8_t intensity)
{
    log_entry("%s\n", __FUNCTION__);
    if (tf_rgb_led_is_valid())
    {
        int r, g, b;
        switch (color)
        {
            case LAMP_RED:
                r = intensity;
                g = 0;
                b = 0;
                break;
            case LAMP_GREEN:
                r = 0;
                g = intensity;
                b = 0;
                break;
            case LAMP_BLUE:
                r = 0;
                g = 0;
                b = intensity;
                break;
            case LAMP_WHITE:
                r = intensity;
                g = intensity;
                b = intensity;
                break;
            default:
                printf(" invalid color %d\n", color);
                return;
        }
        rgb_led_set_rgb_value(tf_get_rgb_led(), r, g, b);
    }
}

