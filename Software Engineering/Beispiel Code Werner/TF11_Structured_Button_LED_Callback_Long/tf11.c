
/* 
 * File:   a03.c
 * Author: uwe
 *
 * Created on 13. April 2017, 11:09
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> 


#include "tf.h"
#include "lamp.h"
#include "dimmer.h"
#include "switch.h"
#include "contact.h"
#include "logger.h"


static void show_info();
static int check_for_log(int argc, char **argv);
static void on_switch_change_function(switch_state_t);
static void on_dimmer_change_function(uint8_t value);
static void on_contact_change_function();

static void init();

#define MAIN_LOOP_SLEEP_TIME_NANOS (100000)


int main(int argc, char **argv)
{
    do_logging = check_for_log(argc, argv);
    show_info();
    tf_init();
    init();

    while (1)
    {
#ifdef __CYGWIN__
        Sleep(MAIN_LOOP_SLEEP_TIME_NANOS / 1000); /* Sleep on Windows expects millis */
#else
        usleep(MAIN_LOOP_SLEEP_TIME_NANOS); /* usleep on Unixoids exepcts nanos */
#endif        
        switch_run();
    }
    tf_exit();
    return (EXIT_SUCCESS);
}


static void show_info()
{
    printf("Example Program: %s\n\n", __FILE__);
    printf("Hardware recognition: Dynamically detecting RGBLED, Dual Button, Poti Bricklet\n");
    printf("Application logic ..: Callbacks, main loop with %u ms wait \n", MAIN_LOOP_SLEEP_TIME_NANOS / 1000);
    printf("Source files .......: Multiple (Modular)\n\n");
    printf("POTI dims, LEFT BUTTON on/off when released after being pressed for at least %.1f sec, \n"
            "RIGHT next color! Both button LEDs provides feedback!\n\n", switch_get_minimum_press_time());
    printf("Source files .......: Multiple (Modular)\n\n");
    printf("Press return to start ... ");
    getchar();
    printf("\n");
}


static void init()
{
    lamp_init();

    switch_init();
    switch_subscribe(on_switch_change_function);

    dimmer_init();
    dimmer_subscribe(on_dimmer_change_function);

    contact_init();
    contact_subscribe(on_contact_change_function);
}


static void on_switch_change_function(switch_state_t state)
{
    log_entry("%s\n", __FUNCTION__);
    if (state == SWITCH_STATE_ON)
    {
        lamp_on();
    }
    else if (state == SWITCH_STATE_OFF)
    {
        lamp_off();
    }
}


static void on_contact_change_function()
{
    log_entry("%s\n", __FUNCTION__);
    lamp_set_next_color();
}


static void on_dimmer_change_function(uint8_t value)
{
    log_entry("%s\n", __FUNCTION__);
    lamp_dim(value);
}

/*
 * returns !0  if "log" provided as first argument in argv or zero
 */
static int check_for_log(int argc, char **argv)
{
    int is_equal=0;
    char * log = "log";
    const int POSITION = 1;
    if (argc > POSITION)
    {
        is_equal = ( strcmp(argv[POSITION], log) == 0);
    }
    return is_equal;
}

